newenv:
	virtualenv env
	source env/bin/activate
install:
	python setup.py install
distribute:
	python setup.py sdist
test:
	python setup.py test
	# bash test.sh
clean:
	find ./ -name "*.pyc" -delete
	find ./ -name "*.DS_Store" -delete
