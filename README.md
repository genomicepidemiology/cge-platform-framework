CGE Module
===================

This project documents the python framework module used by the
Center for Genomic Epidemiology (CGE) for their web-services

Installation
=============

For testing is recommended to use virtual environments

```
#!bash
make newenv
```
After the package can be installed using:
```
#!bash
make install
```
To run tests
```
#!bash
make test
```

Documentation
=============

An overview of the module is available here:
```
#!python

# Arguments (argumentparsing)
def getArguments(options, allowcmd=False, pipeline=False):
def getFqPairs(path):
def getnote(note):
def CheckFileType(files):
def makeFileList(uploadPath):
#def FixLocationMetaData(args):

# Utilities (utility)
class adv_dict(dict):
   def gettree(self, list_of_keys):
class Reg:
   def __init__(self, pattern, *flags):
   def sub(self, replace, string, count=0):
   def findall(self, s):
   def match(self, s):
   def getgroup(self, x):
   def getgroups(self):
class REgroup_obj():
   def __init__(self, pattern, flags=''):
   def Match(self, s):
def createZipDir(zipFilePath, *fileList):
def fileZipper(rootDir):
def fileUnzipper(directory):
def mkpath(filepath, permissions=0777):
def copyDir(src, dst):
def copyFile(src, dst): #srcFile, destPath+name
def moveFile(src, dst): #srcFile, destPath+name
def Sort2Groups(array, gpat=['_R1','_R2']):
def SortAndDistribute(array, splits=2):
def PrepareDownload(src, iid='', sid='', name='results', usr='Anonymous', desc=''):
def FixPermissions():

# IO (io)
class Debug():
   def __init__(self):
   def setup(self, debug=None, webmode=None, logfile=None, stdout=None, stderr=None):
   def printOut(self, *lst):
   def print2file(self, logfile, print2screen, addLineFeed, *lst):
   def log(self, *lst):
   def logNoNewline(self, msg):
   def GraceFullExit(self, service, serviceID, msg):
def open_(filename, mode=None, compresslevel=9):
def SeqsFromFile(filename, exit_on_err=False):
def printInputFilesHtml(inputFiles):
def tsv2html(fp, full_dl='', css_classes='greyhead'):
def dlButton(buttonText='results', requestedFile='', fid='', iid='', sid='', date=''):
def linkButton(url, buttonText):
def extendedOutButton(buttonText='extended output', table=''):
#def getGlobal(var): # Only used by assembly module
#def setGlobal(var, val): # not used

# Paths (pathtracking)
class paths_obj:
   def __init__(self):
   def __getitem__(self, key):
   def add_path(self, name, path):
   def replace_placeholder(self, tag, value):
   def Create(self, key):
def UpdatePaths(s, v, sid, ifolder, wm):
def createServiceDirs():
def createGroupDirs(gfolder, s, v):
def createIsolateDirs(ifolder, s, v):

# MySQL Communication (dbmanager)
def VerifyBatchAccess(usr, sesid, hours=0, days=1):
def add2DatabaseLog(service, runID_, db_User, db_IP, inputType):
def RetrieveDataAboutIsolates(usr, token, columns, iid=None):
def RetrieveContigsInfo(usr, token, iid):
def AddIsolate2DB(args):
def UpdateIsolate(iid, files):
def GetIsolateDir(usr, token, iid):
def AddService2DB(usr, token, iid, service, version, ip, other):
def UpdateServiceInDB(service, sid, status, results=None):
def GetServiceResults(iid, usr):
def AddFile2DB(iid, sid, usr, name, path, desc):
def AddGroup2DB(args, group_name):

# Program processing (cmdline)
class program:
   def __init__(self, name, path=None, timer=0, ptype=None, workDir=None, ssh=True, toQueue=True, wait=False, args=None, walltime=2, mem=4, procs=1, server='cgebase'):
   def GetTime(self):
   def GetStatus(self):
   def GetCMD(self):
   def UpdateTimer(self, time):
   def AppendArgs(self, arg):
   def Execute(self):
   def WaitOn(self, pattern='Done', interval=30, maxloopsleft=20, ePattern='Error'):
   def Printstdout(self):
   def FindStdoutVar(self, varnames=[]):
   def FindErrPattern(self, pattern):
   def FindOutPattern(self, pattern):
class programlist_obj(object):
   def __init__(self):
   def __getitem__(self, key):
   def AddProgram(self, progObj):
   def Add2List(self, progObj):
   def Exists(self, prog):
   def ReturnTimer(self, name, timer):
   def PrintTimers(self):
```

License
=======

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
