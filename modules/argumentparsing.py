#!/usr/bin/env python
""" THIS MODULE CONTAINS ALL THE SHARED WRAPPER FUNCTIONS """
################################################################################
#                              CGE FUNCTION MODULE                             #
################################################################################
# This script is part of the CGE Pipeline structure
import os
from subprocess import Popen, PIPE
from argparse import ArgumentParser, RawDescriptionHelpFormatter, SUPPRESS
#import geocoder
# CGE modules
import io
import utility

def getnote(note):
   """ This function checks if a path was given as note, and tries to read the
       file and return the note.
   """
   truenote = note
   if note is not None:
      if '/' in note:
         if os.path.isfile(note):
            try:
               with open(note,'r') as f:
                  truenote = ' '.join(line.strip() for line in f)
            except: pass
      if truenote.strip() == '': truenote = None
   return truenote

#def FixLocationMetaData(args):
#   """
#   * Get coordinates of the isolate if not present
#   * Return metadata updated
#
#   args.country
#   args.region
#   args.city
#   args.zip_code
#   args.longitude
#   args.latitude
#
#   """
#   # Coordinates not present
#   if args.country is None: args.country = ''
#   if args.region is None: args.region = ''
#   if args.city is None: args.city = ''
#   if args.zip_code is None: args.zip_code = ''
#   if args.longitude is None: args.longitude = ''
#   if args.latitude is None: args.latitude = ''
#   if (args.longitude  == '' or args.latitude == ''):
#      # Region exists in the metadata
#      location_string = ', '.join([x for x in [' '.join([args.zip_code, args.city]).strip(), args.region, args.country] if x != ''])
#      g = geocoder.google(location_string)
#      # Coordinates were not found, use City instead
#      if g.latitude == None or g.longitude == None:
#         g = geocoder.google(args.city)
#      # Replace country from geocoder in metadata
#      # According to Google it follows the ISO 3166-1
#      # http://en.wikipedia.org/wiki/ISO_3166-1
#      args.country = g.country
#      args.city = g.city
#      args.zip_code = g.postal
#      args.longitude = g.lng
#      args.latitude =  g.lat
#   else:
#      # Check location from coordinates with metadata
#      g = geocoder.reverse((args.latitude, args.longitude))
#      args.country = g.country

def getArguments(options, allowcmd=False, pipeline=False):
   """ This function handles and validates the wrapper arguments. """
   # These the next couple of lines defines the header of the Help output
   parser = ArgumentParser(
      formatter_class=RawDescriptionHelpFormatter,
      usage=("""%(prog)s
--------------------------------------------------------------------------------
"""),
      description=("""
Service Wrapper
===============
This is the service wrapper script, which is a part of the CGE services.
Read the online manual for help.
A list of all published services can be found at:
cge.cbs.dtu.dk/services

"""), epilog=("""
--------------------------------------------------------------------------------
      """))

   #ADDING ARGUMENTS
   setarg = parser.add_argument
   setarg("-w", action="store_true", dest="webmode", default=False,
                       help=SUPPRESS) #Webmode
   setarg("--ip", type=str, dest="ip", default="",
                       help=SUPPRESS) # IP address of user
   setarg("--usr", type=str, dest="usr", default="Anonymous",
                       help=SUPPRESS) # User ID
   setarg("--sesid", type=str, dest="sesid", default="",
                       help=SUPPRESS) # Session ID
   setarg("--token", type=str, dest="token", default="",
                       help=SUPPRESS) # Session ID
   if pipeline:
      setarg("--iid", type=int, dest="iid", default=None,
             help=SUPPRESS) # Isolate ID
   else:
      setarg("--uploadpath", type=str, dest="uploadPath", default=None,
             help=SUPPRESS) # Upload Path
   #SERVICE SPECIFIC ARGUMENTS
   if isinstance(options, str):
      options = [[x for i,x in enumerate(line.split()) if i in [1,2]] for line in options.split('\n') if len(line)>0]
      for o in options:
          # FIXME
          try:
              setarg(o[1], type=str, dest=o[0], default=None, help=SUPPRESS)
          except:
              None
   else:
      for o in options: setarg(o[0], type=str, dest=o[1], default=o[2], help=o[3])
   # VALIDATION OF ARGUMENTS
   args = parser.parse_args()
   if not args.webmode and not allowcmd:
      io.debug.GraceFullExit("Error: This wrapper is only meant to be run through "
                          "the web, as an extension of the pipeline service!\n")
   if args.usr != 'Anonymous' and args.token == '' and args.sesid == '':
      io.debug.GraceFullExit("Error: An access token is needed to validate the user!\n")
   if pipeline and args.iid == None:
      io.debug.GraceFullExit("Error: No isolate id was specified!\n")
   elif not pipeline and args.uploadPath == None:
       # FIXME
       try:
           io.debug.GraceFullExit("Error: No upload path was provided!\n")
       except:
           None
   io.debug.log("ARGS: %s"%args)
   return args

def getFqPairs(path):
   """ Use Rolf's input parser script to find valid paired fastq files. Returns
       list of pairs.
   """
   if os.path.exists(path):
      script = '/home/data1/services/CGEpipeline/CGEmodules/parse_input.pl'
      cmd = '%s %s/*'%(script,paths['uploads'])
      p=Popen(cmd, stdout=PIPE, stderr=PIPE)
      out, err = process.communicate()
      # strip and remove empty lines
      output_list = filter(None, map(str.strip, out.split('\n'), []))
      return [x.split('\t')[2:] for x in output_list if x[:6] == 'paired']
   else: return []

def CheckFileType(files):
   """ Check whether the input files are in fasta format, reads format or
       other/mix formats.
   """
   all_are_fasta = True
   all_are_reads = True
   if isinstance(files, str): files = [files]
   for file_ in files:
      io.debug.log('Checking file type: %s\n'%file_)
      with open_(file_, 'r') as f:
         fc = f.readline()[0]
         if fc != "@": all_are_reads = False
         if fc != ">": all_are_fasta = False
   if all_are_fasta: return 'fasta'
   elif all_are_reads: return 'fastq'
   else: return 'other'

def makeFileList(uploadPath):
   """ This function returns list of files in the given dir """
   newlist = []
   # let's get the input file paths
   fList = sorted(os.listdir(uploadPath))
   for el in fList:
      io.debug.log(el) #io.debug
      if ' ' in el:
         new_name = el.replace(' ', '_')
         io.debug.printOut('WARNING: Spaces are not allowed in file names! %s has '
                        'been renamed to %s.\n'%(el, new_name))
         utility.moveFile(uploadPath+el, uploadPath+new_name) # shutil.move
         el = new_name
      # following list will contain the path to the uploaded files
      newlist.append(os.path.normpath(uploadPath+'/'+el))
   io.debug.log('InputFiles: %s\n'%newlist)
   return newlist
