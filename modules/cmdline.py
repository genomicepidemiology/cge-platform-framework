#!/usr/bin/env python
""" THIS MODULE CONTAINS ALL THE SHARED WRAPPER FUNCTIONS """
################################################################################
#                              CGE FUNCTION MODULE                             #
################################################################################
# This script is part of the CGE Pipeline structure
import os
from subprocess import Popen
from pipes import quote
from time import time, sleep
# CGE modules
import io

############# CLASSES #############
class programlist_obj(object):
   """ This Class keeps track of all the programs
      USAGE:
         proglist = programlist_obj()
         proglist.Add2List(progObj)
         for progname in proglist.list: proglist[progname].WaitOn()
         proglist.PrintTimers()
   """
   def __init__(self):
      self.timer = -time() # Wrapper timer
      self.list = []
   def __getitem__(self, key):
      return getattr(self, key)
   def AddProgram(self, progObj):
      setattr(self, progObj.name, progObj)
      self.list.append(progObj.name)
      return progObj
   def Add2List(self, progObj):
      setattr(self, progObj.name, progObj)
      self.list.append(progObj.name)
   def Exists(self, prog):
      """ Checks whether the program exists in the program list. """
      return prog in dir(self)
   def ReturnTimer(self, name, timer):
      """ Return a text formatted timer """
      timer_template = '%s  %s : %s : %9s'
      t = str(timedelta(0, timer)).split(':')
      if len(t) == 4:
         d, h, m, s = int(t[0]), int(t[1]), int(t[2]), float(t[3])
      elif len(t) == 3: h, m, s = int(t[0]), int(t[1]), float(t[2])
      else: h, m, s = 0, 0, str(t)
      return timer_template%(
         name.ljust(20),
         '%2d'%h if h != 0 else '--',
         '%2d'%m if m != 0 else '--',
         '%.6f'%s if isinstance(s, float) else s
      )
   def PrintTimers(self):
      """ PRINT EXECUTION TIMES FOR THE LIST OF PROGRAMS """
      self.timer += time()
      totalTime = self.timer
      tmp = '*  %s  *'
      io.debug.log(
         '',
         '* '*24,
         tmp%(' '*41),
         tmp%('%s %s'%('Program Name'.ljust(20), 'Execute Time (H:M:S)')),
         tmp%('='*41)
      )
      for name in self.list:
         timer = getattr(self, name).GetTime()
         self.timer -= timer
         io.debug.log(tmp%(self.ReturnTimer(name, timer)))
      io.debug.log(
         tmp%(self.ReturnTimer('Wrapper', self.timer)),
         tmp%('='*41),
         tmp%(self.ReturnTimer('Total', totalTime)),
         tmp%(' '*41),
         '* '*24,
         ''
      )

class program:
   """ This class defines a program structure for the CGE system
      USAGE EXAMPLE:
         myprog = program(
            name    = 'MyProg',
            path    = '/path/to/myprog.py',
            ptype   = 'python',
            workdir = '/path/to/myprog/workdir',
            ToQueue = True,
            ssh     = True,
            Wait    = False,
            args    = [
               '--webmode',
               '-i', '/path/to/my/input.file'
            ]
         )
         if something == True:
            myprog.AppendArgs(['--DynamicArgument', 'value'])
         myprog.Execute()
         if not myprog.wait: myprog.WaitOn()
      OPTIONS:
         name        - Set the name of the program. *
         path        - Set the path to the program.
         timer       - Set the initial time expenditure for the program. *
         ptype       - Set the program type (EG. python or /bin/bash).
         workDir     - Set the working directory for the program.
         server      - Set the server for which to execute the program on.
         toQueue     - Set to False to execute the program without using the
                       cgebase queue. Should only be used for wrappers.
         ssh         - Set to False to execute the program on the cge machine,
                       else the program will execute on the specified server.
                       Should only be used for wrappers!
         wait        - Set to True to make the script wait for the program to
                       finish. This should not be used with the toQueue option!
         args        - Provide arguments for the program.
         walltime    - Set the max limit for how many hours the program may run.
         mem         - Set the max limit of how much memory the program may use.
         procs       - Set the amount of processors to reserve for the program.

         * this is for internal technical use only
   """
   def __init__(self, name, path=None, timer=0, ptype=None, workDir=None, ssh=True, toQueue=True, wait=False, args=None, walltime=2, mem=4, procs=1, server='cgebase'):
      # INIT VALUES
      self.path = path
      self.name = name
      self.timer = timer
      self.ptype = ptype
      self.workDir = workDir
      self.toQueue = toQueue
      self.wait = wait
      self.args = []
      self.unquoted_args = []
      self.stderr = "%s/%s.err"%(paths['logs'], name)
      self.stdout = "%s/%s.out"%(paths['outputs'], name) if os.path.exists(paths['outputs']) else "%s/%s.out"%(paths['logs'], name)
      self.queue = 'cge'
      self.wt = walltime # Maximum amount of hours required to complete program
      self.mem = mem     # GB RAM allocation requirement
      self.procs = procs # Number of processors required for the job
      self.p = None      # The Subprocess Object
      self.ssh = ssh
      self.server = server
      self.status = 'Initialised'
      if not workDir: self.workDir = paths['outputs']
      if webmode: self.queue = 'www'
      if args: self.AppendArgs(args)
   def GetTime(self):
      """ This function returns the amount of time used by the program
          (in seconds).
      """
      return self.timer
   def GetStatus(self):
      """ This function returns the amount of time used by the program
          (in seconds).
      """
      io.debug.log('status (%s): %s\n'%(self.name, self.status))
      return self.status
   def GetCMD(self):
      """ This function combines and return the commanline call of the program.
      """
      cmd = []
      if self.path is not None and os.path.exists(self.path):
         if self.ptype is not None:
            if paths[self.ptype] != '': cmd.append(paths[self.ptype])
            elif os.path.exists(self.ptype): cmd.append(self.ptype)
         cmd.append(self.path)
         cmd.extend([str(x) if not isinstance(x, (unicode)) else x.encode('utf-8') for x in [quote(str(x)) for x in self.args]+self.unquoted_args])
      return ' '.join(cmd)
   def UpdateTimer(self, time):
      """ This function updates the program timer. """
      self.timer += time
   def AppendArgs(self, arg):
      """ This function appends the provided arguments to the program object.
      """
      if isinstance(arg, (int,float)): self.args.append(str(arg))
      if isinstance(arg, str): self.args.append(arg)
      if isinstance(arg, list): self.args.extend([str(x) if not isinstance(x, (unicode)) else x.encode('utf-8') for x in arg])
   def Execute(self):
      """ This function Executes the program with set arguments. """
      # SETTING WORKDIR if not set
      prog_cmd = self.GetCMD().strip()
      if prog_cmd == '':
         self.status = 'Failure'
         io.debug.log("Error: No program to execute! (%s, %s)\n"%(self.name, self.path))
      else:
         if self.toQueue:
            otherArgs = ''
            if self.wait: otherArgs += "-K " # ADDING -K argument if wait() is forced
            tmpfile = self.workDir+'/cmd.sh'
            with open(tmpfile, 'w') as f:
               f.write("#!/bin/bash\ncd %s\ntouch %s %s\n%s\n\n"%(self.workDir,
                                                               self.stderr,
                                                               self.stdout,
                                                               prog_cmd))
            os.chmod(tmpfile, 0744)
            # QSUB INFO :: run_time_limit(walltime, dd:hh:mm:ss), memory(mem, up to 100GB *gigabyte), processors(ppn, up to 16) # USE AS LITTLE AS NEEDED!!!
            cmd = ("ssh %s /usr/bin/qsub -l nodes=1:ppn=%d,walltime=%d:00:00,mem=%dg -r y -d  %s -e %s -o %s %s %s")%(self.server, self.procs, self.wt, self.mem, self.workDir, self.stderr, self.stdout, otherArgs, tmpfile ) # '-q %s' self.queue
            io.debug.log("\n\nExecute %s...\n%s\n%s\n" % (self.name, cmd, prog_cmd))
         else:
            cmd = "(cd %s;%s) > %s 2> %s"%(self.workDir, prog_cmd, self.stdout,
                                         self.stderr)
            if self.ssh: cmd = "ssh %s '%s'"%(self.server, cmd)
            io.debug.log("\n\nExecute %s...\n%s\n" % (self.name, cmd)) #io.debug
         self.status = 'Executing'
         # EXECUTING PROGRAM
         self.UpdateTimer(-time()) # TIME START
         if self.wait:
            self.p = Popen(cmd, shell=True, executable="/bin/bash")
            ec = self.p.wait()
            self.status = 'Done'
            self.p = None
         else: # WaitOn should be called to determine if the program has ended
            self.p = Popen(cmd, shell=True, executable="/bin/bash")
         self.UpdateTimer(time()) # TIME END
         io.debug.log("timed: %s" % (self.GetTime()))
   def WaitOn(self, pattern='Done', interval=30, maxloopsleft=20, ePattern='Error'):
      """ This function will wait on a given pattern being shown on the last
          line of a given outputfile.

      OPTIONS
         pattern        - The string pattern to recognise when a program
                          finished properly.
         interval       - The amount of seconds to wait between checking the
                          log file.
         maxloopsleft   - How many times to check the log file before assuming
                          the program failed silently.
         ePattern       - The string pattern to recognise when a program
                          finished with an error.
      """
      io.debug.log("WaitOn called for %s..."%str(self.name))
      if self.status == 'Executing':
         ePattern = ePattern.lower()
         self.UpdateTimer(-time()) # TIME START
         found = False
         if self.toQueue:
            # Handling programs running on the compute servers
            io.debug.printErr('\n<!-- ') # sys.stderr.write
            # Waiting for error log to be created.
            # Prolonged waiting can be caused by the queue being full, or the
            # server being unavailable.
            io.debug.log("   Waiting for the error log to be created (%s)..."%(
                     self.stderr))
            #sleep(20)
            maxqueuedtime = 10800 # Maximum amount of seconds to wait on the errorlog creation, before asuming queue failure.
            while ( not os.path.exists(self.stderr)
                  and time()+self.timer < maxqueuedtime
                  and time()+self.timer > 0
                  ):
               io.debug.log("      Waiting... (max wait time left: %s seconds)"%(
                        str(maxqueuedtime-time()-self.timer)))
               sleep(20)
            io.debug.printErr(' -->\n') # sys.stderr.write
            if os.path.exists(self.stderr):
               # File created looking for pattern
               io.debug.log(('   The error log was found (%s), waiting for the '
                           'program to finish...')%(self.stderr))
               while maxloopsleft > 0:
                  with open(self.stderr) as f:
                     for l in f.readlines()[-5:]: # last five lines
                        if pattern in l:
                           found = True
                           maxloopsleft = 0
                           break
                        elif ePattern in l.lower():
                           found = False
                           maxloopsleft = 0
                           break
                  if maxloopsleft > 1:
                     io.debug.log('      Waiting... (max wait-time left: %s seconds)'%(
                              str(maxloopsleft*interval)))
                     sleep(interval)
                  maxloopsleft -= 1
               if found:
                  io.debug.log("   Program finished successfully!")
                  self.status = 'Done'
               else:
                  io.debug.log("Error: Program took too long, or finished with error!")
                  io.debug.printOut("Technical error occurred!<br>\n",
                     "The service was not able to produce a result.<br>\n",
                     "Please check your settings are correct, and the file "+
                     "type matches what you specified.", "Try again, and if "+
                     "the problem persists please notify the technical "+
                     "support.<br>\n")
                  self.status = 'Failure'
            else:
               io.debug.log(("Error: %s still does not exist!\n")%(self.stderr),
                        "This error might be caused by the cgebase not being "+
                        "available!")
               io.debug.printOut("Technical error occurred!<br>\n",
                  "This error might be caused by the server not being "+
                  "available!<br>\n", "Try again later, and if the problem "+
                  "persists please notify the technical support.<br>\n",
                  "Sorry for any inconvenience.<br>\n")
               self.status = 'Failure'
            if not self.p is None:
               self.p.wait()
               self.p = None
         else:
            # Handling wrappers running on the webserver
            ec = self.p.wait()
            if os.path.exists(self.stderr):
               with open(self.stderr) as f:
                  for l in f.readlines()[-5:]: # last five lines
                     if pattern in l:
                        found = True
                        maxloopsleft = 0
                        break
                     elif ePattern in l.lower():
                        found = False
                        maxloopsleft = 0
                        break
               if found:
                  io.debug.log("   Program finished successfully!")
                  self.status = 'Done'
               else:
                  io.debug.log("Error: Program failed to finish properly!")
                  io.debug.printOut("Technical error occurred!<br>\n",
                     "The service was not able to produce a result.<br>\n",
                     "Please check your settings are correct, and the file "+
                     "type matches what you specified.", "Try again, and if "+
                     "the problem persists please notify the technical "+
                     "support.<br>\n")
                  self.status = 'Failure'
            else:
               io.debug.log(("Error: %s does not exist!\n")%(self.stderr),
                  "This error might be caused by the cgebase not being "+
                  "available!")
               io.debug.printOut("Technical error occurred!<br>\n",
                  "This error might be caused by the server not being "+
                  "available!<br>\n", "Try again later, and if the problem "+
                  "persists please notify the technical support.<br>\n",
                  "Sorry for any inconvenience.<br>\n")
               self.status = 'Failure'
            self.p = None
         self.UpdateTimer(time()) # TIME END
         io.debug.log("   timed: %s"%(self.GetTime()))
      else:
         io.debug.log("   The check-out of the program has been sorted previously.")
   def Printstdout(self):
      """ This function will read the standard out of the program and print it
      """
      # First we check if the file we want to print does exists
      if os.path.exists(self.stdout):
         with open(self.stdout, 'r') as f:
            io.debug.printOut("\n".join([line for line in f]))
      else: # FILE DOESN'T EXIST
         io.debug.log("Error: The stdout file "+ self.stdout +" does not exist!")
   def FindStdoutVar(self, varnames=[]):
      """ This function will read the standard out of the program, catch
          variables and return the values

          EG. #varname=value
      """
      response = [None]*len(varnames)
      # First we check if the file we want to print does exists
      if os.path.exists(self.stdout):
         with open(self.stdout, 'r') as f:
            for line in f:
               if '=' in line:
                  var = line.strip('#').split('=')
                  value = var[1].strip()
                  var = var[0].strip()
                  if var in varnames: response[varnames.index(var)] = value
      else: # FILE DOESN'T EXIST
         io.debug.log("Error: The stdout file "+ self.stdout +" does not exist!")
      return response
   def FindErrPattern(self, pattern):
      """ This function will read the standard error of the program and return
          a matching pattern if found.

          EG. prog_obj.FindErrPattern("Update of mySQL failed")
      """
      response = []
      # First we check if the file we want to print does exists
      if os.path.exists(self.stderr):
         with open(self.stderr, 'r') as f:
            for line in f:
               if pattern in line:
                  response.append(line.strip())
      else: # FILE DOESN'T EXIST
         io.debug.log("Error: The stderr file "+ self.stderr +" does not exist!")
      return response
   def FindOutPattern(self, pattern):
      """ This function will read the standard error of the program and return
          a matching pattern if found.

          EG. prog_obj.FindErrPattern("Update of mySQL failed")
      """
      response = []
      # First we check if the file we want to print does exists
      if os.path.exists(self.stdout):
         with open(self.stdout, 'r') as f:
            for line in f:
               if pattern in line:
                  response.append(line.strip())
      else: # FILE DOESN'T EXIST
         io.debug.log("Error: The stdout file "+ self.stdout +" does not exist!")
      return response


# Initialising program list
proglist = programlist_obj()
