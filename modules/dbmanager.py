#!/usr/bin/env python
""" This library contains classes and methods related to mySQL db calls """
################################################################################
#                              CGE FUNCTION MODULE                             #
################################################################################
# This script is part of the CGE Pipeline structure
import MySQLdb, re, json
from time import localtime, strftime
from datetime import timedelta, date, datetime
import geocoder
import hashlib
# CGE modules
import io

def add2DatabaseLog(service, runID_, db_User, db_IP, inputType):
   """ The service specific run logger """
   global runID
   runID = runID_
   db_Location = geocoder.ip(db_IP).address # City, Region, Country, coordinates (based on db_IP)
   db_Date = strftime("%d/%m-%Y", localtime())

   # CONNECT TO SQL DATABASE
   try:
      conn = MySQLdb.connect(host   = "cge",
                             user   = "cgeclient",
                             passwd = "www",
                             db     = "cge")
   except MySQLdb.Error, e:
      io.debug.log("No record of this run was made!",
                "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
   else:
      # USE CONNECTION AND UPDATE THE DATABSE WITH CURRENT RUN DETAILS
      try:
         io.debug.log("""INSERT INTO runs
                      (service, run_id, user_id, user_ip, user_loc, date,
                      technology)
                      VALUES (%s, %s, %s, %s, %s, %s, %s)
                      ;"""% (service, runID, db_User, db_IP, db_Location, db_Date, inputType))
         cursor = conn.cursor()
         cursor.execute("""INSERT INTO runs
                           (service, run_id, user_id, user_ip, user_loc, date,
                           technology)
                           VALUES (%s, %s, %s, %s, %s, %s, %s)
                           ;""", (service, runID, db_User, db_IP, db_Location, db_Date, inputType))
         cursor.close()
      except MySQLdb.Error, e:
         io.debug.log("No record of this run was made!",
                   "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
      # COMMIT AND CLOSE THE CONNECTION
      conn.commit()
      conn.close()

def VerifyBatchAccess(usr, sesid, hours=0, days=1):
   """ Verify the user and generate a batch access token. """
   io.debug.log('Updating DB - Add batch access token.')
   access_token = None
   # VALIDATE INPUTS
   if usr and sesid:
      # CONNECT TO SQL DATABASE
      try:
         conn = MySQLdb.connect(host   = "cge",
                                user   = "cgeclient",
                                passwd = "www",
                                db     = "cge")
      except MySQLdb.Error, e:
         io.debug.log("No record of this run was made!",
                   "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
      else:
         # VALIDATE USER EXSISTENCE
         try:
            cursor = conn.cursor()
            cursor.execute("""SELECT u.id
                              FROM users u
                              WHERE u.usr = %s
                              AND u.session_id = %s
                              ;""", (usr, sesid))
            r = cursor.fetchone()
            uid = int(r[0])
            cursor.close()
         except MySQLdb.Error, e:
            io.debug.log("User verification in mySQL failed!",
                      "The batch could be executed!",
                      "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
            io.debug.GraceFullExit("Error: Either the user session is invalid, or the user does not exist!\n")
            uid = None
         except Exception, e:
            io.debug.log("User verification in mySQL failed!",
                      "Either the user session is invalid, or the user does not exist!",
                      "The batch could be executed!",
                      e)
            io.debug.GraceFullExit("Error: Either the user session is invalid, or the user does not exist!\n")
            uid = None
         else:
            # GENERATE ACCESS TOKEN
            key = "=(/#JHF%POSuwh89/)(jh9873#"
            now = datetime.now()
            expiration_date = (now+timedelta(hours/24.+days)).strftime('%Y-%m-%d %H:%M:%S')
            access_token = hashlib.sha1("%s%s%s%s%s"%(key, usr, now.strftime('%Y-%m-%d %H:%M:%S'), sesid, key)).hexdigest()
            # ADD TOKEN TO DB
            try:
               cursor = conn.cursor()
               cursor.execute("""INSERT INTO batch_access
                                 (user_id, token, granted_date, expiration_date)
                                 VALUES (%s, %s, now(), %s);""",
                                 (uid, access_token, expiration_date))
               cursor.close()
            except MySQLdb.Error, e:
               io.debug.log("Update of mySQL failed!",
                         "The batch access could not be granted!",
                         "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
            except Exception, e:
               io.debug.log("Update of mySQL failed!",
                         "The batch access could not be granted!",
                         e)
            else:
               # COMMIT CHANGES TO DB
               conn.commit()
         finally:
            # CLOSE CONNECTION
            conn.close()
   else:
      io.debug.log("The batch access could not be granted!!",
                "User name and/or session id was not provided!")
   return access_token

def AddIsolate2DB(args):
   """ This function creates a mySQL database entry for the isolate, and
   returns the isolate ID. Note: The isolate folder has not been created yet,
   and an update is required later to add this."""

   io.debug.log('Updating DB - Adding Isolate.')
   # CONNECT TO SQL DATABASE
   try:
      conn = MySQLdb.connect(host   = "cge",
                             user   = "cgeclient",
                             passwd = "www",
                             db     = "cge")
   except MySQLdb.Error, e:
      io.debug.log("No record of this run was made!",
                "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
      return None, None
   else:
      isolateID = None
      ifolder   = None
      approved  = False
      try:
         # VERIFY USER EXSISTENCE
         io.debug.log("""SELECT id
                      FROM users
                      WHERE usr = %s
                      ;"""%(args.usr))
         cursor = conn.cursor()
         cursor.execute("""SELECT id
                           FROM users
                           WHERE usr = %s
                           ;""", (args.usr,))
         uid = int(cursor.fetchone()[0])
         # VERIFY USER ACCESS
         io.debug.log("""SELECT token
                      FROM batch_access
                      WHERE user_id = %s
                      AND expiration_date >= now()
                      ;"""%(uid))
         cursor.execute("""SELECT token
                           FROM batch_access
                           WHERE user_id = %s
                           AND expiration_date >= now()
                           ;""", (uid,))
         if not any([True if r[0]==args.token else False for r in cursor]):
            raise Exception('Token is not valid!')
         cursor.close()
      except MySQLdb.Error, e:
         io.debug.log("Update of mySQL failed (access validation)!",
                   "No record of this run was made!",
                   "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
      except Exception, e:
         io.debug.log("Update of mySQL failed (access validation)!",
                   "No record of this run was made!", e)
      else:
         # UPDATE DB
         files = makeFileList(args.uploadPath)
         try:
            cursor = conn.cursor()
            cursor.execute("""INSERT INTO isolates
                              (owner, sample_name, date, sequencing_platform,
                              sequencing_type, pre_assembled, isolate_path,
                              file_names, public, shared, removed, sample_type,
                              usage_restrictions, release_date)
                              VALUES (%s, %s, DATE(NOW()), %s, %s, %s, NULL, %s,
                                      0, NULL, 0, %s, %s, %s )
                              ;""", (uid, args.sample_name, args.sequencing_platform, args.sequencing_type, args.pre_assembled, ', '.join(files), args.sample_type, args.usage_restrictions, args.release_date))
            # RETRIEVING SERVICE ID
            cursor.execute('SELECT LAST_INSERT_ID();')
            isolateID = int(cursor.fetchone()[0])
            cursor.close()
         except MySQLdb.Error, e:
            io.debug.log("Update of mySQL failed (adding isolate)!",
                      "No record of this run was made!",
                      "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
         except Exception, e:
            io.debug.log("Update of mySQL failed (adding isolate)!",
                      "No record of this run was made!",
                      e)
         else:
            io.debug.log(isolateID, type(isolateID))
            if isinstance(isolateID, int):
               # UPDATE isolate folder and Add meta data entry
               y, m, d = strftime("%Y %m %d", localtime()).split()
               ifolder = '/home/data1/isolates/%s/%s/%s/%s'%(y, m, d, isolateID)
               try: collection_date = date(*[int(x) for x in args.collection_date.split('-')])
               except Exception: collection_date = None
               try:
                  # Update isolate folder
                  cursor = conn.cursor()
                  cursor.execute("""UPDATE isolates
                                    SET isolate_path = %s
                                    WHERE id = %s
                                    ;""", (ifolder, isolateID))
                  # Add meta data entry
                  io.debug.log("""INSERT INTO meta
                               (id, country, region, city, zip_code,
                               longitude, latitude, location_note, collection_date, date_uncertainty_flag, location_uncertainty_flag,
                               go_country, go_region, go_city, go_zip_code,
                               go_longitude, go_latitude,
                               isolation_source, source_note, pathogenic,
                               pathogenicity_note, notes, organism, strain, subtype, collected_by)
                               VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s,
                               %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                               ;""" % (isolateID, args.country,
                                       args.region, args.city,
                                       args.zip_code, args.longitude,
                                       args.latitude, args.location_note,
                                       collection_date, args.date_uncertainty_flag,
                                       args.location_uncertainty_flag,
                                       args.go_country, args.go_region, args.go_city,
                                       args.go_zip_code, args.go_longitude, args.go_latitude,
                                       args.isolation_source,
                                       args.source_note,
                                       args.pathogenic,
                                       args.pathogenicity_note,
                                       args.notes, args.organism, args.strain, args.subtype, args.collected_by))

                  # Using placeholders insted of string formatting
                  cursor.execute("""INSERT INTO meta
                                    (id, country, region, city, zip_code,
                                    longitude, latitude, location_note, collection_date, date_uncertainty_flag, location_uncertainty_flag,
                                    go_country, go_region, go_city, go_zip_code,
                                    go_longitude, go_latitude,
                                    isolation_source, source_note, pathogenic,
                                    pathogenicity_note, notes, organism, strain, subtype, collected_by)
                                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s,
                                    %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                                    ;""", (isolateID, args.country,
                                           args.region, args.city,
                                           args.zip_code, args.longitude,
                                           args.latitude, args.location_note,
                                           collection_date, args.date_uncertainty_flag,
                                           args.location_uncertainty_flag,
                                           args.go_country, args.go_region, args.go_city,
                                           args.go_zip_code, args.go_longitude, args.go_latitude,
                                           args.isolation_source,
                                           args.source_note,
                                           args.pathogenic,
                                           args.pathogenicity_note,
                                           args.notes, args.organism, args.strain, args.subtype, args.collected_by))
                  cursor.close()
               except MySQLdb.Error, e:
                  io.debug.log("No update was made (adding meta + update folderID)!",
                            "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
                  conn.rollback()
               else:
                  # COMMIT CHANGES TO DB
                  conn.commit()
      finally:
         # CLOSE CONNECTION
         conn.close()
      return isolateID, ifolder


def UpdateIsolate(iid, files):
   """ This function updates the isolate file locations. """
   io.debug.log('Updating DB - Update Isolate files.')
   # VALIDATE INPUTS
   if isinstance(iid, int) and isinstance(files, list):
      # CONNECT TO SQL DATABASE
      try:
         conn = MySQLdb.connect(host   = "cge",
                                user   = "cgeclient",
                                passwd = "www",
                                db     = "cge")
      except MySQLdb.Error, e:
         io.debug.log("No record of this run was made!",
                   "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
      else:
         # UPDATE DB
         try:
            cursor = conn.cursor()
            io.debug.log("""UPDATE isolates
                         SET file_names = %s
                         WHERE id = %s
                         ;"""% (', '.join(files), iid))
            cursor.execute("""UPDATE isolates
                              SET file_names = %s
                              WHERE id = %s
                              ;""", (', '.join(files), iid))
            cursor.close()
         except MySQLdb.Error, e:
            io.debug.log("No update was made (input files update failed)!",
                      "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
            conn.rollback()
         else:
            # COMMIT CHANGES TO DB
            conn.commit()
         finally:
            # CLOSE CONNECTION
            conn.close()


def RetrieveDataAboutIsolates(usr, token, columns, iid=None):
   """ This function retrieves data from the isolates table in the database.
   The columns list specify which columns should be extracted. """
   io.debug.log('Retrieving Isolate Data...')
   results = None
   # Validate Input
   if (     usr
        and token
        and isinstance(columns, list)
        and len(columns)>0
        and all([re.match('^\w+$', x) is not None for x in columns])
      ):
      # CONNECT TO SQL DATABASE
      try:
         conn = MySQLdb.connect(host   = "cge",
                                user   = "cgeclient",
                                passwd = "www",
                                db     = "cge")
      except MySQLdb.Error, e:
         io.debug.log("No data was retrieved!",
                   "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
      else:
         try:
            # VERIFY USER EXSISTENCE
            cursor = conn.cursor()
            io.debug.log("""SELECT id
                         FROM users
                         WHERE usr = %s
                         ;"""%(usr))
            cursor.execute("""SELECT id
                              FROM users
                              WHERE usr = %s
                              ;""", (usr,))
            uid = int(cursor.fetchone()[0])
            # VERIFY USER ACCESS
            io.debug.log("""SELECT token
                         FROM batch_access
                         WHERE user_id = %s
                         AND expiration_date >= now()
                         ;"""%(uid))
            cursor.execute("""SELECT token
                              FROM batch_access
                              WHERE user_id = %s
                              AND expiration_date >= now()
                              ;""", (uid,))
            if not any([True if r[0]==token else False for r in cursor]):
               raise Exception('Token is not valid!')
            cursor.close()
         except MySQLdb.Error, e:
            io.debug.log("MySQL failed (access validation)!",
                      "No data was retrieved!",
                      "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
         except Exception, e:
            io.debug.log("MySQL failed (access validation)!",
                      "No data was retrieved!", e)
         else:
            # Retrieve data from DB
            try:
               isolatecondition = ''
               if iid is not None and isinstance(iid, int):
                  isolatecondition = 'AND id = %d'%(iid)
               elif iid is not None:
                  io.debug.log('Error: isolate id is not an iteger (type=%s)'%(type(iid)))
               io.debug.log("""SELECT %s
                            FROM isolates
                            WHERE owner = %s
                            %s
                            ;"""%(', '.join(columns), '%s', isolatecondition)%(uid))
               cursor = conn.cursor()
               cursor.execute("""SELECT %s
                                 FROM isolates
                                 WHERE owner = %s
                                 %s
                                 ;"""%(', '.join(columns), '%s', isolatecondition), [uid])
               results = cursor.fetchall()
               cursor.close()
            except MySQLdb.Error, e:
               io.debug.log("No data was retrieved!",
                         "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
            else:
               # COMMIT CHANGES TO DB
               pass
      finally:
         # CLOSE CONNECTION
         conn.close()
   return results

def AddGroup2DB(args, group_name):
   """ This function creates a mySQL database entry for the group, and
   returns the group ID. Note: The group folder has not been created yet,
   and an update is required later to add this."""
   io.debug.log('Updating DB - Adding group.')
   # CONNECT TO SQL DATABASE
   try:
      conn = MySQLdb.connect(host   = "cge",
                             user   = "cgeclient",
                             passwd = "www",
                             db     = "cge")
   except MySQLdb.Error, e:
      io.debug.log("No record of this run was made!",
                "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
      return None, None
   else:
      # VALIDATE USER EXSISTENCE
      groupID = None
      gfolder = None
      try:
         # VERIFY USER EXSISTENCE
         cursor = conn.cursor()
         io.debug.log("""SELECT id
                      FROM users
                      WHERE usr = %s
                      ;"""%(args.usr))
         cursor.execute("""SELECT id
                           FROM users u
                           WHERE usr = %s
                           ;""", (args.usr,))
         uid = int(cursor.fetchone()[0])
         # VERIFY USER ACCESS
         io.debug.log("""SELECT token
                      FROM   batch_access
                      WHERE  user_id = %s
                      AND    expiration_date >= now()
                      ;"""%(uid))
         cursor.execute("""SELECT token
                           FROM   batch_access
                           WHERE  user_id = %s
                           AND    expiration_date >= now()
                           ;""", (uid,))
         if not any([True if r[0]==args.token else False for r in cursor]):
            raise Exception('Token is not valid!')
         cursor.close()
      except MySQLdb.Error, e:
         io.debug.log("Update of mySQL failed (access validation)!",
                   "No record of this run was made!",
                   "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
      except Exception, e:
         io.debug.log("Update of mySQL failed (access validation)!",
                   "No record of this run was made!", e)
      else:
         # UPDATE DB
         files = makeFileList(args.uploadPath)
         try:
            cursor = conn.cursor()
             #owner, sample_name, date, sequencing_platform, isolate_path,
             #                 file_names, public, shared, removed, sample_type, usage_restrictions, usage_delay)
            cursor.execute("""INSERT INTO groups
                              (owner, group_name, date_group, group_path, data)
                              VALUES (%s, %s, DATE(NOW()), NULL, NULL )
                              ;""", (uid, group_name))
            # RETRIEVING SERVICE ID
            cursor.execute('SELECT LAST_INSERT_ID();')
            groupID = int(cursor.fetchone()[0])
            cursor.close()
         except MySQLdb.Error, e:
            io.debug.log("Update of mySQL failed (adding group)!",
                      "No record of this run was made!",
                      "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
         except Exception, e:
            io.debug.log("Update of mySQL failed (adding group)!",
                      "No record of this run was made!",
                      e)
         else:
            io.debug.log(groupID, type(groupID))
            if isinstance(groupID, int):
               # UPDATE isolate folder and Add meta data entry
               y, m, d = strftime("%Y %m %d", localtime()).split()
               gfolder = '/home/data1/batch/%s/%s/%s/%s'%(y, m, d, groupID)
               try: collection_date = date(*[int(x) for x in args.collection_date.split('-')])
               except Exception: idate = None
               try:
                  # Update isolate folder
                  cursor = conn.cursor()
                  cursor.execute("""UPDATE groups
                                    SET group_path = %s
                                    WHERE id = %s
                                    ;""", (gfolder, groupID))
                  cursor.close()
               except MySQLdb.Error, e:
                  io.debug.log("No update was made (adding meta + update folderID)!",
                            "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
                  conn.rollback()
               else:
                  # COMMIT CHANGES TO DB
                  conn.commit()
      finally:
         # CLOSE CONNECTION
         conn.close()
      return groupID, gfolder

def RetrieveContigsInfo(usr, token, iid):
   """ This function retrieves data from the isolates table in the database.
   The columns list specify which columns should be extracted. """
   io.debug.log('Retrieving Contigs Information...')
   results = None
   # Validate Input
   if usr and token and iid and isinstance(iid, int):
      # CONNECT TO SQL DATABASE
      try:
         conn = MySQLdb.connect(host   = "cge",
                                user   = "cgeclient",
                                passwd = "www",
                                db     = "cge")
      except MySQLdb.Error, e:
         io.debug.log("No data was retrieved!",
                   "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
      else:
         try:
            # VERIFY USER EXSISTENCE
            cursor = conn.cursor()
            io.debug.log("""SELECT id
                         FROM users
                         WHERE usr = %s
                         ;"""%(usr))
            cursor.execute("""SELECT id
                              FROM users
                              WHERE usr = %s
                              ;""", (usr,))
            uid = int(cursor.fetchone()[0])
            # VERIFY USER ACCESS
            io.debug.log("""SELECT token
                         FROM batch_access
                         WHERE user_id = %s
                         AND expiration_date >= now()
                         ;"""%(uid))
            cursor.execute("""SELECT token
                              FROM batch_access
                              WHERE user_id = %s
                              AND expiration_date >= now()
                              ;""", (uid,))
            if not any([True if r[0]==token else False for r in cursor]):
               raise Exception('Token is not valid!')
            cursor.close()
         except MySQLdb.Error, e:
            io.debug.log("MySQL failed (access validation)!",
                      "No data was retrieved!",
                      "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
         except Exception, e:
            io.debug.log("MySQL failed (access validation)!",
                      "No data was retreived!", e)
         else:
            # Retrieve data from DB
            try:
               cursor = conn.cursor()
               cursor.execute("""SELECT path, description
                                 FROM files
                                 WHERE isolate_id = %s
                                 AND owner = %s
                                 AND name = 'contigs'
                                 ;""", (iid, uid))
               results = cursor.fetchall()
               cursor.close()
            except MySQLdb.Error, e:
               io.debug.log("No data was retrieved!",
                         "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
            else:
               # Select the results with the best N50 score
               io.debug.log(results)
               r = ['', 0]
               for path, s in results:
                  try: n50 = int(re.sub('[\W_]+', '', s.split(':')[1]))
                  except: n50 = 0
                  if n50 > r[1]:
                     r[0] = path
                     r[1] = n50
                  elif r[0] == '':
                     r[0] = path
                     r[1] = n50
               if r[0] != '': results = r[0]
               else:
                  results = None
                  io.debug.log('Error: Found no suitable contigs!')
         finally:
            # CLOSE CONNECTION
            conn.close()
   return results


def AddService2DB(usr, token, iid, service, version, ip, other):
   """ This function creates a mySQL database entry for the service, and returns
   the service ID. Note: the service folder has not been created yet, and an
   update is required later to update this. """
   io.debug.log('Updating DB - Adding Service.')
   serviceID, ifolder = None, None
   # VALIDATE INPUTS
   if usr and token and iid:
      # CONNECT TO SQL DATABASE
      try:
         conn = MySQLdb.connect(host   = "cge",
                                user   = "cgeclient",
                                passwd = "www",
                                db     = "cge")
      except MySQLdb.Error, e:
         io.debug.log("No record of this run was made!",
                   "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
      else:
         try:
            # VERIFY USER EXSISTENCE
            cursor = conn.cursor()
            io.debug.log("""SELECT id
                         FROM users
                         WHERE usr = %s
                         ;"""%(usr))
            cursor.execute("""SELECT id
                              FROM users
                              WHERE usr = %s
                              ;""", (usr,))
            uid = int(cursor.fetchone()[0])
            # VERIFY USER ACCESS
            io.debug.log("""SELECT token
                         FROM batch_access
                         WHERE user_id = %s
                         AND expiration_date >= now()
                         ;"""%(uid))
            cursor.execute("""SELECT token
                              FROM batch_access
                              WHERE user_id = %s
                              AND expiration_date >= now()
                              ;""", (uid,))
            if not any([True if r[0]==token else False for r in cursor]):
               raise Exception('Token is not valid!')
            cursor.close()
         except MySQLdb.Error, e:
            io.debug.log("Update of mySQL failed (access validation)!",
                      "No record of this run was made!",
                      "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
         except Exception, e:
            io.debug.log("Update of mySQL failed (access validation)!",
                      "No record of this run was made!", e)
         else:
            try:
               cursor = conn.cursor()
               cursor.execute("""SELECT i.isolate_path
                                 FROM  isolates i
                                 WHERE i.owner = %s
                                   AND i.id = %s
                                 ;""", (uid, iid,))
               ifolder = cursor.fetchone()[0]
               cursor.close()
            except MySQLdb.Error, e:
               io.debug.log("MySQL failed!",
                         "No record of this run was made!",
                         "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
            except Exception, e:
               io.debug.log("MySQL failed!",
                         "The isolate does not belong to the user!",
                         "No record of this run was made!",
                         e)
               io.debug.GraceFullExit("Error: The isolate does not belong to the user!\n")
            else:
               # UPDATE DB
               try:
                  # Get location from IP
                  loc = geocoder.ip(ip).address

                  cursor = conn.cursor()
                  cursor.execute("""INSERT INTO services
                                    (isolate_id, owner, service, date, folder, ip,
                                    location, other, status)
                                    VALUES (%s, %s, %s, DATE(NOW()), %s, %s, %s, %s,
                                    'In progress')
                                    ;""", (iid, uid, service+'-'+version,
                                          service+'_NA', ip, loc, other))
                  # RETRIEVING SERVICE ID
                  cursor.execute('SELECT LAST_INSERT_ID();')
                  serviceID = int(cursor.fetchone()[0])
                  cursor.close()
               except MySQLdb.Error, e:
                  io.debug.log("Update of mySQL failed!",
                            "No record of this run was made!",
                            "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
               except Exception, e:
                  io.debug.log("Update of mySQL failed!",
                            "No record of this run was made!",
                            e)
               else:
                  # COMMIT CHANGES TO DB
                  conn.commit()
         finally:
            # CLOSE CONNECTION
            conn.close()
   else:
      io.debug.log("No record of this file was made!",
                "Isolate ID and/or user name and session id was not provided!")
   return serviceID, ifolder


def UpdateServiceInDB(service, sid, status, results=None):
   """ This function updates the mySQL database entry for the given service,
   thus finilising the service process in regards to the database."""
   io.debug.log('Updating DB - Updating Service.')
   if not isinstance(results, dict):
      if results is not None: io.debug.log("The results provided was not in a dictionary format!\n%s"%(str(results)))
      results = ''
   else: results = json.dumps(results)
   if sid:
      # CONNECT TO SQL DATABASE
      try:
         conn = MySQLdb.connect(host   = "cge",
                                user   = "cgeclient",
                                passwd = "www",
                                db     = "cge")
      except MySQLdb.Error, e:
         io.debug.log("No update was made!",
                   "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
      else:
         # UPDATE DB
         try:
            cursor = conn.cursor()
            cursor.execute("""UPDATE services
                              SET folder  = %s,
                                  status  = %s,
                                  results = %s
                              WHERE id = %s
                              ;""", ('%s_%s'%(service, sid), status, results, sid))
            cursor.close()
         except MySQLdb.Error, e:
            io.debug.log("No update was made!",
                      "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
         else:
            # COMMIT CHANGES TO DB
            conn.commit()
         finally:
            # CLOSE CONNECTION
            conn.close()


def AddFile2DB(iid, sid, usr, name, path, desc):
   """ This function creates a mySQL database entry for the service, and returns
   the service ID. Note: the service folder has not been created yet, and an
   update is required later to update this. """
   io.debug.log('Updating DB - Adding File.')
   # VALIDATE INPUTS
   if iid:
      fileID = None
      # CONNECT TO SQL DATABASE
      try:
         conn = MySQLdb.connect(host   = "cge",
                                user   = "cgeclient",
                                passwd = "www",
                                db     = "cge")
      except MySQLdb.Error, e:
         io.debug.log("No record of this file was made!",
                   "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
      else:
         # GET USER ID
         try:
            cursor = conn.cursor()
            cursor.execute("""SELECT u.id
                              FROM users u
                              WHERE u.usr = %s
                              ;""", (usr,))
            r = cursor.fetchone()
            uid = int(r[0])
            cursor.close()
         except MySQLdb.Error, e:
            io.debug.log("No data was retrieved!",
                      "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
         except Exception, e:
            io.debug.log("No data was retrieved!", e)
         else:
            # UPDATE DB
            try:
               cursor = conn.cursor()
               cursor.execute("""INSERT INTO files
                                 (isolate_id, service_id, owner, date, name,
                                 path, description)
                                 VALUES (%s, %s, %s, DATE(NOW()), %s, %s, %s)
                                 ;""", (iid, sid, uid, name, path, desc))
               # RETRIEVING SERVICE ID
               cursor.execute('SELECT LAST_INSERT_ID();')
               fileID = int(cursor.fetchone()[0])
               cursor.close()
            except Exception, e: # MySQLdb.Error
               io.debug.log("Creation of file entry failed!",
                         "No record of this file was made!",
                         "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
            else:
               # COMMIT CHANGES TO DB
               conn.commit()
         finally:
            # CLOSE CONNECTION
            conn.close()
      return fileID
   else:
      io.debug.log("No record of this file was made!",
                 "Service ID and/or Isolate ID is not available!")
      return None


def GetServiceResults(iid, usr):
   """ This function queries the database for service results and return them. """
   io.debug.log('\nGet Resulst from DB')
   # VALIDATE INPUTS
   if iid:
      fileID = None
      # CONNECT TO SQL DATABASE
      try:
         conn = MySQLdb.connect(host   = "cge",
                                user   = "cgeclient",
                                passwd = "www",
                                db     = "cge")
      except MySQLdb.Error, e:
            io.debug.log("No data was retrieved!",
                    "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
      else:
         # GET USER ID
         try:
            cursor = conn.cursor()
            cursor.execute("""SELECT u.id
                              FROM users u
                              WHERE u.usr = %s
                              ;""", (usr,))
            r = cursor.fetchone()
            uid = int(r[0])
            cursor.close()
         except MySQLdb.Error, e:
            io.debug.log("No data was retrieved!",
                       "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
         except Exception, e:
            io.debug.log("No data was retrieved!", e)
         else:
            # UPDATE DB
            try:
               cursor = conn.cursor()
               cursor.execute("""SELECT service, results
                                 FROM services
                                 WHERE isolate_id = %s
                                   AND owner = %s
                                 ;""", (iid, uid))
               # RETRIEVING SERVICE ID
               results = cursor.fetchall()
               cursor.close()
            except Exception, e: # MySQLdb.Error
               io.debug.log("Fetching the results failed!",
                          "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
            else:
               # COMMIT CHANGES TO DB
               conn.commit()
      finally:
         # CLOSE CONNECTION
         conn.close()
      data = {}
      for x in results:
         if x[1] is not None and x[1] is not '':
            key = x[0].split('-')[0].lower()
            if not key in data: data[key] = json.loads(x[1])
            else:
               if isinstance(data[key], list): data[key].append(json.loads(x[1]))
               else: data[key] = [data[key], json.loads(x[1])]
      return data
   else:
      io.debug.log("No data was retrieved!",
                 "Isolate ID was not provided!")
      return None


def GetIsolateDir(usr, token, iid):
   """ This function retrieves isolate directory from the isolates table in the
   mysql database."""
   io.debug.log('Get Isolate Directory...')
   results = None
   # Validate Input
   if usr and token and iid and isinstance(iid, int):
      # CONNECT TO SQL DATABASE
      try:
         conn = MySQLdb.connect(host   = "cge",
                                user   = "cgeclient",
                                passwd = "www",
                                db     = "cge")
      except MySQLdb.Error, e:
         io.debug.log("No data was retrieved!",
                   "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
      else:
         try:
            # VERIFY USER EXSISTENCE
            cursor = conn.cursor()
            io.debug.log("""SELECT id
                         FROM users
                         WHERE usr = %s
                         ;"""%(usr))
            cursor.execute("""SELECT id
                              FROM users
                              WHERE usr = %s
                              ;""", (usr,))
            uid = int(cursor.fetchone()[0])
            # VERIFY USER ACCESS
            io.debug.log("""SELECT token
                         FROM batch_access
                         WHERE user_id = %s
                         AND expiration_date >= now()
                         ;"""%(uid))
            cursor.execute("""SELECT token
                              FROM batch_access
                              WHERE user_id = %s
                              AND expiration_date >= now()
                              ;""", (uid,))
            if not any([True if r[0]==token else False for r in cursor]):
               raise Exception('Token is not valid!')
            cursor.close()
         except MySQLdb.Error, e:
            io.debug.log("MySQL failed (access validation)!",
                      "No data was retrieved!",
                      "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
         except Exception, e:
            io.debug.log("MySQL failed (access validation)!",
                      "No data was retreived!", e)
         else:
            # Retrieve data from DB
            try:
               cursor = conn.cursor()
               cursor.execute("""SELECT isolate_path
                                 FROM isolates
                                 WHERE id = %s
                                 AND owner = %s
                                 ;""", (iid, uid))
               results = cursor.fetchone()[0]
               cursor.close()
            except MySQLdb.Error, e:
               io.debug.log("No data was retrieved!",
                         "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
            else:
               if results is None or results == '' or len(results) < 32:
                  results = None
                  io.debug.log('Error: Found no isolate directory!')
         finally:
            # CLOSE CONNECTION
            conn.close()
   return results
