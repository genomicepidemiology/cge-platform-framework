#!/usr/bin/env python
""" THIS MODULE CONTAINS ALL THE SHARED WRAPPER FUNCTIONS """
################################################################################
#                              CGE FUNCTION MODULE                             #
################################################################################
# This script is part of the CGE Pipeline structure
import sys, os, gzip
# CGE modules
import dbmanager
import utility 

##change
   #setDebug ==> debug.setup(debug=True, webmode=False, logfile=paths['logs']+"wrapper.log", stdout=paths['outputs']+service+".out", stderr=None)
   #printOut ==> debug.printOut
   #printDebug ==> debug.log
   #GraceFullExit ==> debug.GraceFullExit


############# CLASSES #############
class Debug():
   """ Debug object, keeps track of all debug related matters, and provides easy
   access to logging debug text

   USAGE
      import os, sys
      from dbmanager import dbmanager.UpdateServiceInDB
      from utility import utility.FixPermissions
      debug = Debug()
      debug.setup(debug=True, webmode=False, logfile=None, stdout=None, stderr=None)
      debug.printOut('hello')
      debug.logNoNewline('Oh')
      debug.logNoNewline(' my')
      debug.log(' god!')
      # The graceful exit is only meant to be used with the integrated CGE platform
      debug.GraceFullExit(service, serviceID, 'error message')
   IMPORTING THE DEBUG OBJECT
      from io import debug
   """
   def __init__(self):
      """  """
      self.debug = True
      self.webmode = False
      self.logfile = sys.stdout
      self.stdout = sys.stdout
      self.stderr = sys.stderr
      self.page_error = None
   def setup(self, debug=None, webmode=None, logfile=None, stdout=None, stderr=None):
      """  """
      if debug   is not None: self.debug   = debug
      if webmode is not None: self.webmode = webmode
      if logfile is not None: self.logfile = logfile
      if stdout  is not None: self.stdout  = stdout
      if stderr  is not None: self.stderr  = stderr
   def printOut(self, *lst):
      """ Print list of strings to the predefined stdout. """
      self.print2file(self.stdout, True, True, *lst)
   def printErr(self, *lst):
      """ Print list of strings to the predefined stdout. """
      self.print2file(self.stderr, False, True, *lst)
   def print2file(self, logfile, print2screen, addLineFeed, *lst):
      """ This function prints to the screen and logs to a file, all the strings
      given.
      # print2screen eg. True, *lst is a commaseparated list of strings
      """
      if addLineFeed:
         if self.webmode: linefeed = '<br>\n'
         else: linefeed = '\n'
      else: linefeed = ''
      if print2screen: print linefeed.join(str(string) for string in lst)
      if isinstance(logfile, file):
         logfile.write(linefeed.join(str(string) for string in lst) + linefeed)
      elif isinstance(logfile, str) and os.path.exists(logfile):
         with open(logfile, 'a') as f:
            f.write(linefeed.join(str(string) for string in lst) + linefeed)
      elif not print2screen: # Print to screen if there is no outputfile
         print linefeed.join(str(string) for string in lst)
   def log(self, *lst):
      """ Print list of strings to the predefined logfile if debug is set. and
      sets the page_error message if an error is found
      """
      self.print2file(self.logfile, self.debug, True, *lst)
      if 'Error' in '\n'.join([str(x) for x in lst]):
         self.page_error = '\n'.join([str(x) for x in lst])
   def logNoNewline(self, msg):
      """ print the message to the predefined log file without newline """
      self.print2file(self.logfile, False, False, msg)
   def GraceFullExit(self, service, serviceID, msg):
      """ This function Tries to update the MSQL database before exiting. """
      # Fix Permissions
      try: utility.FixPermissions()
      except: self.print2file(self.stderr, False, True, 'Fix permissions failed!')
      # Update the SQL entry (ONLY PIPELINE)
      try: dbmanager.UpdateServiceInDB(service, serviceID, 'Failure')
      except: self.print2file(self.stderr, False, True, 'dbmanager.UpdateServiceInDB failed!')
      # Print stored errors to stderr
      self.print2file(self.stderr, False, False, self.page_error)
      # Kill process with error message
      sys.exit(msg)

############# ITERATORS #############
def SeqsFromFile(filename, exit_on_err=False):
   """Extract sequences from a file

   Name:
      SeqsFromFile
   Author(s):
      Martin CF Thomsen
   Date:
      18 Jul 2013
   Description:
      Iterator which extract sequence data from the input file
   Args:
      filename: string which contain a path to the input file
   Supported Formats:
      fasta, fastq

   USAGE:
   >>> import os, sys
   >>> # Create fasta test file
   >>> file_content = ('>head1 desc1\nthis_is_seq_1\n>head2 desc2\n'
                       'this_is_seq_2\n>head3 desc3\nthis_is_seq_3\n')
   >>> with open('test.fsa', 'w') as f: f.write(file_content)
   >>> # Parse and print the fasta file
   >>> for seq, name, desc in SeqsFromFile('test.fsa'):
   ...    print ">%s %s\n%s"%(name, desc, seq)
   ...
   >head1 desc1
   this_is_seq_1
   >head2 desc2
   this_is_seq_2
   >head3 desc3
   this_is_seq_3
   """
   # VALIDATE INPUT
   if not isinstance(filename, str):
      msg = 'Filename has to be a string.'
      if exit_on_err: sys.exit('Error: '+msg)
      else: raise IOError, msg
   if not os.path.exists(filename):
      msg = 'File "%s" does not exist.'%filename
      if exit_on_err: sys.exit('Error: '+msg)
      else: raise IOError, msg

   # EXTRACT DATA
   with open_(filename,"r") as f:
      queryseqsegments = []
      seq, name, desc = '', '', ''
      line = ''
      nextline = f.next
      addsegment = queryseqsegments.append
      for line in f:
         if len(line.strip()) == 0: continue
         #sys.stderr.write("%s\n"%line)
         fields=line.strip().split()
         if line[0] == ">":
            # FASTA HEADER FOUND
            if queryseqsegments != []:
               # YIELD SEQUENCE AND RESET
               seq = ''.join(queryseqsegments)
               yield (seq, name, desc)
               seq, name, desc = '', '', ''
               del queryseqsegments[:]
            name = fields[0][1:]
            desc = ' '.join(fields[1:])

         elif line[0] == "@":
            # FASTQ HEADER FOUND
            name = fields[0][1:]
            desc = ' '.join(fields[1:])
            try:
               # EXTRACT FASTQ SEQUENCE
               line = nextline()
               seq  = line.strip().split()[0]
               # SKIP SECOND HEADER LINE AND QUALITY SCORES
               line = nextline()
               line = nextline() # Qualities
            except:
               break
            else:
               # YIELD SEQUENCE AND RESET
               yield (seq, name, desc)
               seq, name, desc = '', '', ''

         elif len(fields[0])>0:
            # EXTRACT FASTA SEQUENCE
            addsegment(fields[0])

      # CHECK FOR LAST FASTA SEQUENCE
      if queryseqsegments != []:
         # YIELD SEQUENCE
         seq = ''.join(queryseqsegments)
         yield (seq, name, desc)


############# FUNCTIONS #############
def open_(filename, mode=None, compresslevel=9):
   """Switch for both open() and gzip.open().

   Determines if the file is normal or gzipped by looking at the file
   extension.

   The filename argument is required; mode defaults to 'rb' for gzip and 'r'
   for normal and compresslevel defaults to 9 for gzip.

   """
   if filename[-3:] == '.gz':
      if mode is None: mode = 'rb'
      return gzip.open(filename, mode, compresslevel)
   else:
      if mode is None: mode = 'r'
      return open(filename, mode)

def dlButton(buttonText='results', requestedFile='', fid='', iid='', sid='', date=''):
   """ This function will create and print a download button for webside
       inaccessible files
   """
   if fid != '': # dlButton('buttonText', fid='file ID')
      debug.printOut("\n<form "+
         "action='/tools_new/client/platform/download_files.php' "+
         "method='post'>",
         "   <input type='hidden' name='FID' value='%s'>"%(fid),
         "   <input type='submit' value='%s'>"%(buttonText),
         "</form>\n")
   elif iid: # dlButton('buttonText', 'filename.ext', iid='isolate id', sid='service id', date='file creation date')
      #debug.printOut("\n<form action='/cge/user/services/download_files.php' "+
      debug.printOut("\n<form "+
         "action='/tools_new/client/platform/download_files.php' "+
         "method='post'>",
         "   <input type='hidden' name='SERVICE' value='%s'>"%(service),
         "   <input type='hidden' name='VERSION' value='%s'>"%(version),
         "   <input type='hidden' name='FILENAME' value='%s'>"%(requestedFile),
         "   <input type='hidden' name='IID' value='%s'>"%(iid),
         "   <input type='hidden' name='SID' value='%s'>"%(sid),
         "   <input type='hidden' name='DATE' value='%s'>"%(date),
         "   <input type='submit' value='%s'>"%(buttonText),
         "</form>\n")
   elif 'runID' in globals(): # dlButton('buttonText', 'filename.ext')
      #debug.printOut("\n<form action='/cge/download_files.php' method='post' style='display:inline;'>",
      debug.printOut("\n<form "+
         "action='/tools_new/client/platform/download_files.php' method='post'"+
         " style='display:inline;'>",
         "  <input type='hidden' name='service' value='"+ service +"'>",
         "  <input type='hidden' name='version' value='"+ version +"'>",
         "  <input type='hidden' name='filename' value='"+ requestedFile +"'>",
         "  <input type='hidden' name='pathid' value='/"+ runID +"/'>",
         "  <input type='submit' value='"+ buttonText +"'>",
         "</form>\n")

def extendedOutButton(buttonText='extended output', table=''):
   """ This function will create and print a extended output button for website
   """
   # define java function and print button:
   debug.printOut("<script type=\"text/javascript\">",
      "function printOutput(){",
      "if (document.getElementById(\"demo\").style.display=='inline')",
      "{",
      "document.getElementById(\"demo\").style.display='none';",
      "}",
      "else",
      "{",
      "document.getElementById(\"demo\").style.display='inline';",
      "}",
      "}",
      "</script>",
      "<br>",
      "<center>",
      "<button type=\"button\" onclick=\"printOutput()\">%s</button>"%(buttonText),
      "</center>"
      "<div id=\"demo\" style=\"display:none\">",
      "<br>")
   # print table
   tsv2html(table, full_dl=table, css_classes='redhead')

   # finish:
   debug.printOut("</div>",
            "<br>")

def linkButton(url, buttonText):
   """ This function will create and print a download button for online
       available files using url links
   """
   debug.printOut("<form action='"+ url +"' method='get'>\n",
                  "  <input type='submit' value='"+ buttonText +"'>\n",
                  "</form>\n")

def printInputFilesHtml(inputFiles):
   """ This function will write in html the input files given for the execution
   """
   debug.log("Printing Input Files: %s"%(', '.join(inputFiles) if isinstance(inputFiles, list) else str(inputFiles))) #DEBUG
   if len(inputFiles)>0:
      debug.printOut("<h2>Input Files: ")
      debug.printOut("<i class='grey'>")
      string = ''
      for el in inputFiles:
         string += "&nbsp;"*2 + os.path.basename(el)
         if len(string) >= 300:
            debug.printOut(string +"<br>\n"+ "&nbsp;"*13)
            string = ''
      if string != '': debug.printOut(string)
      debug.printOut("</i>")
      debug.printOut("</h2>")

def tsv2html(fp, full_dl='', css_classes='greyhead'):
   debug.log('Writing HTML table for %s'%fp)
   if os.path.exists(fp):
      with open(fp, 'r') as f:
         intable = False
         tsize = 0
         ttitle = ''
         for l in f:
            l = l.strip()
            if l == '':
               if intable:
                  # END TABLE
                  debug.printOut("</table>\n")
                  intable = False
            elif l[0] == '#':
               # HEADER LINE
               tmp = l[1:].split('\t')
               if intable: pass # ignore as comment
               elif len(tmp) > 1:
                  # Start Table
                  tsize = len(tmp)
                  debug.printOut("<table class='results %s'>\n"%(css_classes))
                  # Print Title Line
                  if ttitle != '':
                     debug.printOut("<tr><th colspan='%s'>%s\n"%(tsize, ttitle))
                     if full_dl != '': dlButton('View All', full_dl+'.gz')
                     debug.printOut("</th></tr>\n")
                  # Print Header Line
                  debug.printOut("<tr><th>%s</th></tr>\n"%("</th><th>".join(tmp)))
                  intable = True
               else:
                  # Create new table
                  ttitle = tmp[0]
            elif intable:
               # print table row
               td = l.split('\t')
               tdl = len(td)
               debug.printOut("<tr><td>%s</td></tr>\n"%('</td><td>'.join([str(td[i]) if i < tdl else '&nbsp;' for i in xrange(tsize)])))
         # END TABLE IF OPEN
         if intable: debug.printOut("</table>\n")

# Initiate
debug = Debug()

#def getGlobal(var): # Only used by assembly module
#   """ This function returns the wanted global variable """
#   return globals()[var]
#
#def setGlobal(var, val): # Not used
#   """ This function returns the wanted global variable """
#   globals()[var] = val
