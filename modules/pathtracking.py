#!/usr/bin/env python
""" THIS MODULE CONTAINS ALL THE SHARED WRAPPER FUNCTIONS """
################################################################################
#                              CGE FUNCTION MODULE                             #
################################################################################
# This script is part of the CGE Pipeline structure
import sys, os
# CGE modules
import io

############# CLASSES #############
class paths_obj:
   """ This Class keeps track of all paths
      USAGE:
         paths.add_path(name, path)
         paths.replace_placeholder(tag, value)
         paths[name]
   """
   def __init__(self):
      self.programRoot = "/home/data1/services/{service}/{service}-{version}/"
      self.database    = "/home/data1/services/{service}/database/"
      self.scripts     = "{programRoot}scripts/"
      self.isolateRoot = "{ifolder}/"
      self.groupRoot   = "{gfolder}/"
      self.serviceRoot = "{isolateRoot}services/{serviceFolder}/"
      self.analysisRoot = "{groupRoot}analysis/{analysisFolder}/"
      self.inputs      = "{serviceRoot}inputs/"
      self.outputs     = "{serviceRoot}outputs/"
      self.downloads   = "{serviceRoot}downloads/"
      self.logs        = "{serviceRoot}logs/"
      self.groupinputs  = "{analysisRoot}inputs/"
      self.groupoutputs = "{analysisRoot}outputs/"
      self.groupdownloads = "{analysisRoot}downloads/"
      self.grouplogs   = "{analysisRoot}logs/"
      self.python      = "/home/data1/tools/bin/anaconda/bin/python2.7" #"/tools/bin/python2.7" #/tools/bin/python2.7 /usr/local/anaconda/bin/python2.7 /home/data1/tools/bin/anaconda/bin/python
      self.perl        = "/tools/bin/perl" # perl -> perl
      self.www         = "/services/{service}-{version}/" # Service web path from the server document root
   def __getitem__(self, key):
      try:
         v = getattr(self, key)
      except:
         io.debug.log('Error: Path missing for '+str(key))
         return ''
      else:
         return v
   def add_path(self, name, path):
      setattr(self, name, path)
   def replace_placeholder(self, tag, value):
      """ Replacing the tag with the value in all paths """
      if tag != '' and value is not None:
         for k, v in self.__dict__.iteritems():
            setattr(self, k, v.replace('{'+tag+'}',value))
   def Create(self, key):
      """ Validate and create path. """
      try:
         mkpath(getattr(self, key))
      except NameError, e:
         io.debug.log('Error: Path missing %s\n%s'%(key, e))
         return False
      except IOError, e:
         io.debug.log('Error: Invalid path %s\n%s'%(key, e))
         return False
      else:
         return True

# Initialising paths
paths = paths_obj()

def UpdatePaths(s, v, sid, ifolder, wm):
   """ This function creates all service required directories and adds them to
   paths:
      * service root
      * inputs
      * outputs
      * downloads
      * logs
   """
   # Set Globals Arguments
   global webmode, service, version, serviceID
   serviceID = sid
   service = s
   version = v
   webmode = wm

   # Update paths, by replacing the placeholder tags of service and version
   paths.replace_placeholder('service', service)
   paths.replace_placeholder('version', version)
   paths.replace_placeholder('programRoot', paths['programRoot'])
   paths.replace_placeholder('ifolder', ifolder)
   paths.replace_placeholder('isolateRoot', paths['isolateRoot'])
   paths.replace_placeholder('serviceFolder', '%s_%s'%(service, serviceID))
   paths.replace_placeholder('serviceRoot', paths['serviceRoot'])
   paths.replace_placeholder('analysisFolder', '%s_%s'%(service, serviceID)) ##Missing two variables
   paths.replace_placeholder('analysisRoot', paths['analysisRoot'])

def createIsolateDirs(ifolder, s, v):
   """ This function will create all the needed directories to execute the run
       of the webservice
   """
   global service, version
   service = s
   version = v

   io.debug.log("Creating Necessary Directories...") #io.debug
   # Set / Update Paths
   paths.add_path('services', '{isolateRoot}services/')
   paths.add_path('uploads', '{isolateRoot}uploads/')
   paths.add_path('logs', '{isolateRoot}logs/')
   paths.replace_placeholder('service', service)
   paths.replace_placeholder('version', version)
   paths.replace_placeholder('ifolder', ifolder)
   paths.replace_placeholder('isolateRoot', paths['isolateRoot'])
   # Create Directories
   stat = [False]*4
   stat[0] = paths.Create('isolateRoot')
   stat[1] = paths.Create('services')
   stat[2] = paths.Create('uploads')
   stat[3] = paths.Create('logs')
   if not all(stat):
      io.debug.GraceFullExit('Error: Unable to create the isolate directories!') # sys.exit

   # Change Working Directory to Service Directory
   os.chdir(paths['isolateRoot'])

   # CREATING WRAPPER error LOGFILE
   if not os.path.exists(paths['logs'] +"wrapper.log"):
      if os.path.exists(paths['logs']):
         with open(paths['logs'] +"wrapper.log", 'w') as f:
            # LOG RUN ENVIRONMENT
            f.write(("\nPython Version\t%s\nSystem Arguments ::\t%s\nENV->HOME::\t"
                     "%s\nENV->PATH::\t%s\nCurrent Working Directory::\t%s\nHOST::"
                     "\t%s\n\n")%
               (sys.version, " ".join(sys.argv), os.getenv("HOME"),
                os.getenv("PATH"), os.getcwd(), os.getenv('HOSTNAME'))) #io.debug
      else:
         sys.exit('Error: Log directory did not exist!')

def createGroupDirs(gfolder, s, v):
   """ This function will create all the needed directories to execute the run
       of the webservice
   """
   global analysis, version
   analysis = s
   version = v

   io.debug.log("Creating Necessary Directories...") #io.debug
   # Set / Update Paths
   paths.add_path('analysis', '{groupRoot}analysis/')
#   paths.add_path('analysis', paths['groupRoot']+'/analysis/')
   paths.add_path('uploads', '{groupRoot}/uploads/')
   paths.add_path('logs', '{groupRoot}/logs/')
   paths.replace_placeholder('analysis', analysis)
   paths.replace_placeholder('version', version)
   paths.replace_placeholder('gfolder', gfolder)
   paths.replace_placeholder('groupRoot', paths['groupRoot'])
   # Create Directories
   stat = [False]*4
   stat[0] = paths.Create('groupRoot')
   stat[1] = paths.Create('analysis')
   stat[2] = paths.Create('uploads')
   stat[3] = paths.Create('logs')
#   stat[4] = paths.Create('downloads')
   if not all(stat):
      sys.exit('Error: Unable to create the isolate directories!')

   # Change Working Directory to Service Directory
   os.chdir(paths['groupRoot'])

   # CREATING WRAPPER error LOGFILE
   if not os.path.exists(paths['logs'] +"wrapper.log"):
      if os.path.exists(paths['logs']):
         with open(paths['logs'] +"wrapper.log", 'w') as f:
            # LOG RUN ENVIRONMENT
            f.write(("\nPython Version\t%s\nSystem Arguments ::\t%s\nENV->HOME::\t"
                     "%s\nENV->PATH::\t%s\nCurrent Working Directory::\t%s\nHOST::"
                     "\t%s\n\n")%
               (sys.version, " ".join(sys.argv), os.getenv("HOME"),
                os.getenv("PATH"), os.getcwd(), os.getenv('HOSTNAME'))) #io.debug
      else:
         sys.exit('Error: Log directory did not exist!')

def createServiceDirs():
   """ This function will create all the needed directories to execute the run
       of the webservice
   """
   io.debug.log("Creating Necessary Directories...") #io.debug
   # Check if the Isolate folder exists
   if not os.path.exists(paths['isolateRoot']):
      io.debug.printOut("Error: the directory %s does not exist and the needed "
               "directories could not be created!\n"%(paths['isolateRoot']))
      sys.exit(1)
   # Create Directories
   stat = [False]*5
   stat[0] = paths.Create('serviceRoot')
   stat[1] = paths.Create('inputs')
   stat[2] = paths.Create('outputs')
   stat[3] = paths.Create('downloads')
   stat[4] = paths.Create('logs')
   if not all(stat):
      sys.exit('Error: Unable to create the service directories!')

   # Change Working Directory to Service Directory
   os.chdir(paths['serviceRoot'])

   # CREATING WRAPPER ERROR LOGFILE
   if not os.path.exists(paths['logs'] +"wrapper.log"):
      with open(paths['logs'] +"wrapper.log", 'w') as f:
         # LOG RUN ENVIRONMENT
         f.write(("\nPython Version\t%s\nSystem Arguments ::\t%s\nENV->HOME::\t"
                  "%s\nENV->PATH::\t%s\nCurrent Working Directory::\t%s\nHOST::"
                  "\t%s\n\n")%
            (sys.version, " ".join(sys.argv), os.getenv("HOME"),
             os.getenv("PATH"), os.getcwd(), os.getenv('HOSTNAME'))) #io.debug
