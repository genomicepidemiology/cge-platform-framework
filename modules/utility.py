#!/usr/bin/env python
""" THIS MODULE CONTAINS ALL THE SHARED WRAPPER FUNCTIONS """
################################################################################
#                              CGE FUNCTION MODULE                             #
################################################################################
# This script is part of the CGE Pipeline structure
import os, gzip, shutil, glob
from subprocess import Popen
from zipfile import ZipFile
# CGE modules
import io

############# CLASSES #############
class adv_dict(dict):
   """ This class expands on the dictionary class by adding the gettree class
   method.
   """
   def gettree(self, list_of_keys):
      """ gettree will extract the value from a nested tree

      INPUT
         list_of_keys: a list of keys ie. ['key1', 'key2']
      EXAMPLE
      >>> adv_dict({'key1': {'key2': 'value'}}).gettree(['key1', 'key2'])
      'value'
      """
      cur_obj = self
      for key in list_of_keys:
         cur_obj = cur_obj.get(key)
         if not cur_obj: break
      return cur_obj

class Reg:
   """
   NAME:        Reg - Extended Regular Expression Handler
   AUTHOR:      Martin Thomsen
   DESCRIPTION:
      This class enables a simplistic usage of regular expression to get
      contained groups in a match statement. But it also allows to do some of
      the normal re call, such as findall and sub.
   DEPENDENCIES:
      re (regular expression module)
   USAGE:
      >>> import re
      >>> RegEx = Reg(pattern, flag)
      >>> if RegEx.match(string):
      >>>    RegEx.getgroup(index)
   EXAMPLE:
      >>> RegEx = Reg('[^a]*(a)[^b]*(b)[^Y]*(Y)(a)?', 'I')
      >>> if RegEx.match('aBcdefgHIJKLmnOpqrstuvwxyz'):
      ...    print(RegEx.getgroup(0), # index=0 -> full match
      ...          RegEx.getgroup(1),
      ...          RegEx.getgroup(2),
      ...          RegEx.getgroup(3),
      ...          RegEx.getgroup(4))
      ...
      ('aBcdefgHIJKLmnOpqrstuvwxy', 'a', 'B', 'y', None)
      # NIFTY SUBSTITUTION LOOP
      >>> string = 'There are {%=count%} {%=animal%} on the {%=location%}!'
      >>> # Dictionary containing place-holders and values
      ... # (make sure all placeholders in the string is included!)
      ... d = { 'count': 5, 'animal': 'cows', 'location': 'Battle Field' }
      >>> # RE Object which matches place-holders in the string
      ... tmpPH = Reg('\{\%\=(\w+)\%\}', 'I')
      >>> # substitute all placeholders
      ... while tmpPH.match(string): string = tmpPH.sub(str(d[tmpPH.getgroup(1)]), string, 1)
      ...
      >>> print(string)
      There are 5 cows on the Battle Field!
      """
   def __init__(self, pattern, *flags):
      sd = {'T': 1, 'I': 2, 'L':4,'M':8,'S':16,'U':32,'X':64}
      try:
         flag = sum([sd[f] if f in sd else int(f) for f in set(flags)]) if flags else 0
      except:
         for f in flags:
            if not isinstance(f, int) and not f in sd:
               raise Exception("Error: Unrecognised flag argument '%s' for Reg call."%f)
         flag=0
      if flag: self.re = re.compile(pattern, flag)
      else: self.re = re.compile(pattern)
      self.matches = None
   def sub(self, replace, string, count=0):
      """ returns new string where the matching cases (limited by the count) in
      the string is replaced. """
      return self.re.sub(replace, string, count)
   def findall(self, s):
      """ Finds all matches in the string and returns them in a tuple. """
      return self.re.findall(s)
   def match(self, s):
      """ Matches the string to the stored regular expression, and stores all
      groups in mathches. Returns False on negative match. """
      self.matches = self.re.search(s)
      return self.matches
   def getgroup(self, x):
      """ Returns requested subgroup. """
      return self.matches.group(x)
   def getgroups(self):
      """ Returns all subgroups. """
      return self.matches.groups()

class REgroup_obj():
   """ Regular Expression group object

   This class simplyfies the use of groups for the Sort2Groups function.
   """
   def __init__(self, pattern, flags=''):
      self.re = Reg(pattern, flags)
      self.list = []
   def Match(self, s):
      """ Matching the pattern to the input string, returns True/False and
          saves the matched string in the internal list
      """
      if self.re.match(s):
         self.list.append(s)
         return True
      else: return False

def Sort2Groups(array, gpat=['_R1','_R2']):
   """ Sort an array of strings to groups by patterns """
   groups = [REgroup_obj(gp) for gp in gpat]
   unmatched = []
   for item in array:
      matched = False
      for m in groups:
         if m.Match(item):
            matched = True
            break
      if not matched: unmatched.append(item)
   return [sorted(m.list) for m in groups], sorted(unmatched)

def SortAndDistribute(array, splits=2):
   """ Sort an array of strings to groups by alphabetically continuous
       distribution
   """
   if not isinstance(array, (list,tuple)): raise TypeError("array must be a list")
   if not isinstance(splits, int): raise TypeError("splits must be an integer")
   remaining = sorted(array)
   myxrange = xrange(splits)
   groups = [[] for i in myxrange]
   while len(remaining) > 0:
      for i in myxrange:
         if len(remaining) > 0: groups[i].append(remaining.pop(0))
   return groups

def mkpath(filepath, permissions=0777):
   """ This function executes a mkdir command for filepath and with permissions
   (octal number with leading 0 or string only)
   # eg. mkpath("file/path", "775")
   """
   # Converting string of octal to integer, if string is given.
   if isinstance(permissions, str):
      permissions = sum([int(x)*8**i for i,x in enumerate(reversed(permissions))])
   # Creating directory
   if not os.path.exists(filepath):
      debug.log("mkpath CMD:: "+ filepath +" (permissions:"+ str(permissions)
                 +")") #DEBUG
      os.makedirs(filepath, permissions)
   else:
      debug.log("WARNING :: the directory "+ filepath +" already exists") #DEBUG
   return filepath

def createZipDir(zipFilePath, *fileList):
   """ This function creates a zipfile located in zipFilePath with the files in
   the file list
   # fileList can be both a comma separated list or an array
   """
   try:
      if isinstance(fileList, (list, tuple)): #unfolding list of list or tuple
         if len(fileList) == 1:
            if isinstance(fileList[0], (list, tuple)): fileList = fileList[0]
      #converting string to iterable list
      if isinstance(fileList, str): fileList = [fileList]
      if fileList:
         with ZipFile(zipFilePath, 'w') as zf:
            for curFile in fileList:
               if '/' in curFile:
                  os.chdir('/'.join(curFile.split('/')[:-1]))
               elif '/' in zipFilePath:
                  os.chdir('/'.join(zipFilePath.split('/')[:-1]))
               zf.write(curFile.split('/')[-1])
      else:
         debug.log('Error: No Files in list!',zipFilePath+' was not created!')
   except Exception, e:
      debug.log('Error: Could not create zip dir! argtype: '+
                 str(type(fileList)), "FileList: "+ str(fileList),
                 "Errormessage: "+ str(e))

def fileZipper(rootDir):
   """ This function will zip the files created in the runroot directory and
   subdirectories """
   # SETTING COMPRESSION LEVEL
   gzipCompressionLevel = 9
   # FINDING AND ZIPPING UNZIPPED FILES
   for root, dirs, files in os.walk(rootDir, topdown=False):
      if root != "":
         dirname = root.split("/")
         for currentfile in files:
            filepath = str(root)+"/"+str(currentfile)
            try: #ignore files less than 0 bytes   (1kb?)
               filesize = os.path.getsize(filepath)
            except Exception, e:
               filesize = 0
               debug.log('Error: fileZipper failed to zip following file '+filepath, e)
            finally:
               #excluding gzipped and zipped files
               if (         filesize > 0
                    and     currentfile[-3:] != ".gz"
                    and     currentfile[-4:] != ".zip"
                    and not os.path.islink(filepath)
                  ):
                  #saving a gzipped version
                  with open(filepath, 'rb') as f, gzip.open(filepath+".gz", 'wb', gzipCompressionLevel) as gz:
                     gz.writelines(f)
                  #deleting old (non-zipped) file
                  try: os.remove(filepath)
                  except OSError, e:
                     debug.printOut("WARNING! The file "+ str(filesize) +" could not "+
                              "be removed!", e)
   debug.log("fileZipper :: DONE!")

def fileUnzipper(directory):
   """ This function will unzip all files in the runroot directory and
   subdirectories
   """
   debug.log("Unzipping directory (%s)..."%directory)
   #FINDING AND UNZIPPING ZIPPED FILES
   for root, dirs, files in os.walk(directory, topdown=False):
      if root != "":
         origdir = os.getcwd()
         os.chdir(directory)
         Popen('gunzip -q -f *.gz > /dev/null 2>&1', shell=True).wait()
         Popen('unzip -qq -o "*.zip" > /dev/null 2>&1', shell=True).wait()
         Popen('rm -f *.zip > /dev/null 2>&1', shell=True).wait()
         os.chdir(origdir)

def moveFile(src, dst): #srcFile, destPath+name
   """ this function will simply move the file from the source path to the dest
   path given as input
   """
   # Sanity checkpoint
   src = re.sub('[^\w/\-\.\*]', '', src)
   dst = re.sub('[^\w/\-\.\*]', '', dst)
   if len(re.sub('[\W]', '', src)) < 5 or len(re.sub('[\W]', '', dst)) < 5:
      debug.log("Error: Moving file failed. Provided paths are invalid! src='%s' dst='%s'"%(src, dst))
   else:
      # Check destination
      check = False
      if dst[-1] == '/':
         if os.path.exists(dst):
            check = True # Valid Dir
         else:
            debug.log("Error: Moving file failed. Destination directory does not exist (%s)"%(dst)) #DEBUG
      elif os.path.exists(dst):
         if os.path.isdir(dst):
            check = True # Valid Dir
            dst += '/' # Add missing slash
         else:
            debug.log("Error: Moving file failed. %s exists!"%dst)
      elif os.path.exists(os.path.dirname(dst)):
         check = True # Valid file path
      else:
         debug.log("Error: Moving file failed. %s is an invalid distination!"%dst)
      if check:
         # Check source
         files = glob.glob(src)
         if len(files) != 0:
            debug.log("Moving File(s)...", "Move from %s"%src, "to %s"%dst) #DEBUG
            for file_ in files:
               # Check file exists
               if os.path.isfile(file_):
                  debug.log("Moving file: %s"%file_) #DEBUG
                  shutil.move(file_, dst)
               else:
                  debug.log("Error: Moving file failed. %s is not a regular file!"%file_) #DEBUG
         else: debug.log("Error: Moving file failed. No files were found! (%s)"%src) #DEBUG

def copyFile(src, dst): #srcFile, destPath+name
   """ this function will simply copy the file from the source path to the dest
   path given as input
   """
   # Sanity checkpoint
   src = re.sub('[^\w/\-\.\*]', '', src)
   dst = re.sub('[^\w/\-\.\*]', '', dst)
   if len(re.sub('[\W]', '', src)) < 5 or len(re.sub('[\W]', '', dst)) < 5:
      debug.log("Error: Copying file failed. Provided paths are invalid! src='%s' dst='%s'"%(src, dst))
   else:
      # Check destination
      check = False
      if dst[-1] == '/':
         if os.path.exists(dst):
            check = True # Valid Dir
         else:
            debug.log("Error: Copying file failed. Destination directory does not exist (%s)"%(dst)) #DEBUG
      elif os.path.exists(dst):
         if os.path.isdir(dst):
            check = True # Valid Dir
            dst += '/' # Add missing slash
         else:
            debug.log("Error: Copying file failed. %s exists!"%dst)
      elif os.path.exists(os.path.dirname(dst)):
         check = True # Valid file path
      else:
         debug.log("Error: Copying file failed. %s is an invalid distination!"%dst)
      if check:
         # Check source
         files = glob.glob(src)
         if len(files) != 0:
            debug.log("Copying File(s)...", "Copy from %s"%src, "to %s"%dst) #DEBUG
            for file_ in files:
               # Check file exists
               if os.path.isfile(file_):
                  debug.log("Copying file: %s"%file_) #DEBUG
                  shutil.copy(file_, dst)
               else:
                  debug.log("Error: Copying file failed. %s is not a regular file!"%file_) #DEBUG
         else: debug.log("Error: Copying file failed. No files were found! (%s)"%src) #DEBUG

def copyDir(src, dst):
   """ this function will simply copy the file from the source path to the dest
   path given as input
   #srcDir, destPath[+name]
   """
   try:
      debug.log("copyFile:: from "+ src, "to "+ dst)
      shutil.copytree(src, dst)
   except:
      debug.log("Error:: happened while copying!")

def PrepareDownload(src, iid='', sid='', name='results', usr='Anonymous', desc=''):
   """ Moves file from source (src) to destination (dst), and create a database
   entry of it in SQL. """
   fid = None
   # Check whether source and destination exists
   if os.path.exists(src) and os.path.exists(paths['downloads']):
      # Check whether the file has to be moved
      if os.path.dirname(src) != paths['downloads'].rstrip('/'):
         moveFile(src, paths['downloads'])
      # Add mySQL entry of the downloadable file
      if iid:
         fid = AddFile2DB(iid, sid, usr, name,
                          paths['downloads']+os.path.basename(src), desc)
   else:
      debug.log("No file was found (%s)!"%src)
   return fid

def FixPermissions():
   """ Fixes the permissions for the service root.
   Change directory permissions to 775
   Change file permissions to 664
   """
   if os.path.exists(paths['serviceRoot']):
      os.system("find %s -type d -exec chmod --quiet 755 {} \;"%paths['serviceRoot'])
      os.system("find %s -type f -exec chmod --quiet 644 {} \;"%paths['serviceRoot'])
