#!/usr/bin/env python
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Service structure
#--> The input/arguments are validated
#--> Create service folder and subfolders
#--> The uploaded files are copied to the 'Upload' directory
#--> Log service submission in SQL
#--> Setup and execution of service specific programs
#--> The success of the service is validated
#--> A HTML output page is created if in webmode
#--> Downloadable files are copied/moved to the 'Download' Directory
#--> The SQL service entry is updated
#--> The files are zipped for long-term storage
import sys, os, time, random, subprocess

from time import time, localtime, strftime, sleep

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from assembly_module   import (PrepareAssembly, MakeAssembly,
                               printContigsInfo, AddAssemblyToProgList)
from functions_module  import (printDebug, copyFile, program, createServiceDirs,
                               getArguments, paths, makeFileList, fileUnzipper,
                               printInputFilesHtml, fileZipper, PrepareDownload,
                               UpdatePaths, progs, CheckFileType, printOut,
                               moveFile, setDebug, add2DatabaseLog, dlButton,
                               GraceFullExit, FixPermissions, tsv2html)

#
# Hard-coded paths
#
SNPTreePipelinePath = "scripts/Ks_SNP_tree_pipeline_new.pl" # internal path in the storage root.
MatrixScriptPath = "scripts/Ks_snp_matrix.pl"
FastaToolPath = "/home/people/rkmo/bin/Ks_fasta_tool.pl"


################################################################################
########                         ALT PRG CLASS                         #########
################################################################################

class program_extended(program):
   ''' This Class defines a program structure
      USAGE:
         assembler = proglist.add_program(
            program(name='Assembler',
               path='/panfs1/cge-servers/Assembler/Assembler-1.0'+
                    '/scripts/mlst_wrapper_denovo_2.0.py',
               timer=0,
               ptype='python',
               workdir='',
               ToQueue=False,
               Wait=True,
               args='')
            )
         assembler.AppendArgs(['velvet'])
         assembler.Execute()
   '''
   def __init__(self, name, path=None, timer=0, ptype=None, workDir=None, ssh=True, toQueue=True, wait=False, args=None, walltime=2, mem=4, procs=1, nodes="1"):
      # INIT VALUES
      self.path = path
      self.name = name
      self.timer = timer
      self.ptype = ptype
      self.workDir = workDir
      self.toQueue = toQueue
      self.wait = wait
      self.args = []
      self.stderr = "%s/%s.err"%(paths['logs'], name)
      self.stdout = "%s/%s.out"%(paths['outputs'], name) if os.path.exists(paths['outputs']) else "%s/%s.out"%(paths['logs'], name)
      self.queue = 'cge'
      self.wt = walltime # Maximum amount of hours required to complete program
      self.mem = mem     # GB RAM allocation requirement
      self.procs = procs # Number of processors required for the job
      self.nodes = nodes # Number of nodes, primarily used to control where the job is executed ex. nodes=cgebase
      self.p = None      # The Subprocess Object
      self.ssh = ssh
      self.server = 'cgebase'
      self.status = 'Initialised'
      if not workDir: self.workDir = paths['outputs']
      self.queue = 'www'
      if args: self.AppendArgs(args)
   def Execute(self):
      ''' This function Executes the program with set arguments. '''
      # SETTING WORKDIR if not set
      if self.toQueue:
         otherArgs = ''
         if self.wait: otherArgs += "-K " # ADDING -K argument if wait() is forced
         tmpfile = self.workDir+'/cmd.csh'
         with open(tmpfile, 'w') as f:
            f.write("#!/usr/bin/csh\ncd %s\ntouch %s\n%s\n\n"%(self.workDir,
                                                               self.stderr,
                                                               self.GetCMD()))
         os.chmod(tmpfile, 0744)
         # QSUB INFO :: run_time_limit(walltime, dd:hh:mm:ss), memory(mem, up to 100GB *gigabyte), processors(ppn, up to 16) # USE AS LITTLE AS NEEDED!!!
         cmd = ("ssh %s /usr/bin/qsub -l nodes=%s:ppn=%d,walltime=%d:00:00,mem=%dg -r y -d  %s -e %s -o %s %s %s")%(self.server, self.nodes, self.procs, self.wt, self.mem, self.workDir, self.stderr, self.stdout, otherArgs, tmpfile ) # '-q %s' self.queue
         printDebug("\n\nExecute %s...\n%s\n%s\n" % (self.name, cmd, self.GetCMD()))
      else:
         cmd = "cd %s;%s > %s >& %s"%(self.workDir, self.GetCMD(), self.stdout,
                                      self.stderr)
         if self.ssh: cmd = "ssh %s '%s'"%(self.server, cmd)
         printDebug("\n\nExecute %s...\n%s\n" % (self.name, cmd)) #DEBUG
      self.status = 'Executing'
      # EXECUTING PROGRAM
      self.UpdateTimer(-time()) # TIME START
      if self.wait:
         self.p = subprocess.Popen(cmd, shell=True, executable="/bin/bash").wait()
         self.status = 'Done'
      else: # WaitOn should be called to determine if the program has ended
         self.p = subprocess.Popen(cmd, shell=True, executable="/bin/bash")
      self.UpdateTimer(time()) # TIME END
      printDebug("timed: %s" % (self.GetTime()))


################################################################################
##########                         FUNCTIONS                         ###########
################################################################################

def createScript(cmd, out, waitfor=None, options=None, wait_file=None):
# Description:
# Creates a script from a command line to be used with qbot
#
# Arguments (type: hash):
# cmd       Command line to be executed
# out       Write qbot script to this file
# waitfor   Name of scripts to depend on, seperated by whitespaces (only
#       file name, NOT path)
# options   msub options to be passed to the script
# wait_file Creates a loop in the script that will wait for a specific file
#       to be created with non-zero size. This is to be used with
#       scripts that creates jobs in the queue and then exits, like the
#       "mlst_wrapper" application.
#
# Usage:
# createScript( cmd="script args", out="file", waitfor="script1 script2", options="-l walltime=86000,procs=1,mem=2gb", wait_file="path/to/file" )
  shebang = "#!/usr/bin/sh";

  with open(out, 'w') as f:
    f.write('%s\n\n'%(shebang))

    if options != None:
      f.write('###OPTIONS %s\n\n'%(options))
    if waitfor != None:
      f.write('###WAITFOR %s\n'%(waitfor))

    f.write("%s\n\n"%(cmd))

    if wait_file != None:
      f.write('while [ ! -s %s ]\n'%(wait_file))
      f.write('do\n')
      f.write('\tsleep 120\n')
      f.write('done\n\n')

    f.write('echo \"qbot: job completed\"\n\n')

    if waitfor != None or options != None:
      f.write('\n')

  return_val = subprocess.call(["chmod", "oga+x", out])

  return return_val


################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
setDebug(False)
service, version = "CSIPhylogeny", "1.1"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
#args = getArguments('''

#text           text         -x  VALUE  -x  ''
#file           file         -f  VALUE  -f  ''
#textarea       textarea     -a  VALUE  -a  ''
#checkbox       checkbox     -c  VALUE  -c  ''
#radio          radio        -r  VALUE  -r  ''
#selectionbox   select       -s  VALUE  -s  ''
#mselectionbox  multiselect  -m  VALUE  -m  ''
#''')
args = getArguments('''
selectionbox  minDepth      --depth       VALUE
selectionbox  minRelDepth   --rdepth      VALUE
selectionbox  prune         --prune       VALUE
selectionbox  snpQ          --snpq        VALUE
selectionbox  mapQ          --mapq        VALUE
selectionbox  zscore        --zscore      VALUE
file          refGenomeFile --ref         VALUE
checkbox      include_ref   --includeref  VALUE
''')


#----DELETE THIS----#
# VALIDATE REQUIRED ARGUMENTS
#if args.technology == None: GraceFullExit("Error: No technology platform was chosen!\n")
#-------------------#

# SET RUN DIRECTORY (No need to change this part)
########################### runID = time.strftime('%w_%d_%m_%Y_%H%M%S_')+str(random.randint(100000,999999))
runID = strftime('%w_%d_%m_%Y_%H%M%S_')+str(random.randint(100000,999999))
paths.serviceRoot = '{programRoot}IO/%s/'%(runID)
paths.isolateRoot = '{programRoot}'
paths.add_path('uploads', '{serviceRoot}Uploads/')

# SET AND UPDATE PATHS (No need to change this part)
UpdatePaths(service, version, '', '', args.webmode)

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()
stat = paths.Create('uploads')

# LOG SERVICE SUBMISSION IN SQL (No need to change this part)
add2DatabaseLog(service+'-'+version, runID, args.usr, args.ip, "mixed")

# MOVE UPLOADS FROM APP- TO ISOLATE UPLOAD DIRECTORY (No need to change this part)
if stat:
   # Move the uploaded files to the upload directory
   moveFile(args.uploadPath+'/*', paths['uploads'])
   # Unzipping uploaded files if zipped
   fileUnzipper(paths['uploads'])
   # GET INPUT FILES from input path
   inputFiles = makeFileList(paths['uploads'])
else:
   GraceFullExit("Error: Could not create upload directory!\n")

#
#----CHANGE THIS----#
#
# GET CONTIGS (No need to change this part UNLESS you dont need assembly)
#if args.technology != 'Assembled_Genome':
#   # ADD THE ASSEMBLER to the program list
#   AddAssemblyToProgList()
   # Prepare the Assembly program for execution
#   PrepareAssembly(args.technology, inputFiles)
   # Assemble the reads into contigs
#   n50 = MakeAssembly(args.technology)
   # The success of the assembler is validated
#   status = progs['Assembler'].GetStatus()
#   if status != 'Done' or not isinstance(n50, int):
#      GraceFullExit("Error: Assembly of the inputfile(s) failed!\n"%(len(inputFiles)))
#else:
   # Validate that only 1 file was submitted
#   if len(inputFiles) != 1:
#      GraceFullExit("Error: Invalid number of contig files (%s)\n"%(len(inputFiles)))
   # Validate that the uploaded file is fasta
#   if CheckFileType(inputFiles[0]) != 'fasta':
#      GraceFullExit("Error: Invalid contigs format (%s)!\nOnly the fasta format is recognised as a proper contig format.\n"%(CheckFileType(inputFiles[0])))
   # Add contigsPath
#   paths.add_path('contigs', paths['inputs']+'contigs.fsa')
   # Copy file to Input directory
#   copyFile(inputFiles[0], paths['contigs'])
#
#-------------------#
#

#
# Preparing execution of main script
#

# Preparing reference
reference_init_path = paths.serviceRoot + "init_reference.fa"
reference_path = paths.serviceRoot + "reference.fa"
copyFile(args.refGenomeFile, reference_init_path)
subprocess.call([FastaToolPath + " -f 80 < "+reference_init_path+" > "+reference_path], shell=True)
subprocess.call(["rm "+reference_init_path], shell=True)

# Preparing input files
#for input_file in inputFiles:
#    subprocess.call(["dos2unix "+input_file], shell=True)

inputFiles_str = "";
for input_file in inputFiles:
  inputFiles_str = inputFiles_str + " " + input_file

if(args.include_ref):
  inputFiles_str = inputFiles_str + " " + reference_path

# ADDING PROGRAMS
snp_tree_pipeline = progs.AddProgram(program_extended(
   name='CSIPhylogeny', path=paths['scripts']+'Ks_SNP_tree_pipeline.pl', timer=0,
   ptype='perl', toQueue=True, wait=False, workDir=paths.serviceRoot, walltime=36,
   nodes='cgebase',
   args=['-w', runID,
         '-r', reference_path,
         '-qb', 'qbot_dir',
         '-o', 'downloads',
         '-ap', args.prune,
         '-fmd', args.minDepth,
         '-frd', args.minRelDepth,
         '-fp', args.prune,
         '-fmq', args.mapQ,
         '-fsq', args.snpQ,
         '-fz', args.zscore]))
# Appending input files
for input_file in inputFiles:
  snp_tree_pipeline.AppendArgs([input_file])


matrixScript = progs.AddProgram(program_extended(
   name='matrixScript', path=paths['scripts']+'Ks_snp_matrix.pl', timer=0,
   ptype='perl', toQueue=True, wait=False, workDir=paths.serviceRoot,
   nodes='cgebase',
   args=['-o', paths.serviceRoot+'/downloads/main',
         '-i', paths.serviceRoot+'/downloads/snp_tree.aln.fasta']))



# EXECUTION OF THE PROGRAMS
snp_tree_pipeline.Execute()
# total wait time is 600s (10 min) x 216 = 36 hours
snp_tree_pipeline.WaitOn(pattern='DONE!', interval=600, maxloopsleft=216)

matrixScript.Execute()
matrixScript.WaitOn(pattern='DONE!', interval=10, maxloopsleft=300)


#
# Create links for output page
#
subprocess.call(["ln", "-s", paths.serviceRoot+"/downloads/snp_tree.main_tree.png", "/srv/www/htdocs/services/"+service+"/tmp/snp_tree."+runID+".png"])
subprocess.call(["ln", "-s", paths.serviceRoot+"/downloads/ignored_plot.png", "/srv/www/htdocs/services/"+service+"/tmp/ignored_plot."+runID+".png"])
subprocess.call(["ln", "-s", paths.serviceRoot+"/downloads/snp_tree.main_tree.newick", "/srv/www/htdocs/services/CSIPhylogeny/tmp/snp_tree."+runID+".newick"])
subprocess.call(["ln", "-s", paths.serviceRoot+"/downloads/genome_cov_hist.png", "/srv/www/htdocs/services/CSIPhylogeny/tmp/genome_cov_hist."+runID+".png"])


# THE SUCCESS OF THE SERVICE IS VALIDATED
status = snp_tree_pipeline.GetStatus()
if status != 'Done': GraceFullExit("Error: Execution of the pipeline failed!\n")

status = matrixScript.GetStatus()
if status != 'Done': GraceFullExit("Error: Execution of the matrix script failed!\n")


# CREATE THE HTML OUTPUT FILE (No need to change this part)
if args.webmode:
   if os.path.exists(paths['outputs']) and not os.path.exists(paths['outputs']+service+".out"):
      with open(paths['outputs']+service+".out", 'w') as f:
         f.write('<h1>%s-%s Server - Results</h1>'%(service, version))

# PRINT THE STANDARD OUTPUT OF THE PROGRAM TO THE HTML OUTPUT FILE
snp_tree_pipeline.Printstdout()

# PREPARE THE DOWNLOADABLE FILE(S)
#_ = PrepareDownload(paths['serviceRoot']+'dummy_dir/dummy_txt_file.txt')
#_ = PrepareDownload(paths['serviceRoot']+'dummy_dir/dummy_tab_file.tsv')

# PRINT TAB FILE(S) AS RESULT TABLES TO THE HTML OUTPUT FILE
#tsv=paths['downloads']+'dummy_tab_file.tsv'
#if os.path.exists(tsv):
#   printOut('<h2>Dummy Tab-File:</h2>\n')
#   tsv2html(tsv, full_dl=tsv, css_classes='redhead')
#   printOut('<br>\n')

# PRINT THE PROGRAM PARAMETERS TO THE HTML OUTPUT FILE
#if len(inputFiles) > 0: printInputFilesHtml(inputFiles)

# PRINT FILE DOWNLOAD-BUTTON(S) TO THE HTML OUTPUT FILE
#dlButton('RESULTS', 'dummy_txt_file.txt')

################################################################################
# LOG THE TIMERS (No need to change this part)
progs.PrintTimers()

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# INDICATE THAT THE WRAPPER HAS FINISHED (No need to change this part)
printDebug("Done")

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE (No need to change this part)
# fileZipper(paths['serviceRoot'])
