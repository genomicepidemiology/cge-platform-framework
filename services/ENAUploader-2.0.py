#!/usr/bin/env python
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Service structure
#--> The input/arguments are validated
#--> Create service folder and subfolders
#--> The uploaded files are copied to the 'Upload' directory
#--> Log service submission in SQL
#--> Setup and execution of service specific programs
#--> The success of the service is validated
#--> A HTML output page is created if in webmode
#--> Downloadable files are copied/moved to the 'Download' Directory
#--> The SQL service entry is updated
#--> The files are zipped for long-term storage
import sys, os, random, subprocess, re

from time import time, localtime, strftime, sleep
from pipes import quote

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from assembly_module   import (PrepareAssembly, MakeAssembly,
                               printContigsInfo, AddAssemblyToProgList)
from functions_module  import (printDebug, copyFile, program, createServiceDirs,
                               getArguments, paths, makeFileList, fileUnzipper,
                               printInputFilesHtml, fileZipper, PrepareDownload,
                               UpdatePaths, progs, CheckFileType, printOut,
                               moveFile, setDebug, add2DatabaseLog, dlButton,
                               GraceFullExit, FixPermissions, tsv2html)

#
# Hard-coded paths
#
EnaSubScriptPath = "scripts/ENA_submission.pl" # internal path in the storage root.
GoogleScriptPath = "scripts/google_sheet_parser.pl" # internal path in the storage root.
DataScriptPath = "scripts/data_finder.pl" # internal path in the storage root.

################################################################################
########                         ALT PRG CLASS                         #########
################################################################################

class program_extended(program):
   ''' This Class defines a program structure
      USAGE:
         assembler = proglist.add_program(
            program(name='Assembler',
               path='/panfs1/cge-servers/Assembler/Assembler-1.0'+
                    '/scripts/mlst_wrapper_denovo_2.0.py',
               timer=0,
               ptype='python',
               workdir='',
               ToQueue=False,
               Wait=True,
               args='')
            )
         assembler.AppendArgs(['velvet'])
         assembler.Execute()
   '''
   def __init__(self, name, path=None, timer=0, ptype=None, workDir=None, ssh=True, toQueue=True, wait=False, args=None, walltime=2, mem=4, procs=1, nodes="1"):
      # INIT VALUES
      self.path = path
      self.name = name
      self.timer = timer
      self.ptype = ptype
      self.workDir = workDir
      self.toQueue = toQueue
      self.wait = wait
      self.args = []
      self.stderr = "%s/%s.err"%(paths['logs'], name)
      self.stdout = "%s/%s.out"%(paths['outputs'], name) if os.path.exists(paths['outputs']) else "%s/%s.out"%(paths['logs'], name)
      self.queue = 'cge'
      self.wt = walltime # Maximum amount of hours required to complete program
      self.mem = mem     # GB RAM allocation requirement
      self.procs = procs # Number of processors required for the job
      self.nodes = nodes # Number of nodes, primarily used to control where the job is executed ex. nodes=cgebase
      self.p = None      # The Subprocess Object
      self.ssh = ssh
      self.server = 'cgebase'
      self.status = 'Initialised'
      if not workDir: self.workDir = paths['outputs']
      self.queue = 'www'
      if args: self.AppendArgs(args)
   def GetCMD(self):
      ''' This function combines and return the commandline call of the program. '''
      printDebug("Get cmd.-.-.")
      cmd = []
      if self.ptype is not None:
         cmd.append(self.ptype)
         printDebug("ptype: "+self.ptype)
         #if paths[self.ptype] != '': cmd.append(paths[self.ptype])
         #elif os.path.exists(self.ptype): cmd.append(self.ptype)
         if os.path.exists(self.ptype): printDebug("Found "+self.ptype)
      if self.path is not None and os.path.exists(self.path):
         cmd.append(self.path)
         cmd.extend([str(x) if not isinstance(x, (unicode)) else x.encode('utf-8') for x in [] + [quote(x) for x in self.args]])
      printDebug("RETURNING:")
      printDebug(' '.join(cmd))
      return ' '.join(cmd)
   def Execute(self):
      ''' This function Executes the program with set arguments. '''
      # SETTING WORKDIR if not set
      if self.toQueue:
         otherArgs = ''
         if self.wait: otherArgs += "-K " # ADDING -K argument if wait() is forced
         tmpfile = self.workDir+'/cmd.csh'
         with open(tmpfile, 'w') as f:
            f.write("#!/usr/bin/csh\ncd %s\ntouch %s\n%s\n\n"%(self.workDir,
                                                               self.stderr,
                                                               self.GetCMD()))
         os.chmod(tmpfile, 0744)
         # QSUB INFO :: run_time_limit(walltime, dd:hh:mm:ss), memory(mem, up to 100GB *gigabyte), processors(ppn, up to 16) # USE AS LITTLE AS NEEDED!!!
         cmd = ("ssh %s /usr/bin/qsub -l nodes=%s:ppn=%d,walltime=%d:00:00,mem=%dg -r y -d  %s -e %s -o %s %s %s")%(self.server, self.nodes, self.procs, self.wt, self.mem, self.workDir, self.stderr, self.stdout, otherArgs, tmpfile ) # '-q %s' self.queue
         printDebug("\n\nExecute %s...\n%s\n%s\n" % (self.name, cmd, self.GetCMD()))
      else:
         cmd = "cd %s;%s > %s >& %s"%(self.workDir, self.GetCMD(), self.stdout,
                                      self.stderr)
         if self.ssh: cmd = "ssh %s '%s'"%(self.server, cmd)
         printDebug("\n\nExecute %s...\n%s\n" % (self.name, cmd)) #DEBUG
      self.status = 'Executing'
      # EXECUTING PROGRAM
      self.UpdateTimer(-time()) # TIME START
      if self.wait:
         self.p = subprocess.Popen(cmd, shell=True, executable="/bin/bash").wait()
         self.status = 'Done'
      else: # WaitOn should be called to determine if the program has ended
         self.p = subprocess.Popen(cmd, shell=True, executable="/bin/bash")
      self.UpdateTimer(time()) # TIME END
      printDebug("timed: %s" % (self.GetTime()))

################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
setDebug(True)
service, version = "ENAUploader", "2.0"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
#args = getArguments('''

#text           text         -x  VALUE  -x  ''
#file           file         -f  VALUE  -f  ''
#textarea       textarea     -a  VALUE  -a  ''
#checkbox       checkbox     -c  VALUE  -c  ''
#radio          radio        -r  VALUE  -r  ''
#selectionbox   select       -s  VALUE  -s  ''
#mselectionbox  multiselect  -m  VALUE  -m  ''
#''')
args = getArguments('''
text			prjno			--project	VALUE
text			title			--title		VALUE
textarea		abstract		--abstract	VALUE
checkbox		istest			--test		VALUE
file            seq_list        --seq_list  VALUE
checkbox		uploaded		--uploaded	VALUE
file    		md5			    --md5		VALUE
''')


#----DELETE THIS----#
# VALIDATE REQUIRED ARGUMENTS
#if args.technology == None: GraceFullExit("Error: No technology platform was chosen!\n")
#-------------------#

# SET RUN DIRECTORY (No need to change this part)
runID = strftime('%w_%d_%m_%Y_%H%M%S_')+str(random.randint(100000,999999))
paths.serviceRoot = '{programRoot}IO/%s/'%(runID)
paths.isolateRoot = '{programRoot}'
paths.add_path('uploads', '{serviceRoot}Uploads/')

# SET AND UPDATE PATHS (No need to change this part)
UpdatePaths(service, version, '', '', args.webmode)

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()
stat = paths.Create('uploads')

# LOG SERVICE SUBMISSION IN SQL (No need to change this part)
add2DatabaseLog(service+'-'+version, runID, args.usr, args.ip, "mixed")

# MOVE UPLOADS FROM APP- TO ISOLATE UPLOAD DIRECTORY (No need to change this part)
if stat:
   # Move the uploaded files to the upload directory
   moveFile(args.uploadPath+'/*', paths['uploads'])
   # Unzipping uploaded files if zipped
   fileUnzipper(paths['uploads'])
   # GET INPUT FILES from input path
   inputFiles = makeFileList(paths['uploads'])
else:
   GraceFullExit("Error: Could not create upload directory!\n")

#
#----CHANGE THIS----#
#
# GET CONTIGS (No need to change this part UNLESS you dont need assembly)
#if args.technology != 'Assembled_Genome':
#   # ADD THE ASSEMBLER to the program list
#   AddAssemblyToProgList()
   # Prepare the Assembly program for execution
#   PrepareAssembly(args.technology, inputFiles)
   # Assemble the reads into contigs
#   n50 = MakeAssembly(args.technology)
   # The success of the assembler is validated
#   status = progs['Assembler'].GetStatus()
#   if status != 'Done' or not isinstance(n50, int):
#      GraceFullExit("Error: Assembly of the inputfile(s) failed!\n"%(len(inputFiles)))
#else:
   # Validate that only 1 file was submitted
#   if len(inputFiles) != 1:
#      GraceFullExit("Error: Invalid number of contig files (%s)\n"%(len(inputFiles)))
   # Validate that the uploaded file is fasta
#   if CheckFileType(inputFiles[0]) != 'fasta':
#      GraceFullExit("Error: Invalid contigs format (%s)!\nOnly the fasta format is recognised as a proper contig format.\n"%(CheckFileType(inputFiles[0])))
   # Add contigsPath
#   paths.add_path('contigs', paths['inputs']+'contigs.fsa')
   # Copy file to Input directory
#   copyFile(inputFiles[0], paths['contigs'])
#
#-------------------#
#

#
# Preparing execution of main script
#

# Getting google sheet
#subprocess.call(["wget", "-O", "seq_sheet.xlsx", "http://spreadsheets.google.com/feeds/download/spreadsheets/Export?key=18PHpHjs_Ij3UeLoTbQhZWoVBvtp77OWGfIQQaGEAU9U&exportFormat=xlsx"])
#subprocess.call(["wget", "-O", "seq_sheet2.xlsx", "\'http://spreadsheets.google.com/feeds/download/spreadsheets/Export?key=18PHpHjs_Ij3UeLoTbQhZWoVBvtp77OWGfIQQaGEAU9U&exportFormat=xlsx\'"], shell=True)
# wget -O seq_sheet.xlsx "http://spreadsheets.google.com/feeds/download/spreadsheets/Export?key=18PHpHjs_Ij3UeLoTbQhZWoVBvtp77OWGfIQQaGEAU9U&exportFormat=xlsx"

# Copying files
seq_sheet_path = paths.serviceRoot + "seq_sheet.xlsx"
if(args.seq_list):
    copyFile(args.seq_list, seq_sheet_path)
else:
    subprocess.call(["wget", "-O", "seq_sheet.xlsx", "http://spreadsheets.google.com/feeds/download/spreadsheets/Export?key=18PHpHjs_Ij3UeLoTbQhZWoVBvtp77OWGfIQQaGEAU9U&exportFormat=xlsx"])

abstract_path = paths.serviceRoot + "abstract.txt"
copyFile(args.abstract, abstract_path)

if(args.uploaded):
    md5_path = paths.serviceRoot + "md5.txt"
    copyFile(args.md5, md5_path)

# ADDING PROGRAMS
if(not args.seq_list):
    google_sheet_parser = progs.AddProgram(program_extended(
        name='google_sheet_parser', path=paths['scripts']+'google_sheet_parser.pl',
        timer=0, ptype='perl', toQueue=True, wait=False, workDir=paths.serviceRoot,
        walltime=36, nodes='cgebase',
        args=['--project', args.prjno,
              '-s', seq_sheet_path,
              '-o', paths['downloads']+'google_sheet_data',
              '--web', runID]))

    data_finder = progs.AddProgram(program_extended(
        name='data_finder', path=paths['scripts']+'data_finder.pl', timer=0,
        ptype='perl', toQueue=False, ssh=False, wait=False,
        workDir=paths.serviceRoot,
        args=['--host', 'cgebase2',
              '--pipeline', paths['downloads']+'google_sheet_data.txt',
              '-o', paths['downloads']+'read_file_paths.txt',
              '--web', runID]))
    if(args.uploaded):
        data_finder.AppendArgs('--uploaded')

    ena_submission = progs.AddProgram(program_extended(
        name='ENA_submission', path=paths['scripts']+'ENA_submission.pl', timer=0,
        ptype='perl', toQueue=True, wait=False, workDir=paths.serviceRoot,
        walltime=48, nodes='cgebase',
        args=['--input', paths['downloads']+'google_sheet_data.xlsx',
              '-d', paths['uploads'],
              '--web', runID,
              '--excel',
              '--title', args.title,
              '--abstract', abstract_path]))
    if(args.istest):
        ena_submission.AppendArgs('--validate')
    if(args.uploaded):
        ena_submission.AppendArgs('--uploaded')
else:
    ena_submission = progs.AddProgram(program_extended(
        name='ENA_submission', path=paths['scripts']+'ENA_submission.pl', timer=0,
        ptype='perl', toQueue=True, wait=False, workDir=paths.serviceRoot,
        walltime=48, nodes='cgebase',
        args=['--input', paths['downloads']+'seq_sheet.xlsx',
              '-d', paths['uploads'],
              '--web', runID,
              '--excel',
              '--title', args.title,
              '--abstract', abstract_path]))
    if(args.istest):
        ena_submission.AppendArgs('--validate')
    if(args.uploaded):
        ena_submission.AppendArgs('--uploaded')



# EXECUTION OF THE PROGRAMS

if(not args.seq_list):
    google_sheet_parser.Execute()
    # total wait time is 600s (10 min) x 144 = 24 hours
    google_sheet_parser.WaitOn(pattern='DONE!', interval=120, maxloopsleft=144)

    data_finder.Execute()
    data_finder.WaitOn(pattern='DONE!', interval=10, maxloopsleft=300)
    #if(args.uploaded):
    #    subprocess.call(["perl", "--host", "cgebase2", "--pipeline",  paths['downloads']+'google_sheet_data.txt', "-o", paths['downloads']+'read_file_paths.txt', "--web", runID, "--uploaded"])
    #else:
    #    subprocess.call(["perl", "--host", "cgebase2", "--pipeline",  paths['downloads']+'google_sheet_data.txt', "-o", paths['downloads']+'read_file_paths.txt', "--web", runID])

    # Copy files to cgebase
    if(args.uploaded):
        with open(paths['downloads']+'read_file_paths.txt', "r") as fh:
            for line in fh:
                line = line.rstrip()
                entry = line.split()
                split_pat = re.compile("__split__")
                #  split_pat = re.search(r"^__split__", entry[0])
                printDebug("ENTRY1: "+entry[0])
                printDebug("ENTRY2: "+entry[1])
                if(split_pat.search(entry[0])):
                    printDebug("IN IF")
                    copyFile(entry[0], paths['uploads'])
    else:
        with open(paths['downloads']+'read_file_paths.txt', "r") as fh:
            for line in fh:
                line = line.rstrip()
                entry = line.split()
                for file in entry:
                    copyFile(file, paths['uploads'])

#DEBUG
#printDebug("Done - debug premature end.")
#sys.exit()

ena_submission.Execute()
if(args.uploaded):
    ena_submission.WaitOn(pattern='DONE!', interval=60, maxloopsleft=15)
else:
    # total wait time is 600s (10 min) x 288 = 48 hours
    ena_submission.WaitOn(pattern='DONE!', interval=600, maxloopsleft=288)

# THE SUCCESS OF THE SERVICE IS VALIDATED
status = google_sheet_parser.GetStatus()
if status != 'Done': GraceFullExit("Error: Execution of the google script failed!\n")

status = data_finder.GetStatus()
if status != 'Done': GraceFullExit("Error: Execution of the data finder script failed!\n")

status = ena_submission.GetStatus()
if status != 'Done': GraceFullExit("Error: Execution of the ena submission script failed!\n")


# CREATE THE HTML OUTPUT FILE (No need to change this part)
if args.webmode:
   if os.path.exists(paths['outputs']) and not os.path.exists(paths['outputs']+service+".out"):
      with open(paths['outputs']+service+".out", 'w') as f:
         f.write('<h1>%s-%s Server - Results</h1>'%(service, version))

# PRINT THE STANDARD OUTPUT OF THE PROGRAM TO THE HTML OUTPUT FILE
ena_submission.Printstdout()

# PREPARE THE DOWNLOADABLE FILE(S)
#_ = PrepareDownload(paths['serviceRoot']+'dummy_dir/dummy_txt_file.txt')
#_ = PrepareDownload(paths['serviceRoot']+'dummy_dir/dummy_tab_file.tsv')

# PRINT TAB FILE(S) AS RESULT TABLES TO THE HTML OUTPUT FILE
#tsv=paths['downloads']+'dummy_tab_file.tsv'
#if os.path.exists(tsv):
#   printOut('<h2>Dummy Tab-File:</h2>\n')
#   tsv2html(tsv, full_dl=tsv, css_classes='redhead')
#   printOut('<br>\n')

# PRINT THE PROGRAM PARAMETERS TO THE HTML OUTPUT FILE
#if len(inputFiles) > 0: printInputFilesHtml(inputFiles)

# PRINT FILE DOWNLOAD-BUTTON(S) TO THE HTML OUTPUT FILE
#dlButton('RESULTS', 'dummy_txt_file.txt')

################################################################################
# LOG THE TIMERS (No need to change this part)
progs.PrintTimers()

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# INDICATE THAT THE WRAPPER HAS FINISHED (No need to change this part)
printDebug("Done")

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE (No need to change this part)
# fileZipper(paths['serviceRoot'])
