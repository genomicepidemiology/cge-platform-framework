#!/usr/bin/env python
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Service structure
#--> The input/arguments are validated
#--> Create service folder and subfolders
#--> The uploaded files are copied to the 'Upload' directory
#--> Log service submission in SQL
#--> Setup and execution of service specific programs
#--> The success of the service is validated
#--> A HTML output page is created if in webmode
#--> Downloadable files are copied/moved to the 'Download' Directory
#--> The SQL service entry is updated
#--> The files are zipped for long-term storage
import sys, os, time, random

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
#from assembly_module   import (PrepareAssembly, MakeAssembly,
#                               printContigsInfo, AddAssemblyToProgList)
from functions_module  import (printDebug, copyFile, program, createServiceDirs,
                               getArguments, paths, makeFileList, fileUnzipper,
                               printInputFilesHtml, fileZipper, PrepareDownload,
                               UpdatePaths, progs, CheckFileType, printOut,
                               moveFile, setDebug, add2DatabaseLog, dlButton,
                               GraceFullExit, FixPermissions, tsv2html,
                               extendedOutButton)

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################

################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
setDebug(False)
service, version = "KmerFinder", "2.2"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
args = getArguments('''
file           file         -f  VALUE  -f  ''
mselectionbox  scoring  -s  VALUE  -s  ''
mselectionbox  database  -d  VALUE  -d  ''
''', allowcmd=True)

prefix=False

if args.database == "bacteria_c":
   dbPath= "/home/data2/databases/homology/current/bacteria/bacteria.ATGAC.hr"
   taxPath= "/home/data2/databases/taxonomy/current/bacteria_ncbi_tax"
   prefix= "ATGAC"

elif args.database == "bacteria_o":
   dbPath= "/home/data2/databases/homology/current/bacteria/bacteria.ATGAC.hr"
   taxPath= "/home/data2/databases/taxonomy/current/bacteria_ncbi_tax"
   prefix= "ATGAC"

elif args.database == "plasmids":
   dbPath= "/home/data2/databases/homology/current/plasmids/plasmids.hr"
   taxPath= "/home/data2/databases/taxonomy/current/plasmids_ncbi_tax"
   prefix= "AT"

elif args.database == "type_strains":
   dbPath= "/home/data2/databases/homology/current/type_strains/type_strains.ATGAC.hr"
   taxPath= "/home/data2/databases/taxonomy/current/bacteria_ncbi_tax"
   prefix= "ATGAC"

elif args.database == "virus_a":
   dbPath= "/home/data2/databases/homology/current/viruses/viruses_all.hr"
   taxPath= "/home/data2/databases/taxonomy/current/viruses_ncbi_tax"

elif args.database == "virus_h":
   dbPath= "/home/data2/databases/homology/current/viruses/viruses_human.hr"
   taxPath= "/home/data2/databases/taxonomy/current/viruses_ncbi_tax"

elif args.database == "virus_v":
   dbPath= "/home/data2/databases/homology/current/viruses/virus_vertebrates.hr"
   taxPath= "/home/data2/databases/taxonomy/current/viruses_ncbi_tax"

elif args.database == "fungi":
   dbPath= "/home/data2/databases/homology/current/fungi/fungi.hr"
   taxPath= "/home/data2/databases/taxonomy/current/fungi_tax"
   prefix= "ATGAC"

elif args.database == "protists":
   dbPath= "/home/data2/databases/homology/current/protist/protists.hr"
   taxPath= "/home/data2/databases/taxonomy/current/protists_tax"
   prefix= "ATGAC"

elif args.database == "resfinder":
   dbPath= "/home/data1/services/KmerFinder/database/resfinder.NO_P"

# VALIDATE REQUIRED ARGUMENTS
#if args.technology == None: GraceFullExit("Error: No technology platform was chosen!\n")

# SET RUN DIRECTORY (No need to change this part)
runID = time.strftime('%w_%d_%m_%Y_%H%M%S_')+str(random.randint(100000,999999))
paths.serviceRoot = '{programRoot}IO/%s/'%(runID)
paths.isolateRoot = '{programRoot}'
paths.add_path('uploads', '{serviceRoot}Uploads/')

# SET AND UPDATE PATHS (No need to change this part)
UpdatePaths(service, version, '', '', args.webmode)

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()
stat = paths.Create('uploads')

# LOG SERVICE SUBMISSION IN SQL (No need to change this part)
#add2DatabaseLog(service+'-'+version, runID, args.usr, args.ip, args.technology)
add2DatabaseLog(service+'-'+version, runID, args.usr, args.ip, "not available")

# MOVE UPLOADS FROM APP- TO ISOLATE UPLOAD DIRECTORY (No need to change this part)
if stat:
   # Move the uploaded files to the upload directory
   moveFile(args.uploadPath+'/*', paths['uploads'])
   # Unzipping uploaded files if zipped
   fileUnzipper(paths['uploads'])
   # GET INPUT FILES from input path
   inputFiles = makeFileList(paths['uploads'])
else:
   GraceFullExit("Error: Could not create upload directory!\n")

# GET CONTIGS (No need to change this part UNLESS you dont need assembly)
#if args.technology != 'Assembled_Genome':
#   # ADD THE ASSEMBLER to the program list
#   AddAssemblyToProgList()
#   # Prepare the Assembly program for execution
#   PrepareAssembly(args.technology, inputFiles)
#   # Assemble the reads into contigs
#   n50 = MakeAssembly(args.technology)
#   # The success of the assembler is validated
#   status = progs['Assembler'].GetStatus()
#   if status != 'Done' or not isinstance(n50, int):
#      GraceFullExit("Error: Assembly of the inputfile(s) failed!\n"%(len(inputFiles)))
#else:
#   # Validate that only 1 file was submitted
#   if len(inputFiles) != 1:
#      GraceFullExit("Error: Invalid number of contig files (%s)\n"%(len(inputFiles)))
#   # Validate that the uploaded file is fasta
#   if CheckFileType(inputFiles[0]) != 'fasta':
#      GraceFullExit("Error: Invalid contigs format (%s)!\nOnly the fasta format is recognised as a proper contig format.\n"%(CheckFileType(inputFiles[0])))
#   # Add contigsPath
#   paths.add_path('contigs', paths['inputs']+'contigs.fsa')
#   # Copy file to Input directory
#   copyFile(inputFiles[0], paths['contigs'])

# ADDING PROGRAMS

# run KmerFinder:
kmerfinder = progs.AddProgram(program(
   name='kmerfinder', path=paths['scripts']+'findtemplate.py', timer=0,
   ptype='python', toQueue=True, wait=False, workDir='',
   server='cgebase' if args.webmode else 'cgebase2',
   args=['-i', inputFiles[0],
         '-t',dbPath,
         '-o', paths['outputs'] + 'results.txt']))

if prefix!=False:
   kmerfinder.AppendArgs(['-x',prefix])

if args.scoring == "winner":
   kmerfinder.AppendArgs(['-w'])

if args.database != "resfinder":
   # add taxonomy:

   addTaxonomy = progs.AddProgram(program(
      name='addTaxonomy', path=paths['scripts']+'get_tax.py', timer=0,
      ptype='python', toQueue=True, wait=False, workDir='',
      args=['-i', paths['outputs'] + 'results.txt',
            '-t',taxPath,
            '-o', paths['outputs'] + 'results_tax.txt']))

   if args.database == "bacteria_c" or args.database == "plasmids" or args.database == "type_strains":
      addTaxonomy.AppendArgs(['-b'])
   elif args.database == "fungi" or args.database == "protists" or args.database == "virus_a" or args.database == "virus_h" or args.database == "virus_v":
      addTaxonomy.AppendArgs(['-c'])
   elif args.database == "bacteria_o":
      addTaxonomy.AppendArgs(['-c','-b'])

   # get final table:

   finalOut = progs.AddProgram(program(
      name='finalOut', path=paths['scripts']+'create_final_table.py', timer=0,
      ptype='python', toQueue=True, wait=False, workDir='',
      args=['-i', paths['outputs'] + 'results_tax.txt',
            '-d',args.database,
            '-o', paths['outputs'] + 'results_final.txt']))


# ADDING MORE ARGUMENTS
#dummy.AppendArgs(['-option', 'content'])

# EXECUTION OF THE PROGRAMS
kmerfinder.Execute()
kmerfinder.WaitOn(pattern='DONE!', interval=2, maxloopsleft=2000000)

if args.database != "resfinder":
   addTaxonomy.Execute()
   addTaxonomy.WaitOn(pattern='DONE!', interval=2, maxloopsleft=2000000)

   finalOut.Execute()
   finalOut.WaitOn(pattern='DONE!', interval=2, maxloopsleft=2000000)
else:
   os.system("cp " + paths['outputs']+'results.txt' + " " + paths['outputs'] + 'results_tax.txt')
   os.system("cp " + paths['outputs']+'results.txt' + " " + paths['outputs'] + 'results_final.txt')


# THE SUCCESS OF THE SERVICE IS VALIDATED
status = kmerfinder.GetStatus()
if status != 'Done': GraceFullExit("Error: Execution of the KmerFinder program failed!\n")

if args.database != "resfinder":
   status = addTaxonomy.GetStatus()
   if status != 'Done': GraceFullExit("Error: Execution of the Taxonomy program failed!\n")

   status = finalOut.GetStatus()
   if status != 'Done': GraceFullExit("Error: Execution of the table parsing program failed!\n")

# CREATE THE HTML OUTPUT FILE (No need to change this part)
if args.webmode:
   if os.path.exists(paths['outputs']) and not os.path.exists(paths['outputs']+service+".out"):
      with open(paths['outputs']+service+".out", 'w') as f:
         f.write('<h1>%s-%s Server - Results</h1>'%(service, version))

# PRINT THE STANDARD OUTPUT OF THE PROGRAM TO THE HTML OUTPUT FILE
#searchdb.Printstdout()

# PREPARE THE DOWNLOADABLE FILE(S)
#_ = PrepareDownload(paths['serviceRoot']+'dummy_dir/dummy_txt_file.txt')
_ = PrepareDownload(paths['serviceRoot']+'outputs/results_final.txt')
_ = PrepareDownload(paths['serviceRoot']+'outputs/results_tax.txt')

# PRINT TAB FILE(S) AS RESULT TABLES TO THE HTML OUTPUT FILE
#tsv=paths['downloads']+'searchdb.out'
tsv=paths['downloads']+'results_final.txt'
if os.path.exists(tsv):
   printOut('<h2>' + service + ' ' +  version + ' results:</h2>\n')
   tsv2html(tsv, full_dl=tsv, css_classes='redhead')
   #tsv2html(tsv, full_dl='', css_classes='grayhead')
   printOut('<br>\n')

if args.database != "resfinder":
   etable=paths['downloads']+'results_tax.txt'
   if os.path.exists(etable):
      extendedOutButton(buttonText="EXTENDED OUTPUT",table=etable)
      printOut('<br>\n')

# PRINT THE PROGRAM PARAMETERS TO THE HTML OUTPUT FILE
if len(inputFiles) > 0: printInputFilesHtml(inputFiles)

# PRINT THE CONTIGS INFORMATION TO THE HTML OUTPUT FILE
#if args.technology != 'Assembled_Genome': printContigsInfo(runID=runID)


# PRINT FILE DOWNLOAD-BUTTON(S) TO THE HTML OUTPUT FILE
#dlButton('DOWNLOAD RESULTS', paths['outputs']+'kmerfinder.results_tax')
dlButton('RESULTS', requestedFile='results_tax.txt', downloadscript='https://cge.cbs.dtu.dk/cge/download_files2.php')


################################################################################
# LOG THE TIMERS (No need to change this part)
progs.PrintTimers()

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# INDICATE THAT THE WRAPPER HAS FINISHED (No need to change this part)
printDebug("Done")

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE (No need to change this part)
fileZipper(paths['serviceRoot'])
