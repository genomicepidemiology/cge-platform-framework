#!/tools/bin/python
try:
#This script is a wrapper for one of the CGE services
   service = "MGmapper" # name of service 
   version = "1.3" # service version
   
#The script executes the needed procedures and programs
#Programs run within this script is:
# The MGmapper
   
#This script will wait for the output file from the last program executed on CGE 
#cluster, and once received the output will printed to the html page of this
#service. In the end all files created are zipped for storage purposes.
   
# Including the CGE modules!
   import os, sys
   #sys.path.append("/srv/www/src/CGE/CGE_modules")
   sys.path.append("/home/data1/services/CGEpipeline/CGEmodules/")
   from functions_module_ns import * #Imports all wrapper functions
   
#Names of programs (used to save log files etc)
   MGmapper = "MGmapper"
   MGmapperScriptPath = ''
   
   timerMGmapper = 0 #TIME STAMP
   output = "MGmapper.out" # Program output to be printed
   
############################################################################
################################ FUNCTIONS #################################
############################################################################
   
# Argument parser function.
   def getArguments():
      global runId, db_User, args, MGmapperScriptPath, db_IP, webmode
   # SETTING SCRIPT SPECIFIC VARIABLES AS GLOBAL
   #Don't Touch the next couple of lines, these defines the header of the Help output
      parser = ArgumentParser(
         formatter_class=RawDescriptionHelpFormatter,
         usage=('''%(prog)s [-h] -f FILE [FILE ...] -p PATH -t INPUTTYPE -o ORGANISM
   -------------------------------------------------------------------------------
   '''),
         description=('''\
   MGmapper Wrapper
   ------------
      This is the MGmapper wrapper script, which executes the MGmapper service. 
      
      Note! Create a new directory for each of your runs.
  	   
  	   
   '''), epilog=('''
   -------------------------------------------------------------------------------
      '''))
      
      #ADDING ARGUMENTS
      
      #ADD SERVICE SPECIFIC ARGUMENTS HERE!! (use help=SUPPRESS to hide the help)
      parser.add_argument("-T", type=str, dest="progType", default="P", help=" \
                          Select Single-End (S) or Pair-End (P) mapping mode.")
      parser.add_argument("-S", type=str, dest="cutadapt", default="on", help=" \
                          cutadapt, trimming and adaptor removal.")
      parser.add_argument("-m", type=str, dest="minlen", default="30", help=" \
                          Minimal read length after cutadapt.")
      parser.add_argument("-q", type=str, dest="quality", default="30", help=" \
                          Minimal read quality after cutadapt.")
      parser.add_argument("-B", type=str, dest="QUALITY_BASE", default="33", help=" \
                          QUALITY_BASE in cutadapt")
      parser.add_argument("-C", type=str, dest="Chainmode", default="12,1", help=" \
                          Select databases for Chain mode mapping.")
      parser.add_argument("-F", type=str, dest="Fullmode", default="5", help=" \
                          Select databases for Full mode mapping.")
      parser.add_argument("-a", type=str, dest="Contigs", default="", help=" \
                          Select databases for contig assembly.")
      parser.add_argument("-t", type=str, dest="frac", default="0.8", help=" \
                          Fraction of a read that needs to match.")
      #parser.add_argument("-M", type=str, dest="maxInsertSize", default="", help=" \
      #                    Select max Insert Size")
      parser.add_argument("-Q", type=str, dest="adaptor", default="/home/data1/services/MGmapper/MGmapper-1.3/scripts/adapter.txt", help=" \
                          select adapter sequences to be removed by cutadapt.")
      
      # Following arguments are (CMDline ONLY!)
      parser.add_argument("-f", type=str, dest="file", nargs='+', help=" \
                          Filepaths for inputs. (required for cmdline)")
      parser.add_argument("-p", type=str, dest="path", help=" \
                          Path for storing outputs. (Required for cmdline)")
      # Following arguments are (Webserver ONLY!)
      parser.add_argument("--runroot", type=str, dest="runId", help=SUPPRESS) #unique identifier number
      parser.add_argument("--ip", type=str, dest="db_IP", default="NA", help=SUPPRESS) #IP address of user
      parser.add_argument("--uid", type=str, dest="db_User", default="Anonymous", help=SUPPRESS) #User ID
      parser.add_argument("-w", action="store_true", dest="webmode", default=False, help=SUPPRESS) #Webmode
      args = parser.parse_args()
      
      #----------- GATHERING PARSED CMDline OPTIONS AND/OR SETTING DEFAULTS ---------
   
      #HANDLING FIXED ARGUMENTS
      webmode = args.webmode
      db_IP = args.db_IP
      db_User = args.db_User
      if args.runId != None:
         runId = args.runId
         inputFiles = []
         runRoot = runId + "/"
      elif args.file != None and args.path != None:
         #HANDLE CMDLINE
         runId = "Your"
         inputFiles = args.file
         runRoot = args.path
         if runRoot[0] != '/': runRoot = '/'.join([os.getenv("HOME"), runRoot]) # Assigning full path
         if not runRoot[-1] == "/": runRoot += "/" # Adding trailing / if missing
      elif webmode: # HANDLE MISSING runId
         sys.exit("ERROR: No runId was specified, contact technical support for help!<br>\n")
      elif args.path != None: # HANDLE MISSING path
         sys.exit("ERROR: Missing a runroot path, please assign -p /path/to/rundir/\n")
      else: # HANDLE MISSING file
         sys.exit("ERROR: Missing files, No input files was set. Please assign -f filepath [filepath ...]!\n")
      
      # HANDLE SERVICE SPECIFIC ARGUMENTS
      if args.progType == 'S': MGmapperScriptPath = "scripts/MGmapper_SE.pl"
      elif args.progType == 'P': MGmapperScriptPath = "scripts/MGmapper_PE.pl"
      else: sys.exit('Error: Please choose a valid mapping mode!\n')
      
      if args.cutadapt == 'on': args.cutadapt = ''
      else: args.cutadapt = '-S'
      
      if args.Chainmode == '': args.Chainmode = '0'
      if args.Fullmode == '': args.Fullmode = '0'
      
      
      return runId, db_User, db_IP, inputFiles, runRoot, webmode

# This function will get the parameters from the webface and will prepare the parameter file to be sent to the wrapper in the wrapper on the cge machine
   def setInputParams():
      printDebug("setInputParams::START") #DEBUG
      # GLOBAL VARIABLES
      global inputFiles, runRoot, args
      global downloadPath, xmsubLogs, InputPath, finalOutputPath
      
      inputFiles, runRoot = getVars()
      
      #create the needed directories for the webservice to be executed
      createRunDirs(runRoot)
      
      #GETTING PATHS
      downloadPath, xmsubLogs, InputPath, finalOutputPath, dbPath = getPaths()
      
      # Move adaptor file
      if args.adaptor != '' and os.path.exists(args.adaptor):
         try:
            dst = InputPath+args.adaptor.split('/')[-1]
            moveFile(args.adaptor, dst)
            args.adaptor = dst
         except Exception, e: print "Could not move the adaptor file, %s\n"%(e)
      
#      if args.progType == 'S' and len(inputFiles) > 1:
      if args.progType == 'S' and len(inputFiles) >= 1:
         filelist = InputPath +'/filelist.txt'
         if webmode:
            with open(filelist, 'w') as f: f.write('%s\n'%'\n'.join([runRoot+uploadPath+x for x in os.listdir(uploadPath)]))
         else:
            with open(filelist, 'w') as f: f.write('%s\n'%'\n'.join(inputFiles))
         inputarg = " -f %s "%(filelist)
      elif args.progType == 'P':
         printDebug("In Pair-end mode")
         filelist1 = InputPath +'/filelist1.txt'
         filelist2 = InputPath +'/filelist2.txt'
         inputarg = " -I %s -J %s "%(filelist1, filelist2)
         
         # SORT input files in 2 groups (R1 and R2) by file-pattern
         groups, unmatched = Sort2Groups(inputFiles, ['_R1','_R2'])
         printDebug('\nFiles:', '\n'.join(inputFiles),
                    '\nPairs:', '\n'.join(['\t'.join(x) for x in zip(*groups)]),
                    '\nUnmatched:', '\n'.join(unmatched))
         
         # Use Rolf's input parser script to find valid paired fastq files
         #groups = zip(*getFqPairs(uploadPath))
         
         # Sort unmatched files and pair them (remove unpaired files)
         unmatched_groups = SortAndDistribute(unmatched, splits=2)
         #if len(unmatched_groups[0]) > len(unmatched_groups[1]):
         #   unpaired = unmatched_groups[0].pop()
         
         # Print Groups to files
         with open(filelist1, 'w') as f: f.write('\n'.join(groups[0]+unmatched_groups[0])+'\n')
         with open(filelist2, 'w') as f: f.write('\n'.join(groups[1]+unmatched_groups[1])+'\n')
         
         # COMBINE R1 with R2 and PRINT the isolate one per line
         #print '\n'.join(['\t'.join(x) for x in zip(*groups)]+unmatched)
         
#         # Handle .fq/.fastq files as paired end, eg. 2 files per_line
##         with open(InputPath +"/"+ filelist1, 'w') as f1, open(InputPath +"/"+ filelist2, 'w') as f2:
#         with open(filelist1, 'w') as f1, open(filelist2, 'w') as f2:
#            inline = False
#            for file_ in sorted(os.listdir(uploadPath)):
#               if not inline:
#                  f1.write('%s/%s/%s\n'%(runRoot, uploadPath, file_))
#                  inline = True
#               else:
#                  f2.write('%s/%s/%s\n'%(runRoot, uploadPath, file_))
#                  inline = False
#            if inline:
#               printOut('WARNING: a pairless fastq file was encountered!\n')
#               f.write('\n')
#               inline = False
      
      # SETTING PROGRAM COMMANDS
      storageRoot = getScriptPath() # Program paths
      python = "/usr/bin/python " # python -> python2.7 
      perl = "/usr/bin/perl " # perl -> perl5
      MGmapperCmd = perl + storageRoot + MGmapperScriptPath +" -w -v -c 4 -n Results -d MGmapper "+ args.cutadapt +" -m "+ args.minlen +" -B "+ args.QUALITY_BASE +" -q "+ args.quality +" -C '"+ args.Chainmode +"' -F '"+ args.Fullmode +"' -t "+ args.frac + inputarg +" -Q '"+ args.adaptor +"'"
      if args.Contigs != '': MGmapperCmd += " -a '"+ args.Contigs +"'"
      
      return MGmapperCmd

   #THIS SCRIPT WILL EXECUTE THE PROGRAMS IN THE PIPELINE
   def runScripts(progs):
      printDebug("runScripts::START") #DEBUG
      global timerMGmapper
      
      MGmapperCmd = progs #unfolding progs
      
      # CALLING MGmapper SCRIPT
#      executeProgramNOWAIT(MGmapperCmd, finalOutputPath+MGmapper, WorkDir=runRoot, ToQueue=True, wallhours=12, mem=30, procs=4)
      executeProgramNOWAIT(MGmapperCmd, finalOutputPath+MGmapper, WorkDir=runRoot, ToQueue=True, wallhours=12, mem=30, procs=4)
      # Waiting for Done! to show up in the errorlog MGmapper.err, max wait time is 29 hours
      timerMGmapper = waitOn(finalOutputPath + MGmapper +".err", "Done!", interval=300, maxloopsleft=144, maxqueuedtime=72*60*60)

#      this one allow a 2 days wait before start
#      timerMGmapper = waitOn(finalOutputPath + MGmapper +".err", "Done!", interval=120, maxloopsleft=144, maxqueuedtime=48*60*60)

#      timerMGmapper = waitOn(finalOutputPath + MGmapper +".err", "Done!", interval=120, maxloopsleft=144, maxqueuedtime=60*60*4)
      
      printDebug("runScripts::END") #DEBUG

   def ParseTabFileToTable(fp, fp_dl_pre='', d_color='greyhead'):
      if os.path.exists(fp):
         with open(fp, 'r') as f:
            intable = False
            tsize = 0
            for l in f:
               l = l.strip()
               if l == '':
                  if intable:
                     # END TABLE
                     printOut("</table>\n")
                     intable = False
               elif l[0] == '#':
                  # HEADER LINE
                  tmp = l[1:].split('\t')
                  if intable:
                     # Print header line
                     tsize = len(tmp)
                     printOut("<tr><th>%s</th></tr>\n"%("</th><th>".join(tmp)))
                  elif len(tmp) > 1:
                     # No main header exists
                     # Print header line
                     tsize = len(tmp)
                     printOut("<table class='results %s'>\n"%(d_color))
                     printOut("<tr><th>%s</th></tr>\n"%("</th><th>".join(tmp)))
                     intable = True
                  else:
                     # Create new table
                     name = tmp[0]
                     printOut("<table><tr><td><H2>%s:</H2></td><td>\n"%(name))
                     if fp_dl_pre != '': dlButton(fp_dl_pre%(name)+'.gz', 'View All')
                     printOut("</td></tr></table><br>\n")
                     printOut("<table class='results %s'>\n"%(d_color))
                     intable = True
               elif intable:
                  # print table row
                  td = l.split('\t')
                  tdl = len(td)
                  printOut("<tr><td>%s</td></tr>\n"%('</td><td>'.join([str(td[i]) if i < tdl else '&bnsp;' for i in xrange(tsize)])))
            # END TABLE IF OPEN
            if intable: printOut("</table>\n")


############################################################################
##################################  MAIN  ##################################
############################################################################
   showErrorLogs = False
   setDebug(False)
   setPaths(service, version)
   
   try: SetArguments(getArguments())
   except Exception, e: printDebug('Error: Arguments parsing failed! %s\n'%(e))
   
   ################################
   printDebug("Python Version\t"+ str(sys.version),
   		     "System Arguments ::\t"+ str(sys.argv),
   		     "ENV->HOME::\t"+ str(os.getenv("HOME")),
   		     "ENV->PATH::\t"+ str(os.getenv("PATH")),
   		     "Current Working Directory::\t"+ str(os.getcwd()),
   		     "HOST::\t"+ str(os.getenv('HOSTNAME'))) #DEBUG
   
   # SETTING INPUT PARAMETERS AND CREATING DIRECTORIES AND EXECUTING THE PROGRAMS
   try: runScripts(setInputParams())
   except Exception, e: printDebug('Error: runScripts failed! %s\n'%(e))
   
   # PRINT OUTPUT
   #  printOut('.results td, th{font-size: 10pt;}')
   try:
      # Move downloadable files
      moveFile(runRoot +"/MGmapper/unmapped*", downloadPath)
#      moveFile(runRoot +"/MGmapper/*.summary", downloadPath)
      moveFile(runRoot +"/MGmapper/Results.*", downloadPath)
      moveFile(runRoot +"/MGmapper/stat.*txt", downloadPath)
      moveFile(runRoot +"/MGmapper/*contig*", downloadPath)
      #moveFile(runRoot +"/MGmapper/*.bam", downloadPath)
   
   # Print input files
   #if len(inputFiles) > 0: printInputFilesHtml()
   
   # Write Output Tables
      filename=runRoot+'/MGmapper/time.tab'
      if os.path.exists(filename):
         if args.progType == 'P':
            printOut('<h2>Running time in pair-end mode:</h2>\n')
         else:
            printOut('<h2>Running time in single-end mode:</h2>\n')
         ParseTabFileToTable(runRoot+'/MGmapper/time.tab', d_color='redhead')
         printOut('<br>\n')
         printOut('<br>\n')

      filename=runRoot+'/MGmapper/filestat.tab'
      if os.path.exists(filename):
         printOut('<h2>File Statistics:</h2>\n')
         ParseTabFileToTable(runRoot+'/MGmapper/filestat.tab', d_color='redhead')
         printOut('<br>\n')
         printOut('<br>\n')

      filename=runRoot+'/MGmapper/filestat2.tab'
      if os.path.exists(filename):
#         printOut('<h2>File Statistics:</h2>\n')
         ParseTabFileToTable(runRoot+'/MGmapper/filestat2.tab', d_color='redhead')
         printOut('<br>\n')
         printOut('<br>\n')

      filename=runRoot+'/MGmapper/dbCount.tab'
      if os.path.exists(filename):
         printOut('<h2>Size of selected reference sequence databases:</h2>\n')
         ParseTabFileToTable(runRoot+'/MGmapper/dbCount.tab', d_color='redhead')
         printOut('<br>\n')
         printOut('<br>\n')

      filename=runRoot+'/MGmapper/InsertSize.tab'
      if os.path.exists(filename):
         printOut('<h2>Inferred insert size - generated from all reads mapped in chainmode:</h2>\n')
         ParseTabFileToTable(runRoot+'/MGmapper/InsertSize.tab', d_color='redhead')
         printOut('<br>\n')
         printOut('<br>\n')

      filename=runRoot+'/MGmapper/filestat3.tab'
      if os.path.exists(filename):
         printOut('<h2>Overall mapping results:</h2>\n')
         ParseTabFileToTable(runRoot+'/MGmapper/filestat3.tab', d_color='redhead')
#         printOut('<i>NB! Rreads not mapping to the database notPhiX are those remaining after removal of potential positive control reads. Number of reads not mapping is set to 100%</i><br>\n')
         printOut('NB! The column <i>Reads available</i> in the top row is reads that have a biological origin. That read count is set to 100%<br>\n')
         printOut('<br>\n')

      filename=runRoot+'/MGmapper/filestat4.tab'
      if os.path.exists(filename):
#         if args.progType == 'P':
         printOut('<h2>Overall mapping results for uniquely mapped reads (i.e. a read or the mate has only one best hit):</h2>\n')
#         else:
#         printOut('<h2>Overall mapping results for uniquely mapped reads (i.e. a read has only one best hit):</h2>\n')

         ParseTabFileToTable(runRoot+'/MGmapper/filestat4.tab', d_color='redhead')
#         printOut('<i>NB! Rreads not mapping to the database notPhiX are those remaining after removal of potential positive control reads. Number of reads not mapping is set to 100%</i><br>\n')
         printOut('NB! The column <i>Reads available</i> in the top row is reads that have a biological origin. That read count is set to 100%<br>\n')
         printOut('<br>\n')
      
      
      filename=runRoot+'/MGmapper/Chainmode.tab'
      if os.path.exists(filename):
         printOut('<h2>Statistics  - Chainmode mapping. Percentages are in relation to total number of reads that map to database.</h2>\n')
         ParseTabFileToTable(runRoot+'/MGmapper/Chainmode.tab', fp_dl_pre='stat.%s.txt', d_color='bluehead')
         printOut('<br>\n')

      filename=runRoot+'/MGmapper/Chainmode.uniq.tab'
      if os.path.exists(filename):
         printOut('<h2>Statistics  - Chainmode mapping unique reads. Percentages are in relation to total number of reads that map to database.</h2>\n')
         ParseTabFileToTable(runRoot+'/MGmapper/Chainmode.uniq.tab', fp_dl_pre='stat.%s.uniq.txt', d_color='greenhead')
         printOut('<br>\n')
      
      filename=runRoot+'/MGmapper/Fullmode.tab'
      if os.path.exists(filename):
         printOut('<h2>Statistics - Fullmode mapping. Percentages are in relation to total number of reads that map to database.</h2>\n')
         ParseTabFileToTable(runRoot+'/MGmapper/Fullmode.tab', fp_dl_pre='stat.%s.txt', d_color='bluehead')
         printOut('<br>\n')

      filename=runRoot+'/MGmapper/Fullmode.uniq.tab'
      if os.path.exists(filename):
         printOut('<h2>Statistics - Fullmode mapping unique reads. Percentages are in relation to total number of reads that map to database.</h2>\n')
         ParseTabFileToTable(runRoot+'/MGmapper/Fullmode.uniq.tab', fp_dl_pre='stat.%s.uniq.txt', d_color='greenhead')
         printOut('<br>\n')
      
      
      # Add unmapped download buttons
      printOut("<h3>Download excel workbook result files incl graphics:</h3>")
      for rf in [os.path.basename(f) for f in glob.glob(downloadPath +"Results*")]: dlButton(rf+'.gz', rf)
 #     if args.progType == 'P':
      printOut("<i>NB! In PE-mode Results.uniq.xlsx only properly pairs of reads are used if at least one of them has only 1 best hit i.e. second best hit has a lower alignment score.</i><br>")
#      else:
      printOut("<i>NB! In SE-mode Results.uniq.xlsx only use reads where there is only 1 best hit i.e. second best hit has a lower alignment score.</i><br>")

#      printOut("<i>PE-mode: Files named *uniq* only include reads where either a read or its mate pair has been uniquely mapped to a reference sequence i.e. only 1 best alignment score.</i><br>")
#      printOut("<i>SE-mode: Files named *uniq* only include reads that are uniquely mapped to a reference sequence i.e. only 1 best alignment score.</i><br>")
      
      contig_files = glob.glob(downloadPath +"*contig*")
      if len(contig_files) > 0:
         printOut("<h3>Download pileup assembled contig fasta files. SNPs and INDELs shown in lowercase:</h3>")
         for f in map(os.path.basename, contig_files): dlButton(f+'.gz', f)
      
      stat_files = glob.glob(downloadPath +"stat.*txt")
      if len(stat_files) > 0:
         printOut("<h3>Download unix tab separated files without header. All data also included in excel Results.*.xlsx files:</h3>")
         for f in map(os.path.basename, stat_files): dlButton(f+'.gz', f)
      
      unmapped_files = glob.glob(downloadPath +"unmapped*")
      if len(unmapped_files) > 0:
         printOut("<h3>Download fastq files with unmapped reads:</h3>")
         for f in map(os.path.basename, unmapped_files): dlButton(f+'.gz', f)
      
      # Add bam download buttons
#      printOut("DOWNLOAD BAM FILES:")
#      for rf in [os.path.basename(f) for f in glob.glob(downloadPath +"*.bam")]: dlButton(rf+'.gz', rf)
      
      # Print output from program
      #printOutput(finalOutputPath + output) #MGmapper.summary
   except Exception, e: printDebug('Error: Output printing failed! %s\n'%(e))
   
   # chain mode
   # full mode
   
   ## Print Database Variables - used for creating a database of runned server queries
   #db_filepath, db_Date, db_IP, db_Location, db_resfinder_threshold, db_Uploads, db_Input, db_downloads = setDatabase() #setting datbase variables
   #db_str = runId +"\t"+ service +"-"+ version +"\t"+ inputType +"\tNA\t"+ db_Date +"\t"+ db_IP +"\t"+ db_Location +"\t"+ db_N50 +"\t"+ db_Uploads +"\t"+ db_Input +"\t"+ db_downloads
   #if os.path.isfile(db_filepath):
   #	with open(db_filepath, 'a') as f:
   #		f.write(db_str +'\n')
   #else:
   #	printDebug("No record of this run was made!",
   #			   "db_str: db_FolderId\tdb_Service\tdb_Technology\tdb_Scheme\tdb_Date\tdb_IP\tdb_Location\tdb_N50\tdb_Uploads\tdb_Input\tdb_Outputs",
   #			   "db_str: "+ "\t".join(db_str.split()),
   #			   "db_filepath: "+ db_filepath) #DEBUG
   
   try:
      # Printing the error logs
      if showErrorLogs: printErrorLogs(MGmapper)
      # TIME STAMPS / BENCHMARKING
      printTimers([(MGmapper, timerMGmapper)])
      add2DatabaseLog(service+"-"+version, runId, db_User, db_IP, 'NA')
   except Exception, e: printDebug('Error: Finalizing failed! %s\n'%(e))
   
   printDebug(service+"-"+version+" WAS CALLED!!!!") #DEBUG
   
   # GZIP ALL FILES IN THE RUN DIRECTORY
   fileZipper()
except Exception, e: print("ERROR:%s\n"%e)
