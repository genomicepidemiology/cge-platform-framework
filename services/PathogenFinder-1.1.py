#!/usr/bin/env python
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Service structure
#--> The input/arguments are validated
#--> Create service folder and subfolders
#--> The uploaded files are copied to the 'Upload' directory 
#--> Log service submission in SQL
#--> Setup and execution of service specific programs
#--> The success of the service is validated
#--> A HTML output page is created if in webmode
#--> Downloadable files are copied/moved to the 'Download' Directory
#--> The SQL service entry is updated
#--> The files are zipped for long-term storage
import sys, os, time, random

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from assembly_module  import (PrepareAssembly, MakeAssembly,
                              printContigsInfo, AddAssemblyToProgList)
from functions_module import (printDebug, copyFile, program, createServiceDirs,
                              getArguments, paths, makeFileList, fileUnzipper,
                              printInputFilesHtml, fileZipper, PrepareDownload,
                              UpdatePaths, progs, CheckFileType, printOut, 
                              moveFile, setDebug, add2DatabaseLog, dlButton,
                              GraceFullExit, FixPermissions, tsv2html)

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################

################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
setDebug(False)
service, version = "PathogenFinder", "1.1"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
args = getArguments('''
selectionbox	taxModel		-m		VALUE
selectionbox	technology		-t		VALUE
''', allowcmd=True)

# VALIDATE REQUIRED ARGUMENTS
if args.technology is None: GraceFullExit("Error: No technology platform was chosen!\n")
if args.taxModel == None: GraceFullExit("Error: No phylum class or organism was chosen!\n")
if args.uploadPath is None or len(args.uploadPath) < 5 or not os.path.exists(args.uploadPath):
   GraceFullExit("Error: No valid upload path was provided! (%s)\n"%(args.uploadPath))
elif args.uploadPath[-1] != '/': args.uploadPath += '/' # Add endslash

# SET RUN DIRECTORY (No need to change this part)
runID = time.strftime('%w_%d_%m_%Y_%H%M%S_')+'%06d'%random.randint(0,999999)
paths.serviceRoot = '{programRoot}IO/%s/'%(runID)
paths.isolateRoot = '{programRoot}'
paths.add_path('uploads', '{serviceRoot}uploads/')

# SET AND UPDATE PATHS (No need to change this part)
UpdatePaths(service, version, '', '', args.webmode)

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()
stat = paths.Create('uploads')

# LOG SERVICE SUBMISSION IN SQL (No need to change this part)
add2DatabaseLog(service+'-'+version, runID, args.usr, args.ip, args.technology)

# MOVE UPLOADS FROM APP- TO ISOLATE UPLOAD DIRECTORY (No need to change this part)
if stat:
   # Move the uploaded files to the upload directory
   moveFile(args.uploadPath+'*', paths['uploads'])
   # Unzipping uploaded files if zipped
   fileUnzipper(paths['uploads'])
   # GET INPUT FILES from input path
   inputFiles = makeFileList(paths['uploads'])
else:
   GraceFullExit("Error: Could not create upload directory!\n")

# GET CONTIGS (No need to change this part UNLESS you dont need assembly)
if not args.technology in ['Assembled_Genome','Proteome']:
   # ADD THE ASSEMBLER to the program list
   AddAssemblyToProgList()
   # Prepare the Assembly program for execution
   PrepareAssembly(args.technology, inputFiles)
   # Assemble the reads into contigs
   n50 = MakeAssembly(args.technology)
   # The success of the assembler is validated
   status = progs['Assembler'].GetStatus()
   if status != 'Done' or not isinstance(n50, int):
      GraceFullExit("Error: Assembly of the inputfile(s) failed!\n"%(len(inputFiles)))
else:
   # Validate that only 1 file was submitted
   if len(inputFiles) != 1:
      GraceFullExit("Error: Invalid number of contig files (%s)\n"%(len(inputFiles)))
   # Validate that the uploaded file is fasta
   if CheckFileType(inputFiles[0]) != 'fasta':
      GraceFullExit("Error: Invalid contigs format (%s)!\nOnly the fasta format is recognised as a proper contig format.\n"%(CheckFileType(inputFiles[0])))
   # Add contigsPath
   paths.add_path('contigs', paths['inputs']+'contigs.fsa')
   # Copy file to Input directory
   copyFile(inputFiles[0], paths['contigs'])

# ADDING PROGRAMS
pathogenfinder = program( name='PathogenFinder', 
   ptype='python', path='/home/data1/services/PathogenFinder/PathogenFinder-1.1/scripts/PathogenFinder-1.1_pipeline.py',
   workDir='', ssh=True, toQueue=True, wait=False, server='cgebase' if args.webmode else 'cgebase2',
   args=['-i', paths['contigs'],
         '-m', args.taxModel,
         '-R', paths['serviceRoot']
         ]
   )

# EXECUTION OF THE PROGRAMS
pathogenfinder.Execute()
progs.Add2List(pathogenfinder)
pathogenfinder.WaitOn(pattern='DONE!', interval=60, maxloopsleft=60)

# THE SUCCESS OF THE SERVICE IS VALIDATED
status = pathogenfinder.GetStatus()
if status != 'Done': GraceFullExit("Error: Execution of the program failed!\n")

# PREPARE THE DOWNLOADABLE FILE(S)
_ = PrepareDownload(paths['downloads']+'PathogenFinder_cdhit_raw.txt')
_ = PrepareDownload(paths['downloads']+'PathogenFinder_results.txt')

# SWITCH SERVICE OUTPUT WITH HTML OUTPUT
moveFile(paths['outputs']+service+".out", paths['outputs']+service+"stdout.txt")
copyFile(paths['outputs']+"output.html", paths['outputs']+service+".out")

# CREATE THE HTML OUTPUT FILE (No need to change this part)
if args.webmode:
   if os.path.exists(paths['outputs']) and not os.path.exists(paths['outputs']+service+".out"):
      with open(paths['outputs']+service+".out", 'w') as f:
         f.write('<h1>%s-%s Server - Results</h1>'%(service, version))

# PRINT THE STANDARD OUTPUT OF THE PROGRAM TO THE HTML OUTPUT FILE
pathogenfinder.Printstdout()

# PRINT THE PROGRAM PARAMETERS TO THE HTML OUTPUT FILE
if len(inputFiles) > 0: printInputFilesHtml(inputFiles)

# PRINT THE CONTIGS INFORMATION TO THE HTML OUTPUT FILE
if not args.technology in ['Assembled_Genome','Proteome']:
   printContigsInfo(runID=runID)

# PRINT FILE DOWNLOAD-BUTTON(S) TO THE HTML OUTPUT FILE
#check which download files are available and create the download link
downloadable = os.listdir(paths['downloads'])
#print downloadPath

#now create the buttons to download the files
# Create Download buttons for png, svg, phylip, fasta, newick, vcfs
printOut("<p class='s16'><h4>Downloadable files:\n</h4><table><tr>\n")
for el in downloadable:
  #print el
  #print '<br>minchia<br>' + downloadPath
  printOut("<td>\n")
  if '_cdhit_' in el:
    dlButton("RAW MATCHES", el + '.gz') # clstr
  elif '_results.' in el:
    dlButton("RESULTS", el + '.gz') # result file
  printOut("</td>\n")
printOut("</tr></table></p>\n")

################################################################################
# LOG THE TIMERS (No need to change this part)
progs.PrintTimers() 

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# INDICATE THAT THE WRAPPER HAS FINISHED (No need to change this part)
printDebug("Done")

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE (No need to change this part)
fileZipper(paths['serviceRoot'])
