#!/usr/bin/env python

################################################################################
##########                     TODO for SeqSero                      ###########
################################################################################
# TODO: Handle blank/negative results.

################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Service structure
#--> The input/arguments are validated
#--> Create service folder and subfolders
#--> The uploaded files are copied to the 'Upload' directory
#--> Log service submission in SQL
#--> Setup and execution of service specific programs
#--> The success of the service is validated
#--> A HTML output page is created if in webmode
#--> Downloadable files are copied/moved to the 'Download' Directory
#--> The SQL service entry is updated
#--> The files are zipped for long-term storage
import sys, os, random, subprocess, re

from time import time, localtime, strftime, sleep
from pipes import quote

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from assembly_module  import (PrepareAssembly, MakeAssembly,
                              printContigsInfo, AddAssemblyToProgList)
from functions_module import (printDebug, copyFile, program, createServiceDirs,
                              getArguments, paths, makeFileList, fileUnzipper,
                              printInputFilesHtml, fileZipper, PrepareDownload,
                              UpdatePaths, progs, CheckFileType, printOut,
                              moveFile, setDebug, add2DatabaseLog, dlButton,
                              GraceFullExit, FixPermissions, tsv2html)

import re

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################

class program_extended(program):
   ''' This Class defines a program structure
      USAGE:
         assembler = proglist.add_program(
            program(name='Assembler',
               path='/panfs1/cge-servers/Assembler/Assembler-1.0'+
                    '/scripts/mlst_wrapper_denovo_2.0.py',
               timer=0,
               ptype='python',
               workdir='',
               ToQueue=False,
               Wait=True,
               args='')
            )
         assembler.AppendArgs(['velvet'])
         assembler.Execute()
   '''
   def __init__(self, name, path=None, timer=0, ptype=None, workDir=None, ssh=True, toQueue=True, wait=False, args=None, walltime=2, mem=4, procs=1, nodes="1"):
      # INIT VALUES
      self.path = path
      self.name = name
      self.timer = timer
      self.ptype = ptype
      self.workDir = workDir
      self.toQueue = toQueue
      self.wait = wait
      self.args = []
      self.stderr = "%s/%s.err"%(paths['logs'], name)
      self.stdout = "%s/%s.out"%(paths['outputs'], name) if os.path.exists(paths['outputs']) else "%s/%s.out"%(paths['logs'], name)
      self.queue = 'cge'
      self.wt = walltime # Maximum amount of hours required to complete program
      self.mem = mem     # GB RAM allocation requirement
      self.procs = procs # Number of processors required for the job
      self.nodes = nodes # Number of nodes, primarily used to control where the job is executed ex. nodes=cgebase
      self.p = None      # The Subprocess Object
      self.ssh = ssh
      self.server = 'cgebase'
      self.status = 'Initialised'
      if not workDir: self.workDir = paths['outputs']
      self.queue = 'www'
      if args: self.AppendArgs(args)
   def GetCMD(self):
      ''' This function combines and return the commandline call of the program. '''
      printDebug("Get cmd.-.-.")
      cmd = []
      if self.ptype is not None:
         cmd.append(self.ptype)
         printDebug("ptype: "+self.ptype)
         #if paths[self.ptype] != '': cmd.append(paths[self.ptype])
         #elif os.path.exists(self.ptype): cmd.append(self.ptype)
         if os.path.exists(self.ptype): printDebug("Found "+self.ptype)
      if self.path is not None and os.path.exists(self.path):
         cmd.append(self.path)
         cmd.extend([str(x) if not isinstance(x, (unicode)) else x.encode('utf-8') for x in [] + [quote(x) for x in self.args]])
      printDebug("RETURNING:")
      printDebug(' '.join(cmd))
      return ' '.join(cmd)
   def Execute(self):
      ''' This function Executes the program with set arguments. '''
      # SETTING WORKDIR if not set
      if self.toQueue:
         otherArgs = ''
         if self.wait: otherArgs += "-K " # ADDING -K argument if wait() is forced
         tmpfile = self.workDir+'/cmd.csh'
         with open(tmpfile, 'w') as f:
            f.write("#!/usr/bin/csh\ncd %s\ntouch %s\n%s\n\n"%(self.workDir,
                                                               self.stderr,
                                                               self.GetCMD()))
         os.chmod(tmpfile, 0744)
         # QSUB INFO :: run_time_limit(walltime, dd:hh:mm:ss), memory(mem, up to 100GB *gigabyte), processors(ppn, up to 16) # USE AS LITTLE AS NEEDED!!!
         cmd = ("ssh %s /usr/bin/qsub -l nodes=%s:ppn=%d,walltime=%d:00:00,mem=%dg -r y -d  %s -e %s -o %s %s %s")%(self.server, self.nodes, self.procs, self.wt, self.mem, self.workDir, self.stderr, self.stdout, otherArgs, tmpfile ) # '-q %s' self.queue
         printDebug("\n\nExecute %s...\n%s\n%s\n" % (self.name, cmd, self.GetCMD()))
      else:
         cmd = "cd %s;%s > %s >& %s"%(self.workDir, self.GetCMD(), self.stdout,
                                      self.stderr)
         if self.ssh: cmd = "ssh %s '%s'"%(self.server, cmd)
         printDebug("\n\nExecute %s...\n%s\n" % (self.name, cmd)) #DEBUG
      self.status = 'Executing'
      # EXECUTING PROGRAM
      self.UpdateTimer(-time()) # TIME START
      if self.wait:
         self.p = subprocess.Popen(cmd, shell=True, executable="/bin/bash").wait()
         self.status = 'Done'
      else: # WaitOn should be called to determine if the program has ended
         self.p = subprocess.Popen(cmd, shell=True, executable="/bin/bash")
      self.UpdateTimer(time()) # TIME END
      printDebug("timed: %s" % (self.GetTime()))

################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
setDebug(False)
service, version = "SeqSero", "1.1"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
args = getArguments('''
selectionbox   data_type   -t   VALUE  -t   ''
''')

# VALIDATE REQUIRED ARGUMENTS
if args.data_type == None: GraceFullExit("Error: No data type was chosen!\n")

# SET RUN DIRECTORY (No need to change this part)
runID = strftime('%w_%d_%m_%Y_%H%M%S_')+str(random.randint(100000,999999))
paths.serviceRoot = '{programRoot}IO/%s/'%(runID)
paths.isolateRoot = '{programRoot}'
paths.add_path('uploads', '{serviceRoot}uploads/')

# SET AND UPDATE PATHS (No need to change this part)
UpdatePaths(service, version, '', '', args.webmode)

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()
stat = paths.Create('uploads')

# LOG SERVICE SUBMISSION IN SQL (No need to change this part)
add2DatabaseLog(service+'-'+version, runID, args.usr, args.ip, "mixed")

# MOVE UPLOADS FROM APP- TO ISOLATE UPLOAD DIRECTORY (No need to change this part)
if stat:
   # Move the uploaded files to the upload directory
   moveFile(args.uploadPath+'/*', paths['uploads'])
   # Unzipping uploaded files if zipped
   fileUnzipper(paths['uploads'])
   # GET INPUT FILES from input path
   inputFiles = makeFileList(paths['uploads'])
else:
   GraceFullExit("Error: Could not create upload directory!\n")

#
#----CHANGE THIS----#
#
# GET CONTIGS (No need to change this part UNLESS you dont need assembly)
#if args.technology != 'Assembled_Genome':
   # ADD THE ASSEMBLER to the program list
#   AddAssemblyToProgList(progs)
   # Prepare the Assembly program for execution
#   PrepareAssembly(args.technology, inputFiles)
   # Assemble the reads into contigs
#   n50 = MakeAssembly(args.technology)
   # The success of the assembler is validated
#   status = progs['Assembler'].GetStatus()
#   if status != 'Done' or not isinstance(n50, int):
#      GraceFullExit("Error: Assembly of the inputfile(s) failed!\n"%(len(inputFiles)))
#else:
   # Validate that only 1 file was submitted
#   if len(inputFiles) != 1:
#      GraceFullExit("Error: Invalid number of contig files (%s)\n"%(len(inputFiles)))
   # Validate that the uploaded file is fasta
#   if CheckFileType(inputFiles[0]) != 'fasta':
#      GraceFullExit("Error: Invalid contigs format (%s)!\nOnly the fasta format is recognised as a proper contig format.\n"%(CheckFileType(inputFiles[0])))
   # Add contigsPath
#   paths.add_path('contigs', paths['inputs']+'contigs.fsa')
   # Copy file to Input directory
#   copyFile(inputFiles[0], paths['contigs'])
#
#-------------------#
#


# ADDING PROGRAMS
seq_sero = progs.AddProgram(program_extended(
   name='SeqSero', path=paths['scripts']+'SeqSero.py', timer=0,
   ptype='python', toQueue=True, wait=False, workDir=paths.serviceRoot,
   nodes="cgebase", args=['-m', args.data_type] ))
# Appending input files
seq_sero.AppendArgs("-i")
for input_file in inputFiles:
   seq_sero.AppendArgs([input_file])

# EXECUTION OF THE PROGRAMS
seq_sero.Execute()
if(args.data_type == '4'):
   # Assembled genomes gets 200 secs
   seq_sero.WaitOn(pattern='Done!', interval=10, maxloopsleft=20, ePattern='error')
else:
   # Raw data get 600 secs
   seq_sero.WaitOn(pattern='Done!', interval=30, maxloopsleft=20, ePattern='error')

# THE SUCCESS OF THE SERVICE IS VALIDATED
# status = seq_sero.GetStatus()
# if status != 'Result': GraceFullExit("Error: Execution of the program failed!\nStatus: "+status+"\n")

#
# Parsing the results of SeqSero
#
if args.webmode:
   html_output = '<h1>%s-%s Server - Results</h1>\n\n'%(service, version)
   Opred = ""
   H1pred = ""
   H2pred = ""
   profpred = ""
   serotype = ""
   stdout_org_fh = open(paths['outputs']+service+".stdout", "w")
   with open(paths['outputs']+service+".out", "r") as ouputfile:
      for line in ouputfile:
         stdout_org_fh.write(line)

         search_Opred = re.search( r'O antigen prediction:\s+(\S+)', line, re.M|re.I)
         search_H1pred = re.search( r'H1 antigen prediction\(fliC\):\s+(\S+)', line, re.M|re.I)
         search_H2pred = re.search( r'H2 antigen prediction\(fljB\):\s+(\S+)', line, re.M|re.I)
         search_profpred = re.search( r'Predicted antigenic profile:\s+(\S+)', line, re.M|re.I)
         search_type = re.search( r'Predicted serotype\(s\):\s+(\S+)(.*)', line, re.M|re.I)

         if search_Opred:
            Opred = search_Opred.group(1)
         elif search_H1pred:
            H1pred = search_H1pred.group(1)
         elif search_H2pred:
            H2pred = search_H2pred.group(1)
         elif search_profpred:
            profpred = search_profpred.group(1)
         elif search_type:
            serotype = search_type.group(1)
            if(search_type.group(2) and serotype != "N/A"):
               serotype = serotype+search_type.group(2)
               serotype.rstrip()

   stdout_org_fh.write("\n\n\""+serotype+"\"\n\n")
   stdout_org_fh.close()

   html_output += "<center><h3>Serotype:</h3><br><h1>"+serotype+"</h1><br><br>\n"
   html_output += "<h4><table border=\"0\" style=\"width:200px\">\n"
   html_output += "  <tr>\n"
   html_output += "     <th>Antigenic profile</th>\n"
   html_output += "     <th>"+profpred+"</th>\n"
   html_output += "  </tr>\n"
   html_output += "  <tr>\n"
   html_output += "     <td>O antigen</td>\n"
   html_output += "     <td>"+Opred+"</td>\n"
   html_output += "  </tr>\n"
   html_output += "  <tr>\n"
   html_output += "     <td>H1 antigen</td>\n"
   html_output += "     <td>"+H1pred+"</td>\n"
   html_output += "  </tr>\n"
   html_output += "  <tr>\n"
   html_output += "     <td>H2 antigen</td>\n"
   html_output += "     <td>"+H2pred+"</td>\n"
   html_output += "  </tr>\n"
   html_output += "</table></h4>\n"
   html_output += "<br>\n"
   html_output += "<table border=\"0\" style=\"width:400px\">\n"
   html_output += "  <tr>\n"
   if serotype == "N/A":
      html_output += "     <td><p>The predicted antigenic profile does not exist in the White-Kauffmann-Le Minor scheme</p></td>\n"
   elif serotype == "See comments below*":
      html_output += "     <td><p>\n"
      html_output += "        <span style=\"color:#C80000\">*Additional characterization is necessary to assign a serotype to this strain. Commonly circulating strains of serotype Enteritidis are sdf+, although sdf- strains of serotype Enteritidis are known to exist. Serotype Gallinarum is typically sdf- but should be quite rare. Sdf- strains of serotype Enteritidis and serotype Gallinarum can be differentiated by phenotypic profile or genetic criteria.</span><br><br>"
      html_output += "        The serotype(s) is/are the only serotype(s) with the indicated antigenic profile currently recognized in the Kauffmann White Scheme.  New serotypes can emerge and the possibility exists that this antigenic profile may emerge in a different subspecies.  Identification of strains to the subspecies level should accompany serotype determination; the same antigenic profile in different subspecies is considered different serotypes."
      html_output += "     </p></td>"
   else:
      html_output += "     <td><p>The serotype(s) is/are the only serotype(s) with the indicated antigenic profile currently recognized in the Kauffmann White Scheme.  New serotypes can emerge and the possibility exists that this antigenic profile may emerge in a different subspecies.  Identification of strains to the subspecies level should accompany serotype determination; the same antigenic profile in different subspecies is considered different serotypes.</p></td>\n"
   html_output += "  </tr>\n"
   html_output += "</table>"
   html_output += "</center><br><br><hr>\n"

   with open(paths['outputs']+service+".out", 'w') as f:
      f.write(html_output)

# CREATE THE HTML OUTPUT FILE (No need to change this part)
if args.webmode:
   if os.path.exists(paths['outputs']) and not os.path.exists(paths['outputs']+service+".out"):
      with open(paths['outputs']+service+".out", 'w') as f:
         f.write('<h1>%s-%s Server - Results</h1>'%(service, version))

# PRINT THE STANDARD OUTPUT OF THE PROGRAM TO THE HTML OUTPUT FILE
seq_sero.Printstdout()

# PREPARE THE DOWNLOADABLE FILE(S)
#_ = PrepareDownload(paths['serviceRoot']+'dummy_dir/dummy_txt_file.txt')
#_ = PrepareDownload(paths['serviceRoot']+'dummy_dir/dummy_tab_file.tsv')

# PRINT TAB FILE(S) AS RESULT TABLES TO THE HTML OUTPUT FILE
#tsv=paths['downloads']+'dummy_tab_file.tsv'
#if os.path.exists(tsv):
#   printOut('<h2>Dummy Tab-File:</h2>\n')
#   tsv2html(tsv, full_dl=tsv, css_classes='redhead')
#   printOut('<br>\n')

# PRINT THE PROGRAM PARAMETERS TO THE HTML OUTPUT FILE
if len(inputFiles) > 0: printInputFilesHtml(inputFiles)

# PRINT THE CONTIGS INFORMATION TO THE HTML OUTPUT FILE
#if args.technology != 'Assembled_Genome': printContigsInfo(runID=runID)

# PRINT FILE DOWNLOAD-BUTTON(S) TO THE HTML OUTPUT FILE
#dlButton('RESULTS', 'dummy_txt_file.txt')

################################################################################
# LOG THE TIMERS (No need to change this part)
progs.PrintTimers()

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# INDICATE THAT THE WRAPPER HAS FINISHED (No need to change this part)
printDebug("Done")

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE (No need to change this part)
fileZipper(paths['serviceRoot'])
