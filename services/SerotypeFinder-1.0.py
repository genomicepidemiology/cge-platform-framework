#!/usr/bin/env python

#This script is a wrapper for one of the CGE services
service = "SerotypeFinder" # name of service 
version = "1.0" # service version

#The script executes the needed procedures and programs
#Programs run within this script is:
# The assembler
# The Resistance Finder

#This script will wait for the output file from the last program executed on CGE 
#cluster, and once received the output will printed to the html page of this
#service. In the end all files created are zipped for storage purposes.

# Including the CGE modules!
import sys
sys.path.append("/srv/www/src/CGE/CGE_modules")
from functions_module_ns import * #Imports all wrapper functions
from assembly_module_ns import * #Imports all assembly code (functions)

#Names of programs (used to save log files etc)
SF = "SerotypeFinder"

# Internal Program paths
SFScriptPath = "scripts/SerotypeFinder-1.0.pl" # internal path in the storage root.

timerSF = 0 #TIME STAMP
output = SF +".out" # Program output to be printed
inputType = ""
threshold = "" 
antibiotic = ""
minlength = ""


#This dictionary will contain the avg sizes associated to argf schemes
#avgGenomeSizeDict = {}
avgGenomeSizeDict["aminoglycoside"] = "1816480"
avgGenomeSizeDict["trimethoprim"] = "1525460"
avgGenomeSizeDict["phenicol"] = "2257487"
avgGenomeSizeDict["sulphonamide"] = "2961751"
avgGenomeSizeDict["xxx"] = "1697761"
avgGenomeSizeDict["quinolone"] = "2552482"
avgGenomeSizeDict["vancomycin"] = "1863286"
avgGenomeSizeDict["tetracycline"] = "1852352"
avgGenomeSizeDict["macrolide"] = "3841767"
avgGenomeSizeDict["beta-lactamase"] = "4713259"
avgGenomeSizeDict["plasmid"] = "2257487"
avgGenomeSizeDict["ammy"] = "2134864"
avgGenomeSizeDict["lalal"] = "4644214"


############################################################################
################################ FUNCTIONS #################################
############################################################################

# Argument parser function.
def getArguments():
  global runId, db_User, webmode, runRoot, db_IP
  # SETTING SCRIPT SPECIFIC VARIABLES AS GLOBAL
  global threshold, antibiotic, inputType, minlength
  #Don't Touch the next couple of lines, these defines the header of the Help output
  parser = ArgumentParser(
    formatter_class=RawDescriptionHelpFormatter,
    usage=('''%(prog)s [-h] -f FILE [FILE ...] -p PATH -t INPUTTYPE [-T THRESHOLD] [-a ANTIBIOTIC] [-L MINLENGTH]
--------------------------------------------------------------------------------
'''),
    	description=('''\
SerotypeFinder Wrapper
-----------------
    This is the SerotypeFinder wrapper script, which executes the SerotypeFinder service. 

    Note! Create a new directory for each of your runs.
 
'''), epilog=('''
--------------------------------------------------------------------------------
    '''))

  #ADDING ARGUMENTS

  #ADD SERVICE SPECIFIC ARGUMENTS HERE!! (use help=SUPPRESS to hide the help)
  parser.add_argument("-t", type=str, dest="inputType", default="assembled_genome", help=" \
                      The technology which the reads are based on. Eg. \
                      Assembled_Genome, Paired_End_Reads, Illumina, 454, \
                      454_Paired_End_Reads, Ion_Paired_End_Reads, Ion_Torrent.\
                      (REQUIRED)")
  parser.add_argument("-T", type=str, dest="threshold", default="98.00", help=" \
                      The threshold. Eg. _____________________________________ \
                      100.00 = 100 percent____________________________________ \
                      98.00 = 98 percent _____________________________________ \
                      95.00 = 95 percent _____________________________________ \
                      90.00 = 90 percent _____________________________________ \
                      85.00 = 85 percent _____________________________________ \
                      ")
  parser.add_argument("-a", type=str, dest="antibiotic", default="H_type,O_type", help=" \
                      The serotype database to look in Eg. ___________________ \
                      H_type = H typing datbase ______________________ \
                      O_type = O typing database ____________________ \
                      ")
  parser.add_argument("-L", type=str, dest="minlength", default="0.60", help=" \
                      The minlength. Eg. ____________________________________ \
                      1.00 = 100 percent______________________________________ \
                      0.80 = 80 percent ______________________________________ \
                      0.60 = 60 percent ______________________________________ \
                      0.40 = 40 percent ______________________________________ \
                      0.20 = 20 percent ______________________________________ \
                      ")

  # Following arguments are (CMDline ONLY!)
  parser.add_argument("-f", type=str, dest="file", nargs='+', help=" \
                      Filepaths for inputs. (required for cmdline)")
  parser.add_argument("-p", type=str, dest="path", help=" \
                      Path for storing outputs. (Required for cmdline)")
  # Following arguments are (Webserver ONLY!)
  parser.add_argument("--runroot", type=str, dest="runId", help=SUPPRESS) #unique identifier number
  parser.add_argument("--ip", type=str, dest="db_IP", default="NA", help=SUPPRESS) #IP address of user
  parser.add_argument("--uid", type=str, dest="db_User", default="Anonymous", help=SUPPRESS) #User ID
  parser.add_argument("-w", action="store_true", dest="webmode", default=False, help=SUPPRESS) #Webmode
  args = parser.parse_args()

  #----------- GATHERING PARSED CMDline OPTIONS AND/OR SETTING DEFAULTS ---------

  #HANDLING FIXED ARGUMENTS
  webmode = args.webmode
  db_IP = args.db_IP
  db_User = args.db_User
  if args.runId != None:
    runId = args.runId
    inputFiles = []
    runRoot = runId + "/"
  elif args.file != None and args.path != None:
    #HANDLE CMDLINE
    runId = "Your"
    inputFiles = args.file
    runRoot = args.path
    if runRoot[0] != '/': runRoot = '/'.join([os.getenv("HOME"), runRoot]) # Assigning full path
    if not runRoot[-1] == "/": runRoot += "/" # Adding trailing / if missing
  elif webmode: # HANDLE MISSING runId
    sys.exit("ERROR: No runId was specified, contact technical support for help!<br>\n")
  elif args.path != None: # HANDLE MISSING path
    sys.exit("ERROR: Missing a runroot path, please assign -p /path/to/rundir/\n")
  else: # HANDLE MISSING file
    sys.exit("ERROR: Missing files, No input files was set. Please assign -f filepath [filepath ...]!\n")

  # HANDLE SERVICE SPECIFIC ARGUMENTS
  if args.inputType == None: sys.exit("No input Type was specified!\n")
  inputType = args.inputType
  threshold = args.threshold
  antibiotic = args.antibiotic
  minlength = args.minlength
  
  return runId, db_User, db_IP, inputFiles, runRoot, webmode

# This function will get the parameters from the webface and will prepare the parameter file to be sent to the wrapper in the wrapper on the cge machine
def setInputParams():
  printDebug("setInputParams::START") #DEBUG
  # GLOBAL VARIABLES
  global inputFiles
  global downloadPath, xmsubLogs, InputPath, finalOutputPath
  
  inputFiles, runRoot = getVars()
  
  # CHECK IF THE CORRECT NUMBER OF FILES WERE UPLOADED (ASSEBLER)
  CheckFiles(inputFiles, inputType) # THIS FUNCTION EXIT AT ERRORS
  
  # create the needed directories for the webservice to be executed
  createRunDirs(runRoot)
    
  if inputType!="Assembled_Genome":
    CreateAssemblyDir(runRoot)

  #GETTING PATHS
  downloadPath, xmsubLogs, InputPath, finalOutputPath, dbPath = getPaths()

  # GETTING ASSEMBLED GENOME OR SETTING ASSEMBLER CMD
  if inputType=="Assembled_Genome":
    # copy the uploaded file in to the mlst input directory
    copyFile(inputFiles[0], InputPath + InputName)
    assemblerCmd = "NA"
  else:
    #Setting the assembly command
    assemblerCmd = setAssemblyCmd(inputType, xmsubLogs, InputPath, InputName, inputFiles)

  # SETTING PROGRAM COMMANDS
  storageRoot = getScriptPath() # Program paths
  python = "/usr/bin/python " # python -> python2.7 
  perl = "/usr/bin/perl " # perl -> perl5
  SFCmd = perl + storageRoot + SFScriptPath +" -d "+ antibiotic +" -k "+ threshold +" -l "+ minlength +" -i "+ InputPath + InputName

  return assemblerCmd, SFCmd

#THIS SCRIPT WILL EXECUTE THE PROGRAMS IN THE PIPELINE
# Assebler (denovo) -> Species Finder (Reads) -> Species Finder (Contigs) -> MUMMER -> DBalignComparison
def runScripts(progs):
  printDebug("runScripts::START") #DEBUG
  global timerSF
  
  assemblerCmd, SFCmd = progs #unfolding progs

  # CALLING ASSEMBLER
  if inputType!="Assembled_Genome": # then we need to execute the genome assembler first
    # CALLING ASSEMBLER
    timerAssembler = MakeAssembly(assemblerCmd)

  # CALLING SF SCRIPT
  executeProgramNOWAIT(SFCmd, finalOutputPath + SF)
  timerSF = waitOn(finalOutputPath + SF +".err", "DONE!") #waiting for DONE! to show up in the errorlog MLST.err
  
  printDebug("runScripts::END") #DEBUG


############################################################################
##################################  MAIN  ##################################
############################################################################
showErrorLogs = False
setDebug(False)
setPaths(service, version)
runRoot = SetArguments(getArguments())

################################
printDebug("Python Version\t"+ str(sys.version),
		   "System Arguments ::\t"+ str(sys.argv),
		   "ENV->HOME::\t"+ str(os.getenv("HOME")),
		   "ENV->PATH::\t"+ str(os.getenv("PATH")),
		   "Current Working Directory::\t"+ str(os.getcwd()),
		   "HOST::\t"+ str(os.getenv('HOSTNAME'))) #DEBUG

# SETTING INPUT PARAMETERS AND CREATING DIRECTORIES AND EXECUTING THE PROGRAMS
runScripts(setInputParams()) 

# PRINT OUTPUT
printOutput(finalOutputPath + output)
# PRINT INPUT FILES
printInputFilesHtml()
  
# PRINT CONTIGS INFO
if inputType != "Assembled_Genome":
  db_N50 = showContigsInfo() #also saving the score to a database_variable
  # COPY THE CONTIGS TO THE DOWNLOAD DIRECTORY (compress if True)
  #copyToDownloadDir(downloadPath, True)
  tmpWebDir = getGlobal('tmpWebDir') #getting the global tmpWebDir from functions_module
  wwwRoot = getGlobal('wwwRoot') #getting the global tmpWebDir from functions_module
  try:
    createZipDir(tmpWebDir + runId +"_contigs.fsa.gz", InputPath + InputName)
  except Exception as e:
    printDebug('ERROR WITH ZIPPING:: tmpWebDir: '+ tmpWebDir +' wwwRoot: '+ wwwRoot +' Input: '+ InputPath + InputName, e)
  
  # PRINTING CONTIGS DOWNLOAD LINK
  if inputType != "Assembled_Genome":
    printOut("<center>")
    linkButton(wwwRoot+'/tmp/'+ runId +"_contigs.fsa.gz", 'DOWNLOAD CONTIGS')
    printOut("</center><br><br>")
else:
  db_N50 = 'NA'

## Print Database Variables - used for creating a database of runned server queries
#db_filepath, db_Date, db_IP, db_Location, db_resfinder_threshold, db_Uploads, db_Input, db_downloads = setDatabase() #setting datbase variables
#db_str = runId +"\t"+ service +"-"+ version +"\t"+ inputType +"\t"+ antibiotic +"\t"+ db_Date +"\t"+ db_IP +"\t"+ db_Location +"\t"+ db_N50 +"\t"+ threshold +"\t"+ db_Uploads +"\t"+ db_Input +"\t"+ db_downloads
#if os.path.isfile(db_filepath):
#	with open(db_filepath, 'a') as f:
#		f.write(db_str +'\n')
#else:
#	printDebug("No record of this run was made!",
#			   "db_str: db_FolderId\tdb_Service\tdb_Technology\tAntibiotic Scheme\tdb_Date\tdb_IP\tdb_Location\tdb_N50\tdb_resfinder_threshold\tdb_Uploads\tdb_Input\tdb_Outputs",
#			   "db_str: "+ "\t".join(db_str.split()),
#			   "db_filepath: "+ db_filepath) #DEBUG

# Printing the error logs
if showErrorLogs: printErrorLogs(SF)


# TIME STAMPS / BENCHMARKING
try:
   printTimers([(Assembler, timerAssembler),(SF, timerSF)])
   add2DatabaseLog(service+"-"+version, runId, db_User, db_IP, inputType)
   printDebug("db_User = %s, webmode = %s, len(runId) = %s\n"%(db_User, webmode, len(runId)))
   # Files from anonymous users will not be deleted, as the result files are not downloadable if they are not there.Therefore the following if/else statement was removed
   #if db_User == 'Anonymous' and webmode and len(runId) > 20: #Only files from a specific/logged in user will be saved 
   #  #Removing all files (removing runRoot!)
   #  subprocess.Popen("rm -rf "+ runRoot, shell=True).wait() ## Removed to get files for download
   #else:
   #  printDebug(service+"-"+version+" WAS CALLED!!!!") #DEBUG
   #  # GZIP ALL FILES IN THE RUN DIRECTORY
   #   fileZipper()
   printDebug(service+"-"+version+" WAS CALLED!!!!") #DEBUG
   # GZIP ALL FILES IN THE RUN DIRECTORY
   fileZipper()
except Exception, e: printDebug("Error: %s"(e))
