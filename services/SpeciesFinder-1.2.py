#!/usr/bin/env python
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Service structure
#--> The input/arguments are validated
#--> Create service folder and subfolders
#--> The uploaded files are copied to the 'Upload' directory 
#--> Log service submission in SQL
#--> Setup and execution of service specific programs
#--> The success of the service is validated
#--> A HTML output page is created if in webmode
#--> Downloadable files are copied/moved to the 'Download' Directory
#--> The SQL service entry is updated
#--> The files are zipped for long-term storage
import sys, os, time, random

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from assembly_module  import (PrepareAssembly, MakeAssembly,
                              printContigsInfo, AddAssemblyToProgList)
from functions_module import (printDebug, copyFile, program, createServiceDirs,
                              getArguments, paths, makeFileList, fileUnzipper,
                              printInputFilesHtml, fileZipper, PrepareDownload,
                              UpdatePaths, progs, CheckFileType, printOut, 
                              moveFile, setDebug, add2DatabaseLog, dlButton,
                              GraceFullExit, FixPermissions, tsv2html)

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################

################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
setDebug(True)
service, version = "SpeciesFinder", "1.2"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
args = getArguments('''
''', allowcmd=True)

# SET RUN DIRECTORY (No need to change this part)
if args.reuseid:
  #/srv/www/secure-upload/isolates/5_16_4_2015_162_299_423438//0/
  runID = [x for x in args.uploadPath.split('/') if x!=''][-2]
else:
  runID = time.strftime('%w_%d_%m_%Y_%H%M%S_')+'%06d'%random.randint(0,999999)
paths.serviceRoot = '{programRoot}IO/%s/'%(runID)
paths.isolateRoot = '{programRoot}'
paths.add_path('uploads', '{serviceRoot}uploads/')

# SET AND UPDATE PATHS (No need to change this part)
UpdatePaths(service, version, '', '', args.webmode)

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()
stat = paths.Create('uploads')

# LOG SERVICE SUBMISSION IN SQL (No need to change this part)
#add2DatabaseLog(service+'-'+version, runID, args.usr, args.ip, args.technology)

# MOVE UPLOADS FROM APP- TO ISOLATE UPLOAD DIRECTORY (No need to change this part)
if stat:
   # Move the uploaded files to the upload directory
   moveFile(args.uploadPath+'/*', paths['uploads'])
   # Unzipping uploaded files if zipped
   fileUnzipper(paths['uploads'])
   # GET INPUT FILES from input path
   inputFiles = makeFileList(paths['uploads'])
else:
   GraceFullExit("Error: Could not create upload directory!\n")

# GET CONTIGS (No need to change this part UNLESS you dont need assembly)
#if args.technology != 'Assembled_Genome':
#   # ADD THE ASSEMBLER to the program list
#   AddAssemblyToProgList()
#   # Prepare the Assembly program for execution
#   PrepareAssembly(args.technology, inputFiles)
#   # Assemble the reads into contigs
#   n50 = MakeAssembly(args.technology)
#   # The success of the assembler is validated
#   status = progs['Assembler'].GetStatus()
#   if status != 'Done' or not isinstance(n50, int):
#      GraceFullExit("Error: Assembly of the inputfile(s) failed!\n"%(len(inputFiles)))
#else:
# Validate that only 1 file was submitted
if len(inputFiles) != 1:
   GraceFullExit("Error: Invalid number of contig files (%s)\n"%(len(inputFiles)))

# ADDING PROGRAMS
speciesfinder = progs.AddProgram(program(
   name='speciesfinder', path=paths['scripts']+'SpeciesFinder-1.2.py', timer=0,
   ptype='python', toQueue=True, wait=False, workDir='',
   server='cgebase' if args.webmode else 'cgebase2',
   args=[inputFiles[0],
         '--sample', paths['downloads']]))

# ADDING MORE ARGUMENTS
FileType = CheckFileType(inputFiles[0])
if FileType == 'fasta':
   # Add contigsPath
   paths.add_path('contigs', paths['inputs']+'contigs.fsa')
   # Copy file to Input directory
   copyFile(inputFiles[0], paths['contigs'])
   speciesfinder.AppendArgs(['--contigs'])
elif FileType == 'fastq':
   speciesfinder.AppendArgs(['--Mbases','50'])
   with open(inputFiles[0],"r") as f: firstLine = f.readline()
   if len(firstLine.split(":")) == 3: speciesfinder.AppendArgs(['--no_indel'])

# EXECUTION OF THE PROGRAMS
speciesfinder.Execute()
speciesfinder.WaitOn(pattern='Done', interval=10, maxloopsleft=20)

# THE SUCCESS OF THE SERVICE IS VALIDATED
status = speciesfinder.GetStatus()
if status != 'Done': GraceFullExit("Error: Execution of the program failed!\n")

# CREATE THE HTML OUTPUT FILE (No need to change this part)

if args.webmode:
   if os.path.exists(paths['outputs']) and not os.path.exists(speciesfinder.stdout):
      with open(speciesfinder.stdout, 'w') as f:
         f.write('<h1>%s-%s Server - Results</h1>'%(service, version))
         

# Finding the species, match and status from the output.
species, match, status = '', '', ''
with open(speciesfinder.stdout, 'r') as f:
   try:
      match, status = f.readline().split()[1:]
   except:
      speciesfinder.Printstdout()
   else:
      # Open sample_dir/ssu.out.tab and extract coverage + identity (quality)
    
      # Open /panfs1/cge/people/metteb/taxonomy/training_set.txt and get the Species ID translated
      with open('/panfs1/cge-servers/KmerFinder/KmerFinder-1.1/database/training_set.desc0','r') as fdict:
         for line in fdict:
            if match in line: species = line.split("\t")[-1]

if species == '': species = 'Not found!'
printOut('<style type="text/css">.results th{color:#FFF;background-color:#3956A6;font-size:16px;} .results td{background-color:#6EBE50;font-size:16px;}</style>')
printOut('<table class="center results" width="70%"><tr><th>Species</th><th>Match</th><th>Confidence of result</th></tr>')
printOut('<tr><td>'+ species +'</td><td>'+ match +'</td><td>'+ status +'</td></tr>')
printOut('</table><br><br>')
   
## PREPARE THE DOWNLOADABLE FILE(S)
#_ = PrepareDownload(paths['serviceRoot']+'dummy_dir/dummy_txt_file.txt')
#_ = PrepareDownload(paths['serviceRoot']+'dummy_dir/dummy_tab_file.tsv')

## PRINT TAB FILE(S) AS RESULT TABLES TO THE HTML OUTPUT FILE
#tsv=paths['downloads']+'dummy_tab_file.tsv'
#if os.path.exists(tsv):
#   printOut('<h2>Dummy Tab-File:</h2>\n')
#   tsv2html(tsv, full_dl=tsv, css_classes='redhead')
#   printOut('<br>\n')

# PRINT THE PROGRAM PARAMETERS TO THE HTML OUTPUT FILE
if len(inputFiles) > 0: printInputFilesHtml(inputFiles)

# PRINT THE CONTIGS INFORMATION TO THE HTML OUTPUT FILE
if args.technology != 'Assembled_Genome': printContigsInfo(runID=runID)

## PRINT FILE DOWNLOAD-BUTTON(S) TO THE HTML OUTPUT FILE
#dlButton('RESULTS', 'dummy_txt_file.txt')

################################################################################
# LOG THE TIMERS (No need to change this part)
progs.PrintTimers() 

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# INDICATE THAT THE WRAPPER HAS FINISHED (No need to change this part)
printDebug("Done")

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE (No need to change this part)
fileZipper(paths['serviceRoot'])
