#!/usr/bin/env python
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Service structure
#--> The input/arguments are validated
#--> Create service folder and subfolders
#--> The uploaded files are copied to the 'Upload' directory 
#--> Log service submission in SQL
#--> Setup and execution of service specific programs
#--> The success of the service is validated
#--> A HTML output page is created if in webmode
#--> Downloadable files are copied/moved to the 'Download' Directory
#--> The SQL service entry is updated
#--> The files are zipped for long-term storage
import sys, os, time, random

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
#from assembly_module   import (PrepareAssembly, MakeAssembly,
#                               printContigsInfo, AddAssemblyToProgList)
from functions_module  import (printDebug, copyFile, program, createServiceDirs,
                               getArguments, paths, makeFileList, fileUnzipper,
                               printInputFilesHtml, fileZipper, PrepareDownload,
                               UpdatePaths, progs, CheckFileType, printOut, 
                               moveFile, setDebug, add2DatabaseLog, dlButton,
                               GraceFullExit, FixPermissions, tsv2html)

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################

################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
setDebug(False)
service, version = "VirusFinder", "1.0"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
args = getArguments('''
file           file         -f  VALUE  -f  ''
radio          method        -m  VALUE  -m  ''
mselectionbox  database  -d  VALUE  -d  ''
''')

# VALIDATE REQUIRED ARGUMENTS
#if args.technology is None: GraceFullExit("Error: No technology platform was chosen!\n")
if args.uploadPath is None or len(args.uploadPath) < 5 or not os.path.exists(args.uploadPath):
   GraceFullExit("Error: No valid upload path was provided! (%s)\n"%(args.uploadPath))
elif args.uploadPath[-1] != '/': args.uploadPath += '/' # Add endslash

# SET RUN DIRECTORY (No need to change this part)
if args.reuseid:
  #/srv/www/secure-upload/isolates/5_16_4_2015_162_299_423438//0/
  runID = [x for x in args.uploadPath.split('/') if x!=''][-2]
else:
  runID = time.strftime('%w_%d_%m_%Y_%H%M%S_')+str(random.randint(100000,999999))
paths.serviceRoot = '{programRoot}IO/%s/'%(runID)
paths.isolateRoot = '{programRoot}'
paths.add_path('uploads', '{serviceRoot}Uploads/')

# SET AND UPDATE PATHS (No need to change this part)
UpdatePaths(service, version, '', '', args.webmode)

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()
stat = paths.Create('uploads')

# LOG SERVICE SUBMISSION IN SQL (No need to change this part)
#add2DatabaseLog(service+'-'+version, runID, args.usr, args.ip, args.technology)
add2DatabaseLog(service+'-'+version, runID, args.usr, args.ip, "not available")

# MOVE UPLOADS FROM APP- TO ISOLATE UPLOAD DIRECTORY (No need to change this part)
if stat:
   # Move the uploaded files to the upload directory
   moveFile(args.uploadPath+'*', paths['uploads'])
   # Unzipping uploaded files if zipped
   fileUnzipper(paths['uploads'])
   # GET INPUT FILES from input path
   inputFiles = makeFileList(paths['uploads'])
else:
   GraceFullExit("Error: Could not create upload directory!\n")

# GET CONTIGS (No need to change this part UNLESS you dont need assembly)
#if args.technology != 'Assembled_Genome':
#   # ADD THE ASSEMBLER to the program list
#   AddAssemblyToProgList()
#   # Prepare the Assembly program for execution
#   PrepareAssembly(args.technology, inputFiles)
#   # Assemble the reads into contigs
#   n50 = MakeAssembly(args.technology)
#   # The success of the assembler is validated
#   status = progs['Assembler'].GetStatus()
#   if status != 'Done' or not isinstance(n50, int):
#      GraceFullExit("Error: Assembly of the inputfile(s) failed!\n"%(len(inputFiles)))
#else:
#   # Validate that only 1 file was submitted
#   if len(inputFiles) != 1:
#      GraceFullExit("Error: Invalid number of contig files (%s)\n"%(len(inputFiles)))
#   # Validate that the uploaded file is fasta
#   if CheckFileType(inputFiles[0]) != 'fasta':
#      GraceFullExit("Error: Invalid contigs format (%s)!\nOnly the fasta format is recognised as a proper contig format.\n"%(CheckFileType(inputFiles[0])))
#   # Add contigsPath
#   paths.add_path('contigs', paths['inputs']+'contigs.fsa')
#   # Copy file to Input directory
#   copyFile(inputFiles[0], paths['contigs'])

# ADDING PROGRAMS
if len(inputFiles)==1:
   if args.method == "phash":
      searchdb = progs.AddProgram(program(
         name='searchdb', path=paths['scripts']+'searchdb_phash.py', timer=0,
         ptype='python', toQueue=True, wait=False, workDir='',
         args=['-f', inputFiles[0],
               '-d', args.database]))
   elif args.method == "bwa":
      searchdb = progs.AddProgram(program(
         name='searchdb', path=paths['scripts']+'searchdb_bwa.py', timer=0,
         ptype='python', toQueue=True, wait=False, workDir='',
         args=['-f', inputFiles[0],
               '-d', args.database]))
elif len(inputFiles) >1:
   if args.method == "phash":
      searchdb = progs.AddProgram(program(
         name='searchdb', path=paths['scripts']+'searchdb_phash.py', timer=0,
         ptype='python', toQueue=True, wait=False, workDir='',
         args=['-f', inputFiles[0],
               '-e', inputFiles[1],
               '-d', args.database]))
   elif args.method == "bwa":
      searchdb = progs.AddProgram(program(
         name='searchdb', path=paths['scripts']+'searchdb_bwa.py', timer=0,
         ptype='python', toQueue=True, wait=False, workDir='',
         args=['-f', inputFiles[0],
               '-e', inputFiles[1],
               '-d', args.database]))

# ADDING MORE ARGUMENTS
#dummy.AppendArgs(['-option', 'content'])

# EXECUTION OF THE PROGRAMS
searchdb.Execute()
searchdb.WaitOn(pattern='DONE!', interval=20, maxloopsleft=2000000)

# THE SUCCESS OF THE SERVICE IS VALIDATED
status = searchdb.GetStatus()
if status != 'Done': GraceFullExit("Error: Execution of the program failed!\n")

# CREATE THE HTML OUTPUT FILE (No need to change this part)
if args.webmode:
   if os.path.exists(paths['outputs']) and not os.path.exists(paths['outputs']+service+".out"):
      with open(paths['outputs']+service+".out", 'w') as f:
         f.write('<h1>%s-%s Server - Results</h1>'%(service, version))

# PRINT THE STANDARD OUTPUT OF THE PROGRAM TO THE HTML OUTPUT FILE
#searchdb.Printstdout()

# PREPARE THE DOWNLOADABLE FILE(S)
#os.system("cp " + paths['outputs']+'searchdb_results.txt ' +paths['downloads']+'searchdb_results.txt')
#_ = PrepareDownload(paths['outputs']+'searchdb_results.txt')
_ = PrepareDownload(paths['serviceRoot']+'outputs/searchdb_results.txt')
#_ = PrepareDownload(paths['serviceRoot']+'dummy_dir/dummy_tab_file.tsv')

# PRINT TAB FILE(S) AS RESULT TABLES TO THE HTML OUTPUT FILE
#tsv=paths['downloads']+'searchdb.out'
tsv=paths['downloads']+'searchdb_results.txt'
if os.path.exists(tsv):
   printOut('<h2>' + service +' '+ version +' results:</h2>\n')
   tsv2html(tsv, full_dl=tsv, css_classes='redhead')
   #tsv2html(tsv, full_dl='', css_classes='grayhead')
   printOut('<br>\n')

# PRINT THE PROGRAM PARAMETERS TO THE HTML OUTPUT FILE
if len(inputFiles) > 0: printInputFilesHtml(inputFiles)

# PRINT THE CONTIGS INFORMATION TO THE HTML OUTPUT FILE
#if args.technology != 'Assembled_Genome': printContigsInfo(runID=runID)

# PRINT FILE DOWNLOAD-BUTTON(S) TO THE HTML OUTPUT FILE
#os.system("cp " + paths['outputs']+'searchdb_results.txt ' +paths['downloads']+'searchdb_results.txt')
dlButton('RESULTS', 'searchdb_results.txt', downloadscript='https://cge.cbs.dtu.dk/cge/download_files2.php')

################################################################################
# LOG THE TIMERS (No need to change this part)
progs.PrintTimers() 

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# INDICATE THAT THE WRAPPER HAS FINISHED (No need to change this part)
printDebug("Done")

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE (No need to change this part)
fileZipper(paths['serviceRoot'])
