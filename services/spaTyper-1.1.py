#!/usr/bin/env python
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Service structure
#--> The input/arguments are validated
#--> Create service folder and subfolders
#--> The uploaded files are copied to the 'Upload' directory 
#--> Log service submission in SQL
#--> Setup and execution of service specific programs
#--> The success of the service is validated
#--> A HTML output page is created if in webmode
#--> Downloadable files are copied/moved to the 'Download' Directory
#--> The SQL service entry is updated
#--> The files are zipped for long-term storage
import sys, os, time, random

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from assembly_module  import (PrepareAssembly, MakeAssembly,
                              printContigsInfo, AddAssemblyToProgList)
from functions_module import (printDebug, copyFile, program, createServiceDirs,
                              getArguments, paths, makeFileList, fileUnzipper,
                              printInputFilesHtml, fileZipper, PrepareDownload,
                              UpdatePaths, progs, CheckFileType, printOut, 
                              moveFile, setDebug, add2DatabaseLog, dlButton,
                              GraceFullExit, FixPermissions, tsv2html)

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################

################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
setDebug(False)
service, version = "spaTyper", "1.1"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
args = getArguments('''
selectionbox   technology   -t  VALUE  -t  ''
''')

# VALIDATE REQUIRED ARGUMENTS
if args.technology == None: GraceFullExit("Error: No technology platform was chosen!\n")

# SET RUN DIRECTORY (No need to change this part)
runID = time.strftime('%w_%d_%m_%Y_%H%M%S_')+str(random.randint(100000,999999))
paths.serviceRoot = '{programRoot}IO/%s/'%(runID)
paths.isolateRoot = '{programRoot}'
paths.add_path('uploads', '{serviceRoot}uploads/')

# SET AND UPDATE PATHS (No need to change this part)
UpdatePaths(service, version, '', '', args.webmode)

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()
stat = paths.Create('uploads')

# LOG SERVICE SUBMISSION IN SQL (No need to change this part)
add2DatabaseLog(service+'-'+version, runID, args.usr, args.ip, args.technology)

# MOVE UPLOADS FROM APP- TO ISOLATE UPLOAD DIRECTORY (No need to change this part)
if stat:
   # Move the uploaded files to the upload directory
   moveFile(args.uploadPath+'/*', paths['uploads'])
   # Unzipping uploaded files if zipped
   fileUnzipper(paths['uploads'])
   # GET INPUT FILES from input path
   inputFiles = makeFileList(paths['uploads'])
else:
   GraceFullExit("Error: Could not create upload directory!\n")

# GET CONTIGS (No need to change this part UNLESS you dont need assembly)
if args.technology != 'Assembled_Genome':
   # ADD THE ASSEMBLER to the program list
   AddAssemblyToProgList()
   # Prepare the Assembly program for execution
   PrepareAssembly(args.technology, inputFiles)
   # Assemble the reads into contigs
   n50 = MakeAssembly(args.technology)
   # The success of the assembler is validated
   status = progs['Assembler'].GetStatus()
   if status != 'Done' or not isinstance(n50, int):
      GraceFullExit("Error: Assembly of the inputfile(s) failed!\n"%(len(inputFiles)))
else:
   # Validate that only 1 file was submitted
   if len(inputFiles) != 1:
      GraceFullExit("Error: Invalid number of contig files (%s)\n"%(len(inputFiles)))
   # Validate that the uploaded file is fasta
   if CheckFileType(inputFiles[0]) != 'fasta':
      GraceFullExit("Error: Invalid contigs format (%s)!\nOnly the fasta format is recognised as a proper contig format.\n"%(CheckFileType(inputFiles[0])))
   # Add contigsPath
   paths.add_path('contigs', paths['inputs']+'contigs.fsa')
   # Copy file to Input directory
   copyFile(inputFiles[0], paths['contigs'])

# ADDING PROGRAMS
spaTyper = progs.AddProgram(program(
   name='spa_type', path=paths['scripts']+'spa_type.find.script.sh', timer=0,
   ptype='bash', toQueue=True, wait=False, workDir='',
   args=[ paths['contigs'], paths['database']+'spa_sequences.fna']))

# EXECUTION OF THE PROGRAMS
spaTyper.Execute()
spaTyper.WaitOn(pattern='Done', interval=15, maxloopsleft=90)

# THE SUCCESS OF THE SERVICE IS VALIDATED
status = spaTyper.GetStatus()
if status != 'Done': GraceFullExit("Error: Execution of the program failed!\n")

# Fetch the results from the spaTyper
results = {
   'spa_type':    'unknown',
   'repeats':     '',
   'contig':      'N/A',
   'position':    '',
   'orientation': 'N/A'
}

if os.path.exists(spaTyper.stdout):
   with open(spaTyper.stdout) as f:
      for l in f:
         if l[:5] == 'Bingo':
            #Bingo	spatype_t011	NODE_259_length_63698_cov_37.610912	position:30629-30834	plus
            #Bingo   spatype_t12333  C00004046_n1_c16        position:15641-15990    minus
            tmp = l.split('\t')
            if len(tmp) == 1: tmp = l.split()
            
            results['spa_type']    = tmp[1].split('_')[1].strip()
            results['contig']      = tmp[2].strip()
            results['position']    = tmp[3].split(':')[1].strip()
            results['orientation'] = tmp[4].strip()
else: printDebug('Error: stdout did not exist: %'%spaTyper.stdout)

# Get the repeats order
if os.path.exists(paths['database']+'spa_types.txt'):
   le = len(results['spa_type'])
   with open(paths['database']+'spa_types.txt') as f:
      for l in f:
         #t011,08-16-02-25-34-24-25
         if l[:le] == results['spa_type']:
            results['repeats'] = l.split(',')[1].strip()
else: printDebug('Error: The file containing spa types did not exist: %'%(paths['database']+'spa_types.txt'))

# CREATE THE HTML OUTPUT FILE (No need to change this part)
if args.webmode:
   if os.path.exists(paths['outputs']) and not os.path.exists(paths['outputs']+service+".out"):
      with open(paths['outputs']+service+".out", 'w') as f:
         f.write('<h1>%s-%s Server - Results</h1>'%(service, version))

# Create TAB FILE for RESULT
tsv=paths['downloads']+'spaType_results.tsv'
with open(tsv, 'w') as f:
   #f.write('#') # Add header indicator to first line
   #f.write('\n'.join(['\t'.join(['-'.join(y) if isinstance(y, list) else str(y) for y in x]) for x in zip(*results.items())]))
   f.write("#spa Typing\n")
   f.write("#spa Type\tRepeats\tContig\tPosition\tOrientation\n")
   f.write("%s\t%s\t%s\t%s\t%s\n"%(results['spa_type'], results['repeats'],
                                   results['contig'], results['position'],
                                   results['orientation']))

# PRINT TAB FILE(S) AS RESULT TABLES TO THE HTML OUTPUT FILE
if os.path.exists(tsv):
   tsv2html(tsv, css_classes='greenhead')
   printOut('<br>\n')
else: printDebug('Error: The result file was not created: %'%tsv)

# PRINT THE PROGRAM PARAMETERS TO THE HTML OUTPUT FILE
if len(inputFiles) > 0: printInputFilesHtml(inputFiles)

# PRINT THE CONTIGS INFORMATION TO THE HTML OUTPUT FILE
if args.technology != 'Assembled_Genome': printContigsInfo(runID=runID)

################################################################################
# LOG THE TIMERS (No need to change this part)
progs.PrintTimers() 

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# INDICATE THAT THE WRAPPER HAS FINISHED (No need to change this part)
printDebug("Done")

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE (No need to change this part)
fileZipper(paths['serviceRoot'])
