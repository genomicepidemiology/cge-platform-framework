#!/usr/bin/env python

from setuptools import setup, find_packages
import os
import re

version = re.search(
    '__version__ = "([^\']+)"',
    open(
        os.path.join(os.path.dirname(__file__), 'modules/__init__.py')
    ).read()).group(1)

setup(
    name="modules",
    version=version,
    author="Your Name Here",
    author_email="youremail@example.com",
    url="https://bitbucket.org/genomicepidemiology/cge-functions-module/src",
    description="This project documents CGE Functions Module",
    long_description='\n\n'.join((
        open(os.path.join(os.path.dirname(__file__), 'README.md')).read(),
        open(os.path.join(os.path.dirname(__file__), 'CHANGES.rst')).read(),
    )),
    entry_points={
        'console_scripts': [
            'bacterialPipeline = workflow.scripts.BAP:bacterialPipeline',
            ]
        },
    packages=find_packages(),
    include_package_data=True,
    install_requires=['MySQL-python', 'geocoder'],
    test_suite="tests",
)
