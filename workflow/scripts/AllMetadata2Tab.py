#!/usr/bin/env python
''' Script to retrieve alle metadata from the CGE MySQL database

'''
import sys, os, MySQLdb, json
from time import time, localtime, strftime, sleep
from datetime import timedelta, date, datetime

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################
def printDebug(x):
   print x

def GetAllMetadata(usr):
   ''' This function queries the database for service results and return them. '''
   #printDebug('\nGet Metadata and Results from DB')
   # CONNECT TO SQL DATABASE
   try:
      conn = MySQLdb.connect(host   = "cge",
                             user   = "cgeclient",
                             passwd = "www",
                             db     = "cge")
   except MySQLdb.Error, e:
         printDebug("No data was retrieved!",
                 "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
   else:
      # GET USER ID
      try:
         cursor = conn.cursor()
         cursor.execute("""SELECT u.id
                           FROM users u
                           WHERE u.usr = %s
                           ;""", (usr,))
         r = cursor.fetchone()
         uid = int(r[0])
         cursor.close()
      except MySQLdb.Error, e:
         printDebug("No data was retrieved!",
                    "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
      except Exception, e:
         printDebug("No data was retrieved!", e)
      else:
         # RETRIEVE DATA
         try:
            cursor = conn.cursor()
            cursor.execute("""
               SELECT i.id, i.sample_name, i.sequencing_platform,
                      i.sequencing_type, i.pre_assembled, i.sample_type,
                      m.organism, m.strain, m.subtype, m.country, m.region,
                      m.city,  m.zip_code, m.longitude, m.latitude,
                      m.location_note, m.isolation_source, m.source_note,
                      m.pathogenic, m.pathogenicity_note, m.collection_date,
                      m.date_uncertainty_flag, m.collected_by, m.notes,
                      s.service, s.results 
               FROM isolates i 
               LEFT OUTER JOIN meta m ON m.id = i.id
               LEFT OUTER JOIN services s ON s.isolate_id = i.id
               WHERE i.owner = %s
                 AND NOT i.removed = 1
               ORDER BY m.collection_date, i.id, s.id DESC
               ;""", (uid,))
            # RETRIEVING RESULTS
            results = cursor.fetchall()
            cursor.close()
         except Exception, e: # MySQLdb.Error
            printDebug("Fetching the results failed!",
                       "MySQLdb Error %d: %s" % (e.args[0], e.args[1]))
   finally:
      # CLOSE CONNECTION
      conn.close()
   
   #printDebug("Results: %s"%(str(results)))
   data = {}
   for i,x in enumerate(results):
      x=list(x)
      if not x[0] in data: # New Isolate
         # Add Isolate entry to data
         x[4] = 'yes' if x[4]==1 else 'no'
         try:
            if x[8] != '':
               x[8] = ';'.join(['%s=%s'%(k,v) for k,v in json.loads(x[8].replace("u'","'").replace("'",'"')).items()])
         except:
            if isinstance(x[8], str):
               x[8] = x[8].strip(', ;')
         x[13] = str(x[13])
         x[14] = str(x[14])
         if x[19] is None: x[19] = ''
         x[20] = '-'.join(str(x[20]).split('-')[:3-int(x.pop(21))]) #.strftime('%Y-%m-%d')
         try:
            if x[22] != '':
               x[22] = ';'.join(['%s=%s'%(k,v.replace(';',',')) for k,v in json.loads(x[22].replace("u'","'").replace("'",'"')).items()])
         except:
            if isinstance(x[22], str):
               x[22] = x[22].strip(', ;')
         data[x[0]] = x[1:23] + ['', '', '', '', '', '', '']
      else: del x[21]
      # Add Service Entry to data
      if x[24] != '':
         s_result = json.loads(x[24].replace("u'","'").replace("\'",'*').replace("'",'"'))
         if 'pMLST' in x[23] and s_result['sequence_type']:
            tmp = "%s[%s]"%(s_result['scheme_name'],s_result['sequence_type'])
            data[x[0]][28] += tmp if data[x[0]][28] == '' else ',%s'%tmp
         elif 'MLST' in x[23]:
            data[x[0]][24] = s_result['sequence_type']
         elif 'ResFinder' in x[23]:
            data[x[0]][26] = ','.join([gene['db_name'] for gene in s_result['db_hits']])
         elif 'VirulenceFinder' in x[23]:
            data[x[0]][27] = ','.join([gene['db_name'] for gene in s_result['db_hits']])
         elif 'KmerFinder' in x[23]:
            data[x[0]][23] = s_result['species']
         elif 'PlasmidFinder' in x[23] and s_result['unschemed']:
            tmp = ','.join([plasmid['plasmid'] for plasmid in s_result['unschemed']])
            data[x[0]][28] += tmp if data[x[0]][28] == '' else ',%s'%tmp
         elif 'PathogenFinder' in x[23]:
            data[x[0]][25] = s_result['probability']
         elif 'Assembler' in x[23]:
            data[x[0]][22] = str(s_result['N50'])
   return data.values()


################################################################################
##########                           MAIN                            ###########
################################################################################
# Set Metadata header
meta_head = ["sample_name", "sequencing_platform", "sequencing_type",
             "pre_assembled", "sample_type", "organism", "strain", "subtype",
             "country", "region", "city", "zip_code", "longitude", "latitude",
             "location_note", "isolation_source", "source_note", "pathogenic",
             "pathogenicity_note", "collection_date", "collected_by", "notes",
             "r_n50", "r_species", "r_mlst", "r_pathogenicity", "r_resistance",
             "r_virulence", "r_plasmids"
             ]

#Get Metadata for Martin
metadata = GetAllMetadata('Martin')

# Create tab-separated file
with open('Salmonella_Metadata_full.txt', 'w') as f:
   # Write header line
   f.write("%s\n"%'\t'.join(meta_head))
   # Write isolate entries
   f.write("%s\n"%'\n'.join(['\t'.join([str(fld) for fld in ent]) for ent in metadata]))
