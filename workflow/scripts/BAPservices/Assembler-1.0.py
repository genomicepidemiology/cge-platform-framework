#!/usr/bin/env python
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Pipeline structure
#--> The input/arguments are validated
#--> Log service submission in SQL (status = 'In progress' )
#--> Retrieve service ID
#--> Create service folder and subfolders
#--> The input files are copied to the 'Input' directory 
#--> Setup and execution of MLST is performed
#--> The success of the service is validated
#--> Downloadable files are copied/moved to the 'Download' Directory and a file
# entry is added in the SQL DB
#--> The SQL service entry is updated (status = 'Done' or 'Error')
#--> The files are zipped for long-term storage
import sys, os, time, random

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from functions_module_2 import (printDebug, copyFile, program, add2DatabaseLog,
                               getArguments, paths, makeFileList, fileUnzipper,
                               printInputFilesHtml, fileZipper, PrepareDownload,
                               AddService2DB, UpdatePaths, CheckFileType,
                               UpdateServiceInDB, printOut, moveFile, setDebug,
                               createServiceDirs, GraceFullExit, FixPermissions, 
                               tsv2html, dlButton, proglist,
                               RetrieveDataAboutIsolates)
from assembly_module_2 import (PrepareAssembly, MakeAssembly,
                               printContigsInfo, AddAssemblyToProgList)

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################

################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
setDebug(False)
service, version = "Assembler", "1.0"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'sequencing_platform',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
args = getArguments([
      ('--options',  'options',  "trim", "write 'trim' for trimming of reads, else write '' (default='trim')")
   ], pipeline=True, allowcmd=True)

# VALIDATE REQUIRED ARGUMENTS

# Retrieve sequencing_platform and files from database
isolateinfo = RetrieveDataAboutIsolates(args.usr, args.token, ['sequencing_platform', 'sequencing_type', 'pre_assembled', 'file_names'], args.iid)
if isolateinfo is not None:
   sequencing_platform = isolateinfo[0][0]
   sequencing_type = isolateinfo[0][1]
   pre_assembled = isolateinfo[0][2]
   inputFiles = isolateinfo[0][3].split(', ')
else: GraceFullExit('Error: Isolate data not found!')

# Convert sequencing information to our sequencing schemas
technologyDict = {
   ('unknown',       'unknown')     : "unknown",
   ('unknown',       'single')      : "unknown",
   ('unknown',       'paired')      : "unknown",
   ('Illumina',      'single')      : "Illumina",
   ('Illumina',      'paired')      : "Paired_End_Reads",
   ('Illumina',      'unknown')     : "unknown",
   ('LS454',         'single')      : "454",
   ('LS454',         'paired')      : "454_Paired_End_Reads",
   ('LS454',         'unknown')     : "unknown",
   ('ABI SOLiD',     'single')      : "Solid",
   ('ABI SOLiD',     'paired')      : "S_Paired_End_Reads",
   ('ABI SOLiD',     'mate-paired') : "S_Mate_Paired_Reads",
   ('ABI SOLiD',     'unknown')     : "unknown",
   ('Ion Torrent',   'single')      : "Ion_Torrent",
   ('Ion Torrent',   'unknown')     : "unknown"
}
try: seq_tech = technologyDict[(sequencing_platform, sequencing_type)]
except: GraceFullExit('Error: The technology was not recognised!')

# Log service submission in SQL and Retrieve service ID
serviceID, ifolder = AddService2DB(args.usr, args.token, args.iid, service, version,
                                   args.ip, 'sequencing_platform=%s_%s'%(sequencing_platform, sequencing_type))

# SET AND UPDATE PATHS
UpdatePaths(service, version, serviceID, ifolder, args.webmode)

# Check whether inputfiles were uploaded
if len(inputFiles) == 0: GraceFullExit("Error: No input files were provided!\n")

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()

# CREATE THE HTML OUTPUT FILE (No need to change this part)
if os.path.exists(paths['outputs']) and not os.path.exists(paths['outputs']+service+".out"):
   with open(paths['outputs']+service+".out", 'w') as f:
      f.write('<h1>%s-%s Server - Results</h1>'%(service, version))

# Check whether seq_tech is known
printDebug(seq_tech)
if seq_tech == 'unknown':
   UpdateServiceInDB(service, serviceID, 'Failure')
   printOut("Input error!<br>\n",
            "The sequencing platform and type specified are not valid input "+
            "to the Assembler.",
            "Please provide a proper sequencing platform and type in your "+
            "metadata, or mark the sample as pre-assembled if you do not need "+
            "assembly.<br>\n")
   GraceFullExit("Error: We cannot assemble sequences from unkown platforms!\n")

# ADD THE ASSEMBLER to the program list
AddAssemblyToProgList()

# Prepare the Assembly program for execution
PrepareAssembly(seq_tech, inputFiles, args.options)

# MAKE THE ASSEMBLY
n50 = MakeAssembly(seq_tech)

# THE SUCCESS OF THE ASSEMBLER IS VALIDATED
status = proglist['Assembler'].GetStatus()
if status == 'Done' and isinstance(n50, int): status = 'Success'
else: status = 'Failure'

if status not in ['Done','Success']:
   UpdateServiceInDB(service, serviceID, status)
   GraceFullExit("Error: Execution of the program failed!\n")

# Sumarise Results
# Todo count total bases in contigs
results = {
   'contig_name': 'contigs.fsa',
   'base_count':  None, # int
   'quality':     None, # float
   'N50':         n50,
   'service_id':  serviceID
}

# Downloadable files are moved to the 'Download' Directory and a file
# entry is added in the SQL DB for each file
fid = PrepareDownload(paths['contigs'], args.iid, serviceID, 'contigs',
                      args.usr, 'Draft genome, aka. contigs. (N50: %s )'%(n50))

# SAVE HTML RECORD OF THE PARAMETERS
if len(inputFiles) > 0: printInputFilesHtml(inputFiles)

# PRINT THE CONTIGS INFORMATION N50
printContigsInfo(fid)

################################################################################
#(No need to change this part)
# UPDATE mySQL SERVICE ENTRY (status = 'Done' or 'Error')
UpdateServiceInDB(service, serviceID, status, results)

# LOG THE TIMERS
proglist.PrintTimers() 

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE
fileZipper(paths['serviceRoot'])

# INDICATE THAT THE WRAPPER HAS FINISHED
sys.stderr.write("Done")
