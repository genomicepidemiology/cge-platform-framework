#!/usr/bin/env python
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Pipeline structure
#--> The input/arguments are validated
#--> Log service submission in SQL (status = 'In progress' )
#--> Retrieve service ID
#--> Create service folder and subfolders
#--> The input files are copied to the 'Input' directory 
#--> Setup and execution of KmerFinder is performed
#--> The success of the service is validated
#--> Downloadable files are copied/moved to the 'Download' Directory and a file
# entry is added in the SQL DB
#--> The SQL service entry is updated (status = 'Done' or 'Error')
#--> The files are zipped for long-term storage
import sys, os, time, random

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from functions_module_2 import (printDebug, copyFile, program, add2DatabaseLog,
                               getArguments, paths, makeFileList, fileUnzipper,
                               printInputFilesHtml, fileZipper, PrepareDownload,
                               AddService2DB, UpdatePaths, CheckFileType,
                               UpdateServiceInDB, printOut, moveFile, setDebug,
                               createServiceDirs, GraceFullExit, FixPermissions, 
                               tsv2html, dlButton, proglist)

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################

################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
setDebug(True)
service, version = "KmerFinder", "1.4"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
args = getArguments('''
file    contigs  -f  VALUE
''', pipeline=True, allowcmd=True)

# VALIDATE REQUIRED ARGUMENTS
if args.contigs == None: GraceFullExit("Error: No Contigs were provided!\n")
elif not os.path.exists(args.contigs):
   GraceFullExit("Error: Contigs file does not exist!\n")
elif not CheckFileType(args.contigs) in ['fasta','fastq']:
   GraceFullExit(("Error: Invalid file format (%s)!\nOnly fasta or fastq format"
                  "can be used.\n")%(CheckFileType(args.contigs)))

# Log service submission in SQL and Retrieve service ID
serviceID, ifolder = AddService2DB(args.usr, args.token, args.iid, service, version,
                                   args.ip, '')

# SET AND UPDATE PATHS
UpdatePaths(service, version, serviceID, ifolder, args.webmode)
paths.add_path('contigs', paths['inputs']+'contigs.fsa')
paths.add_path('taxdb', '/home/data1/services/KmerFinder/tax_database/')

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()

# COPY CONTIGS FILE TO INPUT DIRECTORY
copyFile(args.contigs, paths['contigs'] +'.gz')
# UNZIPPING CONTIGS FILE IF ZIPPED
fileUnzipper(paths['inputs'])

# EXECUTION OF THE PROGRAMS
kmerfinder = program( name='KmerFinder', 
   ptype='python', path=paths['scripts'] +'findtemplate_pipeline.py',
   workDir='', ssh=True, toQueue=True, wait=False, server='cgebase' if args.webmode else 'cgebase2',
   args=['-t', paths['database'] +'bacteria.organisms.ATGAC',
         '-i', paths['contigs'],
         '-x', 'ATGAC',
         '-r','-w',
         '-o', paths['downloads'] +'results.tab'
         ]
   )

addTaxonomy = program(
   name='addTaxonomy', path=paths['scripts'] +'get_tax.py', timer=0,
   ptype='python', toQueue=True, wait=False, workDir='', server='cgebase' if args.webmode else 'cgebase2',
   args=['-i', paths['downloads'] +'results.tab',
         '-b',
         '-c',
         '-t', paths['taxdb'] +'bacteria_ncbi_tax',
         '-o', paths['downloads'] +'results_tax.tab'])

# Execute and wait on KmerFinder
proglist.Add2List(kmerfinder)
kmerfinder.Execute()
kmerfinder.WaitOn(pattern='DONE!', interval=15, maxloopsleft=90)

# Execute and wait on taxonomy annotation
proglist.Add2List(addTaxonomy)
addTaxonomy.Execute()
addTaxonomy.WaitOn(pattern='DONE!', interval=2, maxloopsleft=30)

# THE SUCCESS OF THE PROGRAMS IS VALIDATED
predicted_species = None
closest_match = None
Kmers_template = None
Kmer_hits = None
taxid_species = None
taxid = None
liniage = None
Kmer_coverage = None
status = kmerfinder.GetStatus()
if status == 'Done':
   try:
      result_file = paths['downloads']+'results_tax.tab'
      printDebug('KmerFinder result_file: %s'%result_file)
      with open(result_file) as f:
         # Skip header
         _ = f.readline()
         ##Template	Score	Expected	z	p_value	frac_q	frac_d	coverage	total frac_q	total frac_d	total coverage	Kmers in Template	Description	TAXID	Taxonomy	TAXID Species	Species
         hit = f.readline().split('\t')
         #Escherichia_coli_K_12_substr__MG1655_uid57779	    4619	     188	   322.7	2.8e-23	1.1e-03	4.6e-01	9.4e-01	1.1e-03	4.6e-01	9.4e-01	    9991	gi|556503834|ref|NC_000913.3|	511145	cellular organisms; Bacteria; Proteobacteria; Gammaproteobacteria; Enterobacteriales; Enterobacteriaceae; Escherichia; Escherichia coli; Escherichia coli K-12; Escherichia coli str. K-12 substr. MG1655; 	562	Escherichia coli
         liniage = hit[-3].strip().strip(';')
         predicted_species = hit[-1].strip()
         taxid_species = hit[-2].strip()
         closest_match = liniage.split(';')[-1]
         taxid_match = hit[-4].strip()
         coverage_query = float(hit[8].strip())
         coverage_match = float(hit[9].strip())*2
         Kmer_hits = int(hit[1].strip())*2
         Kmers_template = hit[11].strip()
         Kmer_coverage = float(hit[10])
   except Exception, e:
      printDebug('All or some Kmer Results could not be found!\n%s'%(e))
   else: status = 'Success'
else: status = 'Failure'

if status not in ['Done','Success']:
   UpdateServiceInDB(service, serviceID, status)
   GraceFullExit("Error: Execution of the program failed!\n")

# Sumarise Results
results = {
   'liniage':         liniage,
   'species':         predicted_species,
   'taxid_species':   taxid_species,
   'closest_match':   closest_match,
   'taxid_match':     taxid_match,
   'kmers_hit':       Kmer_hits,
   'kmers_template':  Kmers_template,
   'kmer_coverage':   Kmer_coverage,
   'coverage_query':  coverage_query,
   'coverage_match':  coverage_match,
   'service_id':      serviceID
}
printDebug('KmerFinder results: %s'%results)

# Print Result Table
try:
   tsv=paths['downloads']+'results_tax.tab'
   if os.path.exists(tsv):
      printOut('<h2>' + service + ' ' +  version + ' results:</h2>\n')
      tsv2html(tsv, full_dl=tsv, css_classes='redhead')
      #tsv2html(tsv, full_dl='', css_classes='grayhead')
      printOut('<br>\n')
except Exception, e: printDebug('Error: Output printing failed! %s\n'%(e))


# PRINT THE PROGRAM PARAMETERS TO THE HTML OUTPUT FILE
#if len(inputFiles) > 0: printInputFilesHtml(inputFiles)

# PREPARE THE DOWNLOADABLE FILE(S)
_ = PrepareDownload(paths['downloads']+'results.tab', args.iid,
                      serviceID, 'results', args.usr,
                      'Text version of the KmerFinder results.')


################################################################################
#(No need to change this part)
# UPDATE mySQL SERVICE ENTRY (status = 'Done' or 'Error')
UpdateServiceInDB(service, serviceID, status, results)

# LOG THE TIMERS
proglist.PrintTimers() 

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE
fileZipper(paths['serviceRoot'])

# INDICATE THAT THE WRAPPER HAS FINISHED
sys.stderr.write("Done")
