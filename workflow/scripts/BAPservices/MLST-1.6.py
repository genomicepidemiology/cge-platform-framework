#!/usr/bin/env python
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Pipeline structure
#--> The input/arguments are validated
#--> Log service submission in SQL (status = 'In progress' )
#--> Retrieve service ID
#--> Create service folder and subfolders
#--> The input files are copied to the 'Input' directory 
#--> Setup and execution of MLST is performed
#--> The success of the service is validated
#--> Downloadable files are copied/moved to the 'Download' Directory and a file
# entry is added in the SQL DB
#--> The SQL service entry is updated (status = 'Done' or 'Error')
#--> The files are zipped for long-term storage
import sys, os, time, random

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from functions_module_2 import (printDebug, copyFile, program, add2DatabaseLog,
                               getArguments, paths, makeFileList, fileUnzipper,
                               printInputFilesHtml, fileZipper, PrepareDownload,
                               AddService2DB, UpdatePaths, CheckFileType,
                               UpdateServiceInDB, printOut, moveFile, setDebug,
                               createServiceDirs, GraceFullExit, FixPermissions, 
                               tsv2html, dlButton, proglist)

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################

################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
setDebug(False)
service, version = "MLST", "1.6"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
args = getArguments('''
file           contigs      -f    VALUE
selectionbox   organism     -d    VALUE
''', pipeline=True, allowcmd=True)

# VALIDATE REQUIRED ARGUMENTS
if args.organism == None: GraceFullExit("Error: No MLST Scheme was chosen!\n")
if args.contigs == None: GraceFullExit("Error: No Contigs were provided!\n")
elif not os.path.exists(args.contigs):
   GraceFullExit("Error: Contigs file does not exist!\n")
elif CheckFileType(args.contigs) != 'fasta':
   GraceFullExit('''Error: Invalid contigs format (%s)!\nOnly the fasta format
                 is recognised as a proper contig format.\n'''%(
                  CheckFileType(args.contigs)))

# Log service submission in SQL and Retrieve service ID
serviceID, ifolder = AddService2DB(args.usr, args.token, args.iid, service, version,
                                   args.ip, 'scheme='+args.organism)

# SET AND UPDATE PATHS
UpdatePaths(service, version, serviceID, ifolder, args.webmode)
paths.add_path('contigs', paths['inputs']+'contigs.fsa')

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()

# COPY CONTIGS FILE TO INPUT DIRECTORY
copyFile(args.contigs, paths['contigs'] +'.gz')
# UNZIPPING CONTIGS FILE IF ZIPPED
fileUnzipper(paths['inputs'])

# EXECUTION OF THE PROGRAMS
mlst = program( name='MLST', 
   ptype='perl', path='/home/data1/services/MLST/MLST-1.6/scripts/MLST-1.6_pipeline.pl',
   workDir='', ssh=True, toQueue=True, wait=False, server='cgebase' if args.webmode else 'cgebase2',
   args=['-d', args.organism,
         '-i', paths['contigs'],
         '-R', paths['serviceRoot']
         ]
   )
mlst.Execute()
proglist.Add2List(mlst)
mlst.WaitOn(pattern='Done', interval=50, maxloopsleft=48)

# THE SUCCESS OF THE PROGRAMS ARE VALIDATED
status = mlst.GetStatus()
if status == 'Done' and os.path.exists(paths['downloads']+'results.txt'): status = 'Success'
else: status = 'Failure'

if status not in ['Done','Success']:
   UpdateServiceInDB(service, serviceID, status)
   GraceFullExit("Error: Execution of the program failed!\n")

# SUMARISE RESULTS
results = {
   'scheme_name':    '',
   'sequence_type':  '',
   'service_id':     serviceID
}
if status == 'Success':
   with open(paths['downloads']+'results.txt', 'r') as f:
      for l in f:
         d = [ld.strip() for ld in l.split(':')]
         if d[0] == 'Sequence Type': results['sequence_type'] = d[1]
         if d[0] == 'MLST Profile': results['scheme_name'] = d[1]

# CREATE THE HTML OUTPUT FILE (No need to change this part)
if os.path.exists(paths['outputs']) and not os.path.exists(paths['outputs']+service+".out"):
   with open(paths['outputs']+service+".out", 'w') as f:
      pass # only create file, MLST does the rest
      #f.write('<h1>%s-%s Server - Results</h1>'%(service, version))

# PREPARE THE DOWNLOADABLE FILE(S)
_ = PrepareDownload(paths['downloads']+'results.txt', args.iid,
                      serviceID, 'results', args.usr,
                      'Text version of the MLST results.')
_ = PrepareDownload(paths['downloads']+'Hit_in_genome_seq.fsa', args.iid,
                      serviceID, 'hitlist', args.usr,
                      'Fasta file containing all the hits.')
_ = PrepareDownload(paths['downloads']+'MLST_allele_seq.fsa', args.iid,
                      serviceID, 'allelelist', args.usr,
                      'Fasta file containing all the MLST allele sequences.')

################################################################################
#(No need to change this part)
# UPDATE mySQL SERVICE ENTRY (status = 'Done' or 'Error')
UpdateServiceInDB(service, serviceID, status, results)

# LOG THE TIMERS
proglist.PrintTimers() 

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE
fileZipper(paths['serviceRoot'])

# INDICATE THAT THE WRAPPER HAS FINISHED
sys.stderr.write("Done")
