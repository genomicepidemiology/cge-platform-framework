#!/usr/bin/env python
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Pipeline structure
#--> The input/arguments are validated
#--> Log service submission in SQL (status = 'In progress' )
#--> Retrieve service ID
#--> Create service folder and subfolders
#--> The input files are copied to the 'Input' directory 
#--> Setup and execution of PathogenFinder is performed
#--> The success of the service is validated
#--> Downloadable files are copied/moved to the 'Download' Directory and a file
# entry is added in the SQL DB
#--> The SQL service entry is updated (status = 'Done' or 'Error')
#--> The files are zipped for long-term storage
import sys, os, time, random

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from functions_module_2 import (printDebug, copyFile, program, add2DatabaseLog,
                               getArguments, paths, makeFileList, fileUnzipper,
                               printInputFilesHtml, fileZipper, PrepareDownload,
                               AddService2DB, UpdatePaths, CheckFileType,
                               UpdateServiceInDB, printOut, moveFile, setDebug,
                               createServiceDirs, GraceFullExit, FixPermissions, 
                               tsv2html, dlButton, proglist)

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################

################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
setDebug(False)
service, version = "PathogenFinder", "1.1"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
args = getArguments('''
selectionbox taxModel   -m VALUE
file    contigs  -f  VALUE
''', pipeline=True, allowcmd=True)

# VALIDATE REQUIRED ARGUMENTS
if args.taxModel == None: GraceFullExit("Error: No phylum class or organism was chosen!\n")
if args.contigs == None: GraceFullExit("Error: No Contigs were provided!\n")
elif not os.path.exists(args.contigs):
   GraceFullExit("Error: Contigs file does not exist!\n")
elif CheckFileType(args.contigs) != 'fasta':
   GraceFullExit('''Error: Invalid contigs format (%s)!\nOnly the fasta format
                 is recognised as a proper contig format.\n'''%(
                  CheckFileType(args.contigs)))

# Log service submission in SQL and Retrieve service ID
serviceID, ifolder = AddService2DB(args.usr, args.token, args.iid, service, version,
                                   args.ip, '')

# SET AND UPDATE PATHS
UpdatePaths(service, version, serviceID, ifolder, args.webmode)
paths.add_path('contigs', paths['inputs']+'contigs.fsa')

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()


# COPY CONTIGS FILE TO INPUT DIRECTORY
copyFile(args.contigs, paths['contigs'] +'.gz')
# UNZIPPING CONTIGS FILE IF ZIPPED
fileUnzipper(paths['inputs'])

# EXECUTION OF THE PROGRAMS
pathogenfinder = program( name='PathogenFinder', 
   ptype='python', path='/home/data1/services/PathogenFinder/PathogenFinder-1.1/scripts/PathogenFinder-1.1_pipeline.py',
   workDir='', ssh=True, toQueue=True, wait=False, server='cgebase' if args.webmode else 'cgebase2',
   args=['-m', args.taxModel,
         '-i', paths['contigs'],
         '-R', paths['serviceRoot']
         ]
   )
pathogenfinder.Execute()
proglist.Add2List(pathogenfinder)
pathogenfinder.WaitOn(pattern='DONE!', interval=60, maxloopsleft=60)

# THE SUCCESS OF THE PROGRAMS ARE VALIDATED
status = pathogenfinder.GetStatus()
if status == 'Done' and os.path.exists(paths['downloads']+'PathogenFinder_results.txt'): status = 'Success'
else: status = 'Failure'

if status not in ['Done','Success']:
   UpdateServiceInDB(service, serviceID, status)
   copyFile(paths['outputs']+service+".out", paths['outputs']+service+"stdout.txt")
   copyFile(paths['outputs']+"output.html", paths['outputs']+service+".out")
   GraceFullExit("Error: Execution of the program failed!\n")

# SUMARISE RESULTS
results = {
   'probability':    None,
   'service_id':     serviceID
}
if status == 'Success':
   with open(paths['downloads']+'PathogenFinder_results.txt', 'r') as f:
      for l in f:
         d = [ld.strip() for ld in l.split('::')]
         if d[0] == 'Probability of being human pathogen': results['probability'] = d[1]

# CREATE THE HTML OUTPUT FILE (No need to change this part)
if (         os.path.exists(paths['outputs'])
     and not os.path.exists(paths['outputs']+service+".out")
   ):
   with open(paths['outputs']+service+".out", 'w') as f:
      pass
      #f.write('<h1>%s-%s Server - Results</h1>'%(service, version))

# PRINT THE STANDARD OUTPUT OF THE PROGRAM TO THE HTML OUTPUT FILE
#pathogenfinder.Printstdout()

# PRINT THE PROGRAM PARAMETERS TO THE HTML OUTPUT FILE
#if len(inputFiles) > 0: printInputFilesHtml(inputFiles)

# PREPARE THE DOWNLOADABLE FILE(S)
_ = PrepareDownload(paths['downloads']+'PathogenFinder_cdhit_raw.txt', args.iid,
                      serviceID, 'raw_matches', args.usr,
                      'Raw matches.')
_ = PrepareDownload(paths['downloads']+'PathogenFinder_results.txt', args.iid,
                      serviceID, 'results', args.usr,
                      'Text file with results.')


## Move stdout from .out to .stdout.txt file, copy output.html to .out file

copyFile(paths['outputs']+service+".out", paths['outputs']+service+"stdout.txt")
copyFile(paths['outputs']+"output.html", paths['outputs']+service+".out")

################################################################################
#(No need to change this part)
# UPDATE mySQL SERVICE ENTRY (status = 'Done' or 'Error')
UpdateServiceInDB(service, serviceID, status, results)

# LOG THE TIMERS
proglist.PrintTimers()

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE
fileZipper(paths['serviceRoot'])

# INDICATE THAT THE WRAPPER HAS FINISHED
sys.stderr.write("Done")
