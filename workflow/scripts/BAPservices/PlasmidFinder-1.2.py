#!/usr/bin/env python
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Pipeline structure
#--> The input/arguments are validated
#--> Log service submission in SQL (status = 'In progress' )
#--> Retrieve service ID
#--> Create service folder and subfolders
#--> The input files are copied to the 'Input' directory 
#--> Setup and execution of PlasmidFinder is performed
#--> The success of the service is validated
#--> Downloadable files are copied/moved to the 'Download' Directory and a file
# entry is added in the SQL DB
#--> The SQL service entry is updated (status = 'Done' or 'Error')
#--> The files are zipped for long-term storage
import sys, os, time, random

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from functions_module_2 import (printDebug, copyFile, program, add2DatabaseLog,
                               getArguments, paths, makeFileList, fileUnzipper,
                               printInputFilesHtml, fileZipper, PrepareDownload,
                               AddService2DB, UpdatePaths, CheckFileType,
                               UpdateServiceInDB, printOut, moveFile, setDebug,
                               createServiceDirs, GraceFullExit, FixPermissions, 
                               tsv2html, dlButton, proglist)

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################

################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
setDebug(False)
service, version = "PlasmidFinder", "1.2"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
args = getArguments('''
selectionbox    threshold   -T  VALUE
selectionbox    database    -d  VALUE
file    contigs  -f  VALUE
''', pipeline=True, allowcmd=True)

# VALIDATE REQUIRED ARGUMENTS
if args.threshold == None: GraceFullExit("Error: No threshold was chosen!\n")
if args.database == None: GraceFullExit("Error: No database was chosen!\n")
if args.contigs == None: GraceFullExit("Error: No Contigs were provided!\n")
elif not os.path.exists(args.contigs):
   GraceFullExit("Error: Contigs file does not exist!\n")
elif CheckFileType(args.contigs) != 'fasta':
   GraceFullExit('''Error: Invalid contigs format (%s)!\nOnly the fasta format
                 is recognised as a proper contig format.\n'''%(
                  CheckFileType(args.contigs)))

# Log service submission in SQL and Retrieve service ID
serviceID, ifolder = AddService2DB(args.usr, args.token, args.iid, service, version,
                                   args.ip, '')

# SET AND UPDATE PATHS
UpdatePaths(service, version, serviceID, ifolder, args.webmode)
paths.add_path('contigs', paths['inputs']+'contigs.fsa')

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()


# COPY CONTIGS FILE TO INPUT DIRECTORY
copyFile(args.contigs, paths['contigs'] +'.gz')
# UNZIPPING CONTIGS FILE IF ZIPPED
fileUnzipper(paths['inputs'])

# EXECUTION OF THE PROGRAMS
plasmidfinder = program( name='PlasmidFinder', 
   ptype='perl', path='/home/data1/services/PlasmidFinder/PlasmidFinder-1.2/scripts/PlasmidFinder-1.2_pipeline.pl',
   workDir='', ssh=True, toQueue=True, wait=False, server='cgebase' if args.webmode else 'cgebase2',
   args=['-d', args.database,
         '-k', args.threshold,
         '-i', paths['contigs'],
         '-R', paths['serviceRoot']
         ]
   )
plasmidfinder.Execute()
proglist.Add2List(plasmidfinder)
plasmidfinder.WaitOn(pattern='DONE!', interval=10, maxloopsleft=60)

# THE SUCCESS OF THE PROGRAMS ARE VALIDATED
status = plasmidfinder.GetStatus()
if status == 'Done' and os.path.exists(paths['downloads']+'results_tab.txt'): status = 'Success'
else: status = 'Failure'

if status not in ['Done','Success']:
   UpdateServiceInDB(service, serviceID, status)
   GraceFullExit("Error: Execution of the program failed!\n")

# GET RESULTS
isodir = '/'.join(os.getcwd().split('/')[:-2])+'/'
schemed = {}
unschemed = {}
pMLST_result = []
if status == 'Success':
   # FIND PLASMIDS AND CHECK FOR KNOWN PMLST SCHEMES SUBJECTS
   schemes = { 'IncF':     ['FIA', 'FIB', 'FII', 'FIC'],
               'IncHI1':   ['HI1A', 'HI1B'],
               'IncHI2':   ['HI2', 'HI2A'],
               'IncI1':    ['I1'],
               'IncN':     ['N', 'N2', 'N3']
   }
   phead = [False]*len(schemes)
   with open(paths['downloads']+'results_tab.txt', 'r') as f:
      #f = '''
      #Plasmid	Identity	Query/HSP	Contig	Position in contig	Note	Accession no.
      #FIB(AP001918)	98.39	682/682	NODE_192_length_3637_cov_14.801209	1877..2558		AP001918
      #FII	95.67	261/254	NODE_474_length_521_cov_31.654510	312..565		AY458016
      #FIA	99.74	388/384	NODE_102_length_9773_cov_14.911490	3387..3770		AP001918
      #HI1A(CIT)	99.74	388/384	NODE_102_length_9773_cov_14.911490	3387..3770		AP001918
      #Col156	99.74	388/384	NODE_102_length_9773_cov_14.911490	3387..3770		AP001918
      #'''
      for l in f:
         d=l.strip().split()
         if len(d) > 3:
            if d[0] == 'Plasmid': continue # skip header
            inscheme = [d[0].split('(')[0].replace('Inc','') in scheme for k, scheme in sorted(schemes.items())] # .split('_')[0]
            # compute quality ID * overlap fraction
            identity =  float(d[1])/100
            overlap  =  [int(x) for x in d[2].split('/')]
            quality  =  round( identity * overlap[0] / overlap[1], 4 )
            if any(inscheme):
               for i in xrange(len(phead)):
                  phead[i] = True if inscheme[i] else phead[i]
               if d[0] in schemed and quality < schemed[d[0]]['quality']: continue # Skip 
               schemed[d[0]] = {
                  'plasmid':   d[0],
                  'identity':  identity,
                  'overlap':   overlap,
                  'quality':   quality
               }
            else:
               if d[0] in unschemed and quality < unschemed[d[0]]['quality']: continue # Skip 
               unschemed[d[0]] = {
                  'plasmid':   d[0],
                  'identity':  identity,
                  'overlap':   overlap,
                  'quality':   quality
               }
   
   # RUN PMLST
   for i, scheme in enumerate(sorted(schemes.keys())):
      if phead[i]:
         printDebug("Run pMLST %s\n"%scheme)
         pmlst = program( name='pMLST_'+scheme, 
            ptype=None, path='/home/data1/services/CGEpipeline/CGEpipeline-1.0/pMLST-1.3.py',
            workDir=isodir+'services/', ssh=False, toQueue=False, wait=False,
            args=['--iid',   args.iid,
                  '--ip',    args.ip,
                  '--usr',   args.usr,
                  '--token', args.token,
                  '-f', args.contigs,
                  '-d', scheme.lower(),
                  ]
            )
         if args.webmode: pmlst.AppendArgs('-w')
         # Update to correct output paths and execute
         pmlst.stderr = isodir+'logs/'+pmlst.stderr.split('/')[-1]
         pmlst.stdout = isodir+'logs/'+pmlst.stdout.split('/')[-1]
         pmlst.Execute()
         proglist.Add2List(pmlst)
   
   # WAIT ON PMLST SERVICES TO FINISH
   printDebug('\nWait on all pMLST services to finish...')
   for progname in proglist.list: proglist[progname].WaitOn(pattern='Done', interval=15, maxloopsleft=40, ePattern='Error')
   
   printDebug('\nExtracting pMLST scheme results...')
   for i, scheme in enumerate(sorted(schemes.keys())):
      if phead[i]:
         printDebug('\n   - Found %s'%scheme)
         ST, quality = proglist['pMLST_'+scheme].FindStdoutVar(['sequence_type', 'quality'])
         pMLST_result.append({
            'scheme_name':    scheme,
            'sequence_type':  ST,
            'quality':        quality
            #'plasmidnames':   [str, str]
         })

# SUMARISE RESULTS
results = {
   'schemed':     list(schemed.values()),
   'unschemed':   list(unschemed.values()),
   'pMLST':       pMLST_result,
   'service_id':  serviceID
}



#
#"/home/data1/services/pMLST/pMLST-1.3/IO/2_3_11_2014_1422_586_117043/downloads/standard_output.txt.gz"
#txt = '''
#pMLST Results
#
#Sequence Type: Unknown ST
#pMLST Profile: IncF
#
#********************************************************************************
#   GENE       % IDENTITY    HSP Length    Allele Length   GAPS     BEST MATCH   
#********************************************************************************
#   FII             -            -               -           -           FII_0
#   FIA          100.00         329             329          0           FIA_6
#   FIB           99.20         373             373          0           FIB_6
#================================================================================
#
#'''
#flag = 0
#FAB = ['F','A','B']
#for l in txt.split('\n'):
#   d = [ld.strip() for ld in l.split(':')]
#   #if d[0] == 'pMLST Profile': results['clustered']['scheme_name'] = d[1]
#   if l == '='*80: flag = 0; break
#   if flag == 2:
#      d = l.strip().split()
#      match = d[5]
#      idx = 'IAB'.find(match[2])
#      if idx >= 0: FAB[idx] += match[-1] if int(match[-1]) > 0 else '-'
#   if l == '*'*80: flag += 1
#
#print '[%s]'%(':'.join(FAB))
#
#
#
#
#
#
## THE SUCCESS OF THE PROGRAMS ARE VALIDATED
#status = plasmidfinder.GetStatus()
#if status == 'Done' and os.path.exists(paths['downloads']+'results_tab.txt'): status = 'Success'
#else: status = 'Failure'
#
#if status not in ['Done','Success']:
#   UpdateServiceInDB(service, serviceID, status)
#   GraceFullExit("Error: Execution of the program failed!\n")
#
## SUMARISE RESULTS
#db_hits = {}
#if status == 'Success':
#   with open(paths['downloads']+'results_tab.txt', 'r') as f:
#      for l in f:
#         d=l.split('\t')
#         if d[0] == 'Virulence factor': continue # skip header
#         if len(d) > 5:
#            # compute quality ID * overlap fraction
#            identity =  float(d[1])/100
#            overlap  =  [int(x) for x in d[2].split('/')]
#            quality  =  round( identity * overlap[0] / overlap[1], 4 )
#            if d[0] in db_hits and quality < db_hits[d[0]]['quality']: continue # Skip     
#            db_hits[d[0]] = {'gene':      d[0],
#                             'identity':  identity,
#                             'overlap':   overlap,
#                             'quality':   quality,
#                             'db_name':   d[5].strip()}
#                             #'db_name':   d[5].split('resistance')[0].strip()}
#   



# CREATE THE HTML OUTPUT FILE (No need to change this part)
if os.path.exists(paths['outputs']) and not os.path.exists(paths['outputs']+service+".out"):
   with open(paths['outputs']+service+".out", 'w') as f:
      pass
      #f.write('<h1>%s-%s Server - Results</h1>'%(service, version))

# PRINT THE STANDARD OUTPUT OF THE PROGRAM TO THE HTML OUTPUT FILE
#plasmidfinder.Printstdout()

# PRINT THE PROGRAM PARAMETERS TO THE HTML OUTPUT FILE
#if len(inputFiles) > 0: printInputFilesHtml(inputFiles)

# PREPARE THE DOWNLOADABLE FILE(S)
_ = PrepareDownload(paths['downloads']+'results.txt', args.iid,
                      serviceID, 'results', args.usr,
                      'Text version of the PlasmidFinder results.')
_ = PrepareDownload(paths['downloads']+'Hit_in_genome_seq.fsa', args.iid,
                      serviceID, 'hitlist', args.usr,
                      'Fasta file containing all the hits.')
_ = PrepareDownload(paths['downloads']+'Plasmid_seq.fsa', args.iid,
                      serviceID, 'Plasmid_seq', args.usr,
                      'Fasta file containing all the plasmid sequences.')
_ = PrepareDownload(paths['downloads']+'results_tab.txt', args.iid,
                      serviceID, 'results_tab', args.usr,
                      'Tab separated text version of the PlasmidFinder results.')


################################################################################
#(No need to change this part)
# UPDATE mySQL SERVICE ENTRY (status = 'Done' or 'Error')
UpdateServiceInDB(service, serviceID, status, results)

# LOG THE TIMERS
proglist.PrintTimers() 

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE
fileZipper(paths['serviceRoot'])

# INDICATE THAT THE WRAPPER HAS FINISHED
sys.stderr.write("Done")
