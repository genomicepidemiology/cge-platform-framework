#!/usr/bin/env python
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Pipeline structure
#--> The input/arguments are validated
#--> Log service submission in SQL (status = 'In progress' )
#--> Retrieve service ID
#--> Create service folder and subfolders
#--> The input files are copied to the 'Input' directory 
#--> Setup and execution of MLST is performed
#--> The success of the service is validated
#--> Downloadable files are copied/moved to the 'Download' Directory and a file
# entry is added in the SQL DB
#--> The SQL service entry is updated (status = 'Done' or 'Error')
#--> The files are zipped for long-term storage
import sys, os, time, random

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from functions_module_2 import (printDebug, copyFile, program, add2DatabaseLog,
                               getArguments, paths, makeFileList, fileUnzipper,
                               printInputFilesHtml, fileZipper, PrepareDownload,
                               AddService2DB, UpdatePaths, CheckFileType,
                               UpdateServiceInDB, printOut, moveFile, setDebug,
                               createServiceDirs, GraceFullExit, FixPermissions, 
                               tsv2html, dlButton, proglist)

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################

################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
setDebug(True)
service, version = "ResFinder", "2.1"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
args = getArguments('''
file           contigs      -f    VALUE
mselectionbox  database     -d    VALUE
selectionbox   threshold    -T    VALUE
text           minlength    -L    VALUE
''', pipeline=True, allowcmd=True)

# VALIDATE REQUIRED ARGUMENTS
if args.database == '': GraceFullExit("Error: No antimicrobial database was "
                                      "chosen!\n")
if args.contigs == None: GraceFullExit("Error: No Contigs were provided!\n")
elif not os.path.exists(args.contigs):
   GraceFullExit("Error: Contigs file does not exist!\n")
elif CheckFileType(args.contigs) != 'fasta':
   GraceFullExit('''Error: Invalid contigs format (%s)!\nOnly the fasta format
                 is recognised as a proper contig format.\n'''%(
                  CheckFileType(args.contigs)))

# Log service submission in SQL and Retrieve service ID
serviceID, ifolder = AddService2DB(args.usr, args.token, args.iid, service, version,
                                   args.ip, "database='%s'||threshold='%s'"%(args.database, args.threshold))

# SET AND UPDATE PATHS
UpdatePaths(service, version, serviceID, ifolder, args.webmode)
paths.add_path('contigs', paths['inputs']+'contigs.fsa')

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()

# COPY CONTIGS FILE TO INPUT DIRECTORY
copyFile(args.contigs, paths['contigs'] +'.gz')
# UNZIPPING CONTIGS FILE IF ZIPPED
fileUnzipper(paths['inputs'])

# EXECUTION OF THE PROGRAMS
resfinder = program( name='ResFinder', 
   ptype='perl', path='/home/data1/services/ResFinder/ResFinder-2.1/scripts/ResFinder-2.1_pipeline.pl',
   workDir='', ssh=True, toQueue=True, wait=False, server='cgebase' if args.webmode else 'cgebase2',
   args=['-d', args.database,
         '-k', args.threshold,
         '-l', args.minlength,
         '-i', paths['contigs'],
         '-R', paths['serviceRoot']
         ]
   )
resfinder.Execute()
proglist.Add2List(resfinder)
resfinder.WaitOn(pattern='DONE!', interval=25, maxloopsleft=24)

# THE SUCCESS OF THE PROGRAMS ARE VALIDATED
status = resfinder.GetStatus()
if status == 'Done' and os.path.exists(paths['downloads']+'results_tab.txt'): status = 'Success'
else: status = 'Failure'

if status not in ['Done','Success']:
   UpdateServiceInDB(service, serviceID, status)
   GraceFullExit("Error: Execution of the program failed!\n")

# SUMARISE RESULTS
db_hits = {}
if status == 'Success':
   with open(paths['downloads']+'results_tab.txt', 'r') as f:
      for l in f:
         d=l.split('\t')
         if d[0] == 'Resistance gene': continue # skip header
         if len(d) > 5:
            # compute quality ID * overlap fraction
            identity =  float(d[1])/100
            overlap  =  [int(x) for x in d[2].split('/')]
            quality  =  round( identity * overlap[0] / overlap[1], 4 )
            if d[5] in db_hits and quality < db_hits[d[5]]['quality']: continue # Skip     
            db_hits[d[5]] = {'gene':      d[0],
                             'identity':  identity,
                             'overlap':   overlap,
                             'quality':   quality,
                             'db_name':   d[5].split('resistance')[0].strip()}
   
results = {
   'db_hits': db_hits.values(),
   'service_id':  serviceID
}

# CREATE THE HTML OUTPUT FILE (No need to change this part)
if os.path.exists(paths['outputs']) and not os.path.exists(paths['outputs']+service+".out"):
   with open(paths['outputs']+service+".out", 'w') as f:
      pass # only create file, MLST does the rest
      #f.write('<h1>%s-%s Server - Results</h1>'%(service, version))

# PRINT THE STANDARD OUTPUT OF THE PROGRAM TO THE HTML OUTPUT FILE
#mlst.Printstdout() MLST does it by default

# PRINT THE PROGRAM PARAMETERS TO THE HTML OUTPUT FILE
#if len(inputFiles) > 0: printInputFilesHtml(inputFiles)

# PREPARE THE DOWNLOADABLE FILE(S)
_ = PrepareDownload(paths['downloads']+'results.txt', args.iid,
                      serviceID, 'results', args.usr,
                      'Text version of the ResFinder results.')
_ = PrepareDownload(paths['downloads']+'results_tab.txt', args.iid,
                      serviceID, 'results_tab', args.usr,
                      'Tab separated version of the ResFinder results.')
_ = PrepareDownload(paths['downloads']+'Hit_in_genome_seq.fsa', args.iid,
                      serviceID, 'hitlist', args.usr,
                      'Fasta file containing all the hits.')
_ = PrepareDownload(paths['downloads']+'Resistance_gene_seq.fsa', args.iid,
                      serviceID, 'sequencelist', args.usr,
                      'Fasta file containing all the Resistance gene sequences.')

################################################################################
#(No need to change this part)
# UPDATE mySQL SERVICE ENTRY (status = 'Done' or 'Error')
UpdateServiceInDB(service, serviceID, status, results)

# LOG THE TIMERS
proglist.PrintTimers() 

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE
fileZipper(paths['serviceRoot'])

# INDICATE THAT THE WRAPPER HAS FINISHED
sys.stderr.write("Done")
