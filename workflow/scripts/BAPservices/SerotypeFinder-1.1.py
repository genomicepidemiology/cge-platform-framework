#!/usr/bin/env python
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Pipeline structure
#--> The input/arguments are validated
#--> Log service submission in SQL (status = 'In progress' )
#--> Retrieve service ID
#--> Create service folder and subfolders
#--> The input files are copied to the 'Input' directory 
#--> Setup and execution of MLST is performed
#--> The success of the service is validated
#--> Downloadable files are copied/moved to the 'Download' Directory and a file
# entry is added in the SQL DB
#--> The SQL service entry is updated (status = 'Done' or 'Error')
#--> The files are zipped for long-term storage
import sys, os, time, random

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from functions_module_2 import (printDebug, copyFile, program, add2DatabaseLog,
                               getArguments, paths, makeFileList, fileUnzipper,
                               printInputFilesHtml, fileZipper, PrepareDownload,
                               AddService2DB, UpdatePaths, CheckFileType,
                               UpdateServiceInDB, printOut, moveFile, setDebug,
                               createServiceDirs, GraceFullExit, FixPermissions, 
                               tsv2html, dlButton, proglist)

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################

################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
setDebug(True)
service, version = "SerotypeFinder", "1.1"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
args = getArguments('''
file           contigs      -f  VALUE
selectionbox   database	    -d  VALUE
selectionbox   threshold    -T  VALUE
selectionbox   minlength    -L  VALUE
''', pipeline=True, allowcmd=True)

# VALIDATE REQUIRED ARGUMENTS
if args.database == None: GraceFullExit("Error: No database was chosen!\n")
if args.threshold == None: GraceFullExit("Error: No threshold was chosen!\n")
if args.minlength == None: GraceFullExit("Error: No minlength was chosen!\n")
if args.contigs == None: GraceFullExit("Error: No Contigs were provided!\n")
elif not os.path.exists(args.contigs):
   GraceFullExit("Error: Contigs file does not exist!\n")
elif CheckFileType(args.contigs) != 'fasta':
   GraceFullExit('''Error: Invalid contigs format (%s)!\nOnly the fasta format
                 is recognised as a proper contig format.\n'''%(
                  CheckFileType(args.contigs)))

# Log service submission in SQL and Retrieve service ID
serviceID, ifolder = AddService2DB(args.usr, args.token, args.iid, service, version, args.ip,
                                   "database='%s'||threshold='%s||minlength=%s'"%(args.database, args.threshold, args.minlength))

# SET AND UPDATE PATHS
UpdatePaths(service, version, serviceID, ifolder, args.webmode)
paths.add_path('contigs', paths['inputs']+'contigs.fsa')

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()

# COPY CONTIGS FILE TO INPUT DIRECTORY
copyFile(args.contigs, paths['contigs'] +'.gz')
# UNZIPPING CONTIGS FILE IF ZIPPED
fileUnzipper(paths['inputs'])

# EXECUTION OF THE PROGRAMS
serotypefinder = program(
   name='serotypefinder', path=paths['scripts']+'SerotypeFinder-1.1_pipeline.pl', timer=0,
   ptype='perl', toQueue=True, wait=False, workDir='',
   server='cgebase' if args.webmode else 'cgebase2',
   args=['-i', paths['contigs'],
         '-R', paths['serviceRoot'],
         '-d', args.database,
         '-k', args.threshold,
         '-l', args.minlength])

serotypefinder.Execute()
proglist.Add2List(serotypefinder)
serotypefinder.WaitOn(pattern='Done', interval=30, maxloopsleft=20)

# THE SUCCESS OF THE PROGRAMS ARE VALIDATED
status = serotypefinder.GetStatus()
if status == 'Done' and os.path.exists(serotypefinder.stdout): status = 'Success'
else: status = 'Failure'

if status not in ['Done','Success']:
   UpdateServiceInDB(service, serviceID, status)
   GraceFullExit("Error: Execution of the program failed!\n")

# SUMARISE RESULTS
results = {
   'serotype':    '',
   'service_id':  serviceID
}
if status == 'Success':
   # Find var1 and var2 in the stdout by looking for following lines:
   # '#var1=value\n' and '#var2=value\n'
   results['serotype'] = serotypefinder.FindStdoutVar(['serotype'])[0]

# CREATE THE HTML OUTPUT FILE (No need to change this part)
if args.webmode:
   if os.path.exists(paths['outputs']) and not os.path.exists(paths['outputs']+service+".out"):
      with open(paths['outputs']+service+".out", 'w') as f:
         f.write('<h1>%s-%s Server - Results</h1>'%(service, version))

# PRINT THE STANDARD OUTPUT OF THE PROGRAM TO THE HTML OUTPUT FILE
serotypefinder.Printstdout()

# PREPARE THE DOWNLOADABLE FILE(S)
#_ = PrepareDownload(paths['outputs']+service+".out", args.iid,
#                      serviceID, 'results', args.usr,
#                      'The output of the service.')

################################################################################
#(No need to change this part)
# UPDATE mySQL SERVICE ENTRY (status = 'Done' or 'Error')
UpdateServiceInDB(service, serviceID, status, results)

# LOG THE TIMERS
proglist.PrintTimers() 

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE
fileZipper(paths['serviceRoot'])

# INDICATE THAT THE WRAPPER HAS FINISHED
sys.stderr.write("Done")
