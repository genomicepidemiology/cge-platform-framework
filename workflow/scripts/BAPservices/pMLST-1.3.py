#!/usr/bin/env python
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Pipeline structure
#--> The input/arguments are validated
#--> Log service submission in SQL (status = 'In progress' )
#--> Retrieve service ID
#--> Create service folder and subfolders
#--> The input files are copied to the 'Input' directory 
#--> Setup and execution of pMLST is performed
#--> The success of the service is validated
#--> Downloadable files are copied/moved to the 'Download' Directory and a file
# entry is added in the SQL DB
#--> The SQL service entry is updated (status = 'Done' or 'Error')
#--> The files are zipped for long-term storage
import sys, os, time, random

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from functions_module_2 import (printDebug, copyFile, program, add2DatabaseLog,
                               getArguments, paths, makeFileList, fileUnzipper,
                               printInputFilesHtml, fileZipper, PrepareDownload,
                               AddService2DB, UpdatePaths, CheckFileType,
                               UpdateServiceInDB, printOut, moveFile, setDebug,
                               createServiceDirs, GraceFullExit, FixPermissions, 
                               tsv2html, dlButton, proglist)

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################

################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
setDebug(True)
service, version = "pMLST", "1.3"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
args = getArguments('''
selectionbox   scheme     -d    VALUE
file           contigs    -f    VALUE
''', pipeline=True, allowcmd=True)

# VALIDATE REQUIRED ARGUMENTS
if args.scheme == None: GraceFullExit("Error: No pMLST Scheme was chosen!\n")
if args.contigs == None: GraceFullExit("Error: No Contigs were provided!\n")
elif not os.path.exists(args.contigs):
   GraceFullExit("Error: Contigs file does not exist!\n")
elif CheckFileType(args.contigs) != 'fasta':
   GraceFullExit('''Error: Invalid contigs format (%s)!\nOnly the fasta format
                 is recognised as a proper contig format.\n'''%(
                  CheckFileType(args.contigs)))

# Log service submission in SQL and Retrieve service ID
serviceID, ifolder = AddService2DB(args.usr, args.token, args.iid, service, version,
                                   args.ip, 'scheme='+args.scheme)

# SET AND UPDATE PATHS
UpdatePaths(service, version, serviceID, ifolder, args.webmode)
paths.add_path('contigs', paths['inputs']+'contigs.fsa')

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()

# COPY CONTIGS FILE TO INPUT DIRECTORY
copyFile(args.contigs, paths['contigs'] +'.gz')
# UNZIPPING CONTIGS FILE IF ZIPPED
fileUnzipper(paths['inputs'])

# EXECUTION OF THE PROGRAMS
pmlst = program( name='pMLST', 
   ptype='perl', path='/home/data1/services/pMLST/pMLST-1.3/scripts/pMLST-1.3_pipeline.pl',
   workDir='', ssh=True, toQueue=True, wait=False, server='cgebase' if args.webmode else 'cgebase2',
   args=['-d', args.scheme,
         '-i', paths['contigs'],
         '-R', paths['serviceRoot']
         ]
   )
pmlst.Execute()
proglist.Add2List(pmlst)
pmlst.WaitOn(pattern='Done', interval=10, maxloopsleft=60)

# THE SUCCESS OF THE PROGRAMS ARE VALIDATED
status = pmlst.GetStatus()
if status == 'Done' and os.path.exists(paths['downloads']+'results.txt'): status = 'Success'
else: status = 'Failure'

if status not in ['Done','Success']:
   UpdateServiceInDB(service, serviceID, status)
   GraceFullExit("Error: Execution of the program failed!\n")

# SUMARISE RESULTS
printDebug('Sumarising results...')
results = {
   'scheme_name':    None,
   'sequence_type':  None,
   'service_id':     serviceID
}
if status == 'Success':
   if args.scheme == 'incf':
      flag = 0
      FAB = ['F','A','B']
   with open(paths['downloads']+'results.txt', 'r') as f:
      for l in f:
         d = [ld.strip() for ld in l.split(':')]
         if d[0] == 'pMLST Profile': results['scheme_name'] = d[1]
         if args.scheme == 'incf':
            if d[0] == '='*80: flag = 0; break
            if flag == 2:
               d = l.strip().split()
               match = d[5]
               idx = 'IAB'.find(match[2])
               if idx >= 0:
                  try:
                     st = int(match.split('_')[-1])
                     if st == 0: st = '-' # ST not applicable
                     elif float(d[1]) < 100.00 or d[2] != d[3]: st = '*' # New ST found
                     else: st = str(st)
                  except: st = '-'
                  FAB[idx] += st
            if d[0] == '*'*80: flag += 1
         else:
            if d[0] == 'Sequence Type': results['sequence_type'] = d[1]
            elif d[0] == '*'*80: break
   
   if args.scheme == 'incf': results['sequence_type'] = ':'.join(FAB)

# PRINT SEQUENCE TYPE
print("\n#sequence_type=%s\n"%(results['sequence_type']))

#txt = '''
#pMLST Results
#
#Sequence Type: Unknown ST
#pMLST Profile: IncF
#
#********************************************************************************
#   GENE       % IDENTITY    HSP Length    Allele Length   GAPS     BEST MATCH   
#********************************************************************************
#   FII             -            -               -           -           FII_0
#   FIA          100.00         329             329          0           FIA_6
#   FIB           99.20         373             373          0           FIB_6
#================================================================================
#
#'''


# CREATE THE HTML OUTPUT FILE (No need to change this part)
if os.path.exists(paths['outputs']) and not os.path.exists(paths['outputs']+service+".out"):
   with open(paths['outputs']+service+".out", 'w') as f:
      pass # only create file, pMLST does the rest
      #f.write('<h1>%s-%s Server - Results</h1>'%(service, version))

# PREPARE THE DOWNLOADABLE FILE(S)
_ = PrepareDownload(paths['downloads']+'results.txt', args.iid,
                      serviceID, 'results', args.usr,
                      'Text version of the pMLST results.')
_ = PrepareDownload(paths['downloads']+'Hit_in_genome_seq.fsa', args.iid,
                      serviceID, 'hitlist', args.usr,
                      'Fasta file containing all the hits.')
_ = PrepareDownload(paths['downloads']+'pMLST_allele_seq.fsa', args.iid,
                      serviceID, 'allelelist', args.usr,
                      'Fasta file containing all the pMLST allele sequences.')

################################################################################
#(No need to change this part)
# UPDATE mySQL SERVICE ENTRY (status = 'Done' or 'Error')
UpdateServiceInDB(service, serviceID, status, results)

# LOG THE TIMERS
proglist.PrintTimers() 

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE
fileZipper(paths['serviceRoot'])

# INDICATE THAT THE WRAPPER HAS FINISHED
sys.stderr.write("Done")
