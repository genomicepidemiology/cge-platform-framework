#!/usr/bin/env python
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Pipeline structure
#--> The input/arguments are validated
#--> Log service submission in SQL (status = 'In progress' )
#--> Retrieve service ID
#--> Create service folder and subfolders
#--> The input files are copied to the 'Input' directory 
#--> Setup and execution of MLST is performed
#--> The success of the service is validated
#--> Downloadable files are copied/moved to the 'Download' Directory and a file
# entry is added in the SQL DB
#--> The SQL service entry is updated (status = 'Done' or 'Error')
#--> The files are zipped for long-term storage
import sys, os, time, random

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from functions_module_2 import (printDebug, copyFile, program, add2DatabaseLog,
                               getArguments, paths, makeFileList, fileUnzipper,
                               printInputFilesHtml, fileZipper, PrepareDownload,
                               AddService2DB, UpdatePaths, CheckFileType,
                               UpdateServiceInDB, printOut, moveFile, setDebug,
                               createServiceDirs, GraceFullExit, FixPermissions, 
                               tsv2html, dlButton, proglist)

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################

################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
setDebug(True)
service, version = "PipelineServiceTemplate", "1.0"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
args = getArguments('''
text           text         -x  VALUE  -x  ''
file           file         -f  VALUE  -f  ''
textarea       textarea     -a  VALUE  -a  ''
checkbox       checkbox     -c  VALUE  -c  ''
radio          radio        -r  VALUE  -r  ''
selectionbox   select       -s  VALUE  -s  ''
mselectionbox  multiselect  -m  VALUE  -m  ''
''', allowcmd=True, pipeline=True)

# VALIDATE REQUIRED ARGUMENTS
if args.text == None: GraceFullExit("Error: No text was written!\n")
if args.file == None: GraceFullExit("Error: No file were provided!\n")
elif not os.path.exists(args.file):
   GraceFullExit("Error: file does not exist!\n")

# Log service submission in SQL and Retrieve service ID
serviceID, ifolder = AddService2DB(args.usr, args.token, args.iid, service, version,
                                   args.ip, '')

# SET AND UPDATE PATHS
UpdatePaths(service, version, serviceID, ifolder, args.webmode)
paths.add_path('file', paths['inputs']+'file.ext')

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()

# COPY CONTIGS FILE TO INPUT DIRECTORY
copyFile(args.file, paths['file'] + ('.gz' if args.file.split('.')[-1] == 'gz' else ''))
# UNZIPPING ZIPPED FILES IN THE INPUT DIRECTORY
fileUnzipper(paths['inputs'])

# EXECUTION OF THE PROGRAMS
dummy = program( name='dummy', 
   ptype='python', path=paths['scripts']+'dummy.py',
   workDir='', ssh=True, toQueue=True, wait=False,
   args=['-i', paths['file'],
         '-R', paths['serviceRoot']
         ]
   )
#if args.webmode: mlst.AppendArgs('-w')
dummy.Execute()
proglist.Add2List(dummy)
dummy.WaitOn(pattern='Done', interval=10, maxloopsleft=20)

# THE SUCCESS OF THE PROGRAMS ARE VALIDATED
status = dummy.GetStatus()
if status == 'Done' and os.path.exists(dummy.stdout): status = 'Success'
else: status = 'Failure'

if status not in ['Done','Success']:
   UpdateServiceInDB(service, serviceID, status)
   GraceFullExit("Error: Execution of the program failed!\n")

# SUMARISE RESULTS
results = {
   'var1':        None,
   'var2':        None,
   'service_id':  serviceID
}
if status == 'Success':
   # Find var1 and var2 in the stdout by looking for following lines:
   # '#var1=value\n' and '#var2=value\n'
   results['var1'], results['var2'] = dummy.FindStdoutVar(['var1', 'var2'])

# CREATE THE HTML OUTPUT FILE (No need to change this part)
if args.webmode:
   if os.path.exists(paths['outputs']) and not os.path.exists(paths['outputs']+service+".out"):
      with open(paths['outputs']+service+".out", 'w') as f:
         f.write('<h1>%s-%s Server - Results</h1>'%(service, version))

# PREPARE THE DOWNLOADABLE FILE(S)
_ = PrepareDownload(paths['outputs']+service+".out", args.iid,
                      serviceID, 'results', args.usr,
                      'The output of the service.')

################################################################################
#(No need to change this part)
# UPDATE mySQL SERVICE ENTRY (status = 'Done' or 'Error')
UpdateServiceInDB(service, serviceID, status, results)

# LOG THE TIMERS
proglist.PrintTimers() 

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE
fileZipper(paths['serviceRoot'])

# INDICATE THAT THE WRAPPER HAS FINISHED
sys.stderr.write("Done")
