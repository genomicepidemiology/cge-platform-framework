#!/usr/bin/env python
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Pipeline structure
#--> The input/arguments are validated
#--> Log service submission in SQL (status = 'In progress' )
#--> Retrieve service ID
#--> Create service folder and subfolders
#--> The input files are copied to the 'Input' directory 
#--> Setup and execution of MLST is performed
#--> The success of the service is validated
#--> Downloadable files are copied/moved to the 'Download' Directory and a file
# entry is added in the SQL DB
#--> The SQL service entry is updated (status = 'Done' or 'Error')
#--> The files are zipped for long-term storage
import sys, os, time, random

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from functions_module_2 import (printDebug, copyFile, program, add2DatabaseLog,
                               getArguments, paths, makeFileList, fileUnzipper,
                               printInputFilesHtml, fileZipper, PrepareDownload,
                               AddService2DB, UpdatePaths, CheckFileType,
                               UpdateServiceInDB, printOut, moveFile, setDebug,
                               createServiceDirs, GraceFullExit, FixPermissions, 
                               tsv2html, dlButton, proglist)

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################

################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
setDebug(False)
service, version = "spaTyper", "1.0"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
args = getArguments('''
file    contigs  -f  VALUE
''', pipeline=True, allowcmd=True)

# VALIDATE REQUIRED ARGUMENTS
if args.contigs == None: GraceFullExit("Error: No Contigs were provided!\n")
elif not os.path.exists(args.contigs):
   GraceFullExit("Error: Contigs file does not exist!\n")
elif not CheckFileType(args.contigs) in ['fasta','fastq']:
   GraceFullExit(("Error: Invalid file format (%s)!\nOnly fasta or fastq format"
                  "can be used.\n")%(CheckFileType(args.contigs)))

# Log service submission in SQL and Retrieve service ID
serviceID, ifolder = AddService2DB(args.usr, args.token, args.iid, service, version,
                                   args.ip, '')

# SET AND UPDATE PATHS
UpdatePaths(service, version, serviceID, ifolder, args.webmode)
paths.add_path('contigs', paths['inputs']+'contigs.fsa')

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()

# COPY CONTIGS FILE TO INPUT DIRECTORY
copyFile(args.contigs, paths['contigs'] +'.gz')
# UNZIPPING ZIPPED FILES IN THE INPUT DIRECTORY
fileUnzipper(paths['inputs'])

# ADDING PROGRAMS
spaTyper = program(
   name='spa_type', path=paths['scripts'] +'spa_type.find.script.sh',
   timer=0, ptype='bash', ssh=True, toQueue=True, wait=False, workDir='',
   args=[paths['contigs'],
         paths['database'] +'spa_sequences.fna'
         ]
   )

# EXECUTION OF THE PROGRAMS
spaTyper.Execute()
proglist.Add2List(spaTyper)
spaTyper.WaitOn(pattern='Done', interval=15, maxloopsleft=90)

# THE SUCCESS OF THE SERVICE IS VALIDATED
status = spaTyper.GetStatus()
if status != 'Done': GraceFullExit("Error: Execution of the program failed!\n")

# Fetch the results from the spaTyper
results = {
   'spa_type':    'unknown',
   'repeats':     '',
   'contig':      'N/A',
   'position':    '',
   'orientation': 'N/A'
}

if os.path.exists(spaTyper.stdout):
   with open(spaTyper.stdout) as f:
      for l in f:
         if l[:5] == 'Bingo':
            #Bingo	spatype_t011	NODE_259_length_63698_cov_37.610912	position:30629-30834	plus
            #Bingo   spatype_t12333  C00004046_n1_c16        position:15641-15990    minus
            tmp = l.split('\t')
            if len(tmp) == 1:
               tmp = l.split()
            results['spa_type']    = tmp[1].split('_')[1].strip()
            results['contig']      = tmp[2].strip()
            results['position']    = tmp[3].split(':')[1].strip()
            results['orientation'] = tmp[4].strip()
            # Update Status
            status = 'Success'
else: printDebug('Error: stdout did not exist: %'%spaTyper.stdout)

# Get the repeats order
if os.path.exists(paths['database']+'spa_types.txt'):
   le = len(results['spa_type'])
   with open(paths['database']+'spa_types.txt') as f:
      for l in f:
         #t011,08-16-02-25-34-24-25
         if l[:le] == results['spa_type']:
            results['repeats'] = l.split(',')[1].strip()
else: printDebug('Error: The file containing spa types did not exist: %'%(paths['database']+'spa_types.txt'))

# CREATE THE HTML OUTPUT FILE (No need to change this part)
if args.webmode:
   if os.path.exists(paths['outputs']) and not os.path.exists(paths['outputs']+service+".out"):
      with open(paths['outputs']+service+".out", 'w') as f:
         f.write('<h1>%s-%s Server - Results</h1>'%(service, version))

# Create TAB FILE for RESULT
tsv=paths['downloads']+'spaType_results.tsv'
with open(tsv, 'w') as f:
   #f.write('#') # Add header indicator to first line
   #f.write('\n'.join(['\t'.join(['-'.join(y) if isinstance(y, list) else str(y) for y in x]) for x in zip(*results.items())]))
   f.write("#spa Typing\n")
   f.write("#spa Type\tRepeats\tContig\tPosition\tOrientation\n")
   f.write("%s\t%s\t%s\t%s\t%s\n"%(results['spa_type'], results['repeats'],
                                   results['contig'], results['position'],
                                   results['orientation']))

# PRINT TAB FILE(S) AS RESULT TABLES TO THE HTML OUTPUT FILE
if os.path.exists(tsv):
   tsv2html(tsv, css_classes='greenhead')
   printOut('<br>\n')
else: printDebug('Error: The result file was not created: %'%tsv)

################################################################################
#(No need to change this part)
# UPDATE mySQL SERVICE ENTRY (status = 'Done' or 'Error')
UpdateServiceInDB(service, serviceID, status, results)

# LOG THE TIMERS
proglist.PrintTimers() 

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE
fileZipper(paths['serviceRoot'])

# INDICATE THAT THE WRAPPER HAS FINISHED
sys.stderr.write("Done")
