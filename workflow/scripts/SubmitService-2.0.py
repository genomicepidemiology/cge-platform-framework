#!/usr/bin/env python
'''
################################################################################
#                         CGE Platform - SubmitService                         #
################################################################################
# This script is main body of the CGE Pipeline structure
#--> The input/arguments are parsed and validated
#--> Log isolate submission in SQL
#--> Retrieve isolate ID
#--> Create isolate folder and subfolders
#--> The input files are moved to the 'Input' directory and zipped
#--> Check the requested service parameters and execute the service (subprocess)
#--> Print outputpage:
#      * Summary of errors with guide to fix them
#      * Redirection link to isolate overview page
#      * Link to CGE pipeline for submission of another query
#--> Clean up old files from upload directory

Submit Service 2.0 Changes:
TODO * Update the assembler to 1.1
      - Add Reads and assembly analysis
      - Add FASTQC execution?
TODO * Update KmerFinder to 2.1
V * Add spaTyper 1.0
      - setup automatik update of spaTyper database
V * Add SerotypeFinder 1.0

'''
# Including the CGE modules!
import sys, os, glob, re
from subprocess import Popen, PIPE
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from functions_module_2 import (UpdatePaths, copyFile, program, add2DatabaseLog,
                                getArguments, paths, makeFileList, fileUnzipper,
                                printInputFilesHtml, dlButton, PrepareDownload,
                                AddIsolate2DB, CheckFileType, UpdateIsolate,
                                printOut, moveFile, setDebug, createIsolateDirs,
                                GraceFullExit, printDebug, tsv2html, fileZipper,
                                FixPermissions, proglist, getnote, AddFile2DB,
                                printLog, RetrieveContigsInfo, open_, adv_dict,
                                GetServiceResults, setGlobal, VerifyBatchAccess,
                                Reg, GetIsolateDir)

################################################################################
#                                  FUNCTIONS                                   #
################################################################################
def WaitForAssembly(proglist):
   ''' Checking the status of the assembler, and waiting for it to finish if
   it is still running. '''
   # Verify that assembly is submitted
   if proglist.Exists('Assembler'):
      status = proglist['Assembler'].GetStatus()
      if status == 'Executing':
         printDebug("\nWaiting for Assembly to finish...") #DEBUG
         proglist['Assembler'].WaitOn(pattern='Done', ePattern='Error')
         status = proglist['Assembler'].GetStatus()
         # THE SUCCESS OF THE ASSEMBLY IS VALIDATED
         if status == 'Done':
            print(template_line%'The Assembly was finished...')
            # Retrieve contigs path
            path = RetrieveContigsInfo(args.usr, access_token, isolate_id)
            if path is not None:
               # Removing the gz extension
               if path[-3:]=='.gz': path = path[:-3]
               # Checking if the file exists as normal or gzipped file
               if os.path.exists(path):
                  paths.add_path('contigs', path)
               elif os.path.exists(path+'.gz'):
                  paths.add_path('contigs', path+'.gz')
               else:
                  printDebug("Contigs could not be found! (%s)"%(path))
                  print(template_line%'The contigs file could not be found!')
                  status = 'Failure'
                  proglist['Assembler'].status = status
            else:
               print(template_line%'No suitable contigs was created by the assembler!')
               status = 'Failure'
               proglist['Assembler'].status = status
      if status == 'Done': return True
      else: return False
   else:
      return True

def printNsave(*lst):
   '''  '''
   outputfile = paths['logs']+"/pipeline.out"
   if os.path.exists(paths['logs']) and not os.path.exists(outputfile):
      # Creating output file
      with open(outputfile, 'w') as f: pass
   printLog(outputfile, True, False, *lst)

def CMDout2list(cmd):
   ''' Executes a command through the operating system and returns the output
   as a list, or on error a string with the standard error.
   EXAMPLE:
      >>> from subprocess import Popen, PIPE
      >>> CMDout2array('ls -l')
   '''
   p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
   stdout, stderr = p.communicate()
   if p.returncode != 0 and stderr != '': return "ERROR: %s\n"%(stderr)
   else: return stdout.split('\n')

################################################################################
#                                    MAIN                                      #
################################################################################
# SET GLOBAL VARIABLES
setDebug(False)
service, version = "SubmitService", "2.0"
paths.programRoot = '/home/data1/services/CGEpipeline/CGEpipeline-2.0/scripts/BAPservices/'
setGlobal('service', service)
setGlobal('version', version)

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('--sequencing_platform',   'sequencing_platform',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file

# source_note, pathogenicity_note, notes WAS textarea before

#/home/data1/services/CGEpipeline/CGEpipeline-2.0/SubmitService-2.0.py -w --uploadpath notset --ip 10.57.8.236 --usr Martin --sesid 8ac4efff3cca9b21b5916f9cc43dd05d7b6084bc --iid 8572 --ser MLST --ao '' --PFt 95.00 --PFd plasmid_database --MTs achromobacter --PMs '' --RFd aminoglycoside,beta-lactamase,quinolone,fosfomycin,fusidicacid,vancomycin,macrolide,nitroimidazole,oxazolidinone,phenicol,rifampicin,sulphonamide,tetracycline,trimethoprim --RFt 98.00 --RFl 0.60 --VFd virulence_ecoli,virulence_ENT,s.aureus_adherence,s.aureus_toxin,s.aureus_exoenzyme,s.aureus_hostimm,s.aureus_secretion --VFt 98.00 --PTm auto
args = getArguments('''
text           iid               --iid       VALUE

checkbox       services          --ser       VALUE

checkbox       assembleroptions  --ao        VALUE

selectionbox   KFscoring         --KFs       VALUE
selectionbox   KFdatabase        --KFd       VALUE

selectionbox   PFthreshold       --PFt       VALUE
mselectionbox  PFdatabases       --PFd       VALUE

selectionbox   MLSTscheme        --MTs       VALUE
selectionbox   pMLSTscheme       --PMs       VALUE

mselectionbox  RFdatabases       --RFd       VALUE
selectionbox   RFthreshold       --RFt       VALUE
selectionbox   RFminlength       --RFl       VALUE

mselectionbox  SFdatabases       --SFd       VALUE
selectionbox   SFthreshold       --SFt       VALUE
selectionbox   SFminlength       --SFl       VALUE

mselectionbox  VFdatabases       --VFd       VALUE
selectionbox   VFthreshold       --VFt       VALUE

selectionbox   PAtaxModel        --PTm       VALUE
''', allowcmd=True)

# ADD DEFAULT VALUES
if args.assembleroptions is None: args.assembleroptions = ''
if args.services is None: args.services = ''
if args.KFscoring is None: args.KFscoring = 'winner'
if args.KFdatabase is None: args.KFdatabase = 'bacteria_o'
if args.PFthreshold is None: args.PFthreshold = '80.00'
if args.PFdatabases is None: args.PFdatabases = 'plasmid_database' # ,plasmid_positiv
if args.RFdatabases is None: args.RFdatabases = 'aminoglycoside,beta-lactamase,quinolone,fosfomycin,fusidicacid,vancomycin,macrolide,phenicol,rifampicin,sulphonamide,tetracycline,trimethoprim'
if args.RFthreshold is None: args.RFthreshold = '98.00'
if args.RFminlength is None: args.RFminlength = '0.60'
if args.SFdatabases is None: args.SFdatabases = 'H_type,O_type'
if args.SFthreshold is None: args.SFthreshold = '85.00'
if args.SFminlength is None: args.SFminlength = '0.60'
if args.VFthreshold is None: args.VFthreshold = '90.00'
if args.VFdatabases is None: args.VFdatabases = 'virulence_ecoli,virulence_ENT,s.aureus_adherence,s.aureus_toxin,s.aureus_exoenzyme,s.aureus_hostimm,s.aureus_secretion'
if args.PAtaxModel is None: args.PAtaxModel = 'auto'

# VALIDATION OF SERVICE SPECIFIC ARGUMENTS
if args.services != '': services = args.services.split(',')
else: GraceFullExit("Error: No services was requested!\n")


if (     'PlasmidFinder' in services
     and
         (    args.PFthreshold == ''
           or args.PFdatabases == ''
         )
   ):
   GraceFullExit("Error: PlasmidFinder threshold and/or databases was "
                 "missing!\n")

if 'MLST' in services and args.MLSTscheme == '':
   GraceFullExit("Error: No MLST scheme was chosen!\n")

if 'pMLST' in services and args.pMLSTscheme == '':
   GraceFullExit("Error: No pMLST scheme was chosen!\n")

if (     'ResFinder' in services
     and
         (    args.RFdatabases == ''
           or args.RFminlength == ''
           or args.RFthreshold == ''
         )
   ):
   GraceFullExit("Error: Missing ResFinder threshold or databases or minlength "
                 "arguments!\n")

if (     'SerotypeFinder' in services
     and
         (    args.SFdatabases == ''
           or args.SFminlength == ''
           or args.SFthreshold == ''
         )
   ):
   GraceFullExit("Error: Missing SerotypeFinder threshold or databases or minlength "
                 "arguments!\n")

if (     'VirulenceFinder' in services
     and
         (    args.VFthreshold == ''
           or args.VFdatabases == ''
         )
   ):
   GraceFullExit("Error: Missing VirulenceFinder threshold or databases "
                 "arguments!\n")

if 'PathogenFinder' in services and args.PAtaxModel == '':
   GraceFullExit("Error: Missing modelselection for PathogenFinder!\n")

# Extract Isolate ID
try: isolate_id = int(args.iid)
except: GraceFullExit("Error: A proper isolate ID was not provided!\n")

# Authorize Platform Access
access_token = VerifyBatchAccess(args.usr, args.sesid)

# Get Isolate Directory
isodir = GetIsolateDir(args.usr, access_token, isolate_id)
if isodir is not None:
   paths.services = isodir+'/services/'
   paths.logs     = isodir+'/logs/'
   paths.outputs  = isodir+'/logs/'
else: GraceFullExit("Error: The isolate directory could not be found!\n")

# PRINT HTML OUTPUT
template_line = '<li>%s</li>'
template_head = '<h1>%s</h1><ul>'
print(template_head%('%s-%s Pipeline Service Summary'%(service, version)))

# EXECUTE CONTIGS INDEPENDENT PIPELINE SERVICES
if 'Assembler' in services: # Assembler
   # ASSEMBLE THE READS
   assembler = program( name='Assembler', 
      ptype=None, path=paths['programRoot']+'Assembler-1.1.py',
      workDir=paths['services'], ssh=False, toQueue=False, wait=False,
      args=['--iid',     isolate_id,
            '--ip',      args.ip,
            '--usr',     args.usr,
            '--token',   access_token,
            '--options', args.assembleroptions
            ]
      )
   if args.webmode: assembler.AppendArgs('-w')
   assembler.Execute()
   proglist.Add2List(assembler)
   print(template_line%'Assembler was executed according to plan...')
   assembly_done = WaitForAssembly(proglist)
else:
   # GET CONTIGS PATHS
   path = RetrieveContigsInfo(args.usr, access_token, isolate_id) # CHECK BATCH SUBMISSION TOKEN PROBLEM!!!!
   if path is not None:
      # Removing the gz extension
      if path[-3:]=='.gz': path = path[:-3]
      # Checking if the file exists as normal or gzipped file
      if os.path.exists(path):
         paths.add_path('contigs', path)
      elif os.path.exists(path+'.gz'):
         paths.add_path('contigs', path+'.gz')
      else:
         printDebug("Contigs could not be found! (%s)"%(path))
         print(template_line%'The contigs file could not be found!')

# Verify Contigs
if paths['contigs'] == '':
   GraceFullExit("Error: No contigs file was found!\n")

# SPECIES TYPING
if 'KmerFinder' in services: # KmerFinder
   kmerfinder = program(name='KmerFinder', 
      ptype=None, path=paths['programRoot']+'KmerFinder-2.1.py',
      workDir=paths['services'], ssh=False, toQueue=False, wait=False,
      args=['--iid',   isolate_id,
            '--ip',    args.ip,
            '--usr',   args.usr,
            '--token', access_token,
            '-f',      paths['contigs'],
            '-s',      args.KFscoring,
            '-d',      args.KFdatabase
            ]
      )
   if args.webmode: kmerfinder.AppendArgs('-w')
   kmerfinder.Execute()
   proglist.Add2List(kmerfinder)
   print(template_line%'KmerFinder was executed according to plan...')

if 'SpeciesFinder' in services:
   # Setup SpeciesFinder
   pass

# EXECUTE CONTIGS DEPENDENT PIPELINE SERVICES
if 'PathogenFinder' in services: # PathogenFinder
   pathogenfinder = program( name='PathogenFinder', 
      ptype=None, path=paths['programRoot']+'PathogenFinder-1.1.py',
      workDir=paths['services'], ssh=False, toQueue=False, wait=False,
      args=['--iid',   isolate_id,
            '--ip',    args.ip,
            '--usr',   args.usr,
            '--token', access_token,
            '-m', args.PAtaxModel,
            '-f', paths['contigs']
            ]
      )
   if args.webmode: pathogenfinder.AppendArgs('-w')
   pathogenfinder.Execute()
   proglist.Add2List(pathogenfinder)
   print(template_line%'PathogenFinder was executed according to plan...')

if 'ResFinder' in services: # ResFinder
   resfinder = program( name='ResFinder', 
      ptype=None, path=paths['programRoot']+'ResFinder-2.1.py',
      workDir=paths['services'], ssh=False, toQueue=False, wait=False,
      args=['--iid',   isolate_id,
            '--ip',    args.ip,
            '--usr',   args.usr,
            '--token', access_token,
            '-f', paths['contigs'],
            '-d', args.RFdatabases,
            '-T', args.RFthreshold,
            '-L', args.RFminlength
            ]
      )
   if args.webmode: resfinder.AppendArgs('-w')
   resfinder.Execute()
   proglist.Add2List(resfinder)
   print(template_line%'ResFinder was executed according to plan...')

if 'SerotypeFinder' in services: # SerotypeFinder
   print '<!-- %s, %s, %s -->'%(isolate_id, args.usr, access_token)
   serotypefinder = program( name='SerotypeFinder', 
      ptype=None, path=paths['programRoot']+'SerotypeFinder-1.1.py',
      workDir=paths['services'], ssh=False, toQueue=False, wait=False,
      args=['--iid',   isolate_id,
            '--ip',    args.ip,
            '--usr',   args.usr,
            '--token', access_token,
            '-f', paths['contigs'],
            '-d', args.SFdatabases,
            '-T', args.SFthreshold,
            '-L', args.SFminlength
            ]
      )
   if args.webmode: serotypefinder.AppendArgs('-w')
   serotypefinder.Execute()
   proglist.Add2List(serotypefinder)
   print(template_line%'SerotypeFinder was executed according to plan...')

# SEQUENCE TYPING
if 'MLST' in services: # MLST
   # Find MLST Scheme
   if args.MLSTscheme == '':
      print(template_line%'MLST could not be executed since no MLST scheme was provided!')
   else:
      mlst = program( name='MLST', 
         ptype=None, path=paths['programRoot']+'MLST-1.6.py',
         workDir=paths['services'], ssh=False, toQueue=False, wait=False,
         args=['--iid',   isolate_id,
               '--ip',    args.ip,
               '--usr',   args.usr,
               '--token', access_token,
               '-f', paths['contigs'],
               '-d', args.MLSTscheme,
               '-w']
         )
      mlst.Execute()
      proglist.Add2List(mlst)
      print(template_line%'MLST was executed according to plan...')

# PLASMID TYPING (includes pMLST)
if 'PlasmidFinder' in services: # PlasmidFinder
   if args.PFdatabases == '' or float(args.PFthreshold)<=0 or float(args.PFthreshold)>100:
      print(template_line%'PlasmidFinder could not be executed since either no databases was specified, or an invalid threshold was specified!')
   else:
      plasmidfinder = program( name='PlasmidFinder', 
         ptype=None, path=paths['programRoot']+'PlasmidFinder-1.2.py',
         workDir=paths['services'], ssh=False, toQueue=False, wait=False,
         args=['--iid',   isolate_id,
               '--ip',    args.ip,
               '--usr',   args.usr,
               '--token', access_token,
               '-T', args.PFthreshold,
               '-d', args.PFdatabases,
               '-f', paths['contigs']
               ]
         )
      if args.webmode: plasmidfinder.AppendArgs('-w')
      plasmidfinder.Execute()
      proglist.Add2List(plasmidfinder)
      print(template_line%'PlasmidFinder was executed according to plan...')

# Virus phenotyping
if 'VirulenceFinder' in services: # VirulenceFinder
   if args.VFdatabases == '' or float(args.VFthreshold)<=0 or float(args.VFthreshold)>100:
      print(template_line%'VirulenceFinder could not be executed since either no databases was specified, or an invalid threshold was specified!')
   else:
      virulencefinder = program( name='VirulenceFinder', 
         ptype=None, path=paths['programRoot']+'VirulenceFinder-1.2.py',
         workDir=paths['services'], ssh=False, toQueue=False, wait=False,
         args=['--iid',   isolate_id,
               '--ip',    args.ip,
               '--usr',   args.usr,
               '--token', access_token,
               '-T', args.VFthreshold,
               '-d', args.VFdatabases,
               '-f', paths['contigs']
               ]
         )
      if args.webmode: virulencefinder.AppendArgs('-w')
      virulencefinder.Execute()
      proglist.Add2List(virulencefinder)
      print(template_line%'VirulenceFinder was executed according to plan...')

# SPA TYPING
if 'spaTyper' in services: # spaTyper
   spatyper = program(name='spaTyper', 
      ptype=None, path=paths['programRoot']+'spaTyper-1.0.py',
      workDir=paths['services'], ssh=False, toQueue=False, wait=False,
      args=['--iid',   isolate_id,
            '--ip',    args.ip,
            '--usr',   args.usr,
            '--token', access_token,
            '-f',      paths['contigs']
            ]
      )
   if args.webmode: spatyper.AppendArgs('-w')
   spatyper.Execute()
   proglist.Add2List(spatyper)
   print(template_line%'spaTyper was executed according to plan...')


# Wait on Services to finish
printDebug('\nWait on all services to finish...')
for progname in proglist.list: proglist[progname].WaitOn()

# Close HTML list
print('</ul>')

# TIME STAMPS / BENCHMARKING
proglist.PrintTimers()

# Fix permissions for folders and files
os.system("find %s -type d -print -exec chmod 775 {} \; >& /dev/null"%(paths['isolateRoot']))
os.system("find %s -type f -print -exec chmod 664 {} \; >& /dev/null"%(paths['isolateRoot']))

# INDICATE THAT THE WRAPPER HAS FINISHED
sys.stderr.write("Done")
