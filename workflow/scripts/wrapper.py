#!/usr/bin/env python
'''
################################################################################
#                           CGE Pipeline BatchUpload                           #
################################################################################
The BatchUpload script executes the CGE Pipeline for each uploaded isolate
--> The input/arguments are parsed and validated
  |--> The upload folder is found and the JSON object file is loaded
  |--> The meta data for each isolate is validated
  |--> The excistance of the isolate files are validated
--> Each isolate is prepared for the pipeline
--> The pipeline calls are made
--> Print outputpage:
      * Summary of errors with guide to fix them
      * Redirection link to isolate overview page
      * Link to CGE pipeline for submission of another query
--> Clean up old files from upload directory

Batch Upload 2.0 Changes:
TODO * Validate metadata
      - catch wrong collection dates, alert the user and set them to None, but continue
TODO * Update Bacterium Analysis Pipeline (BAP)
TODO * Add Virus Analysis Pipeline (VAP)
TODO * Add Fungi Analysis Pipeline (FAP)
TODO * Add Parasite Analysis Pipeline (PAP)
TODO * Update Metadatasheet
      - Organism: please write the full scientific species name or 'unknown'
'''
# Including the CGE modules!
import sys, os, json
import geocoder, datetime
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from functions_module_2  import (printDebug, copyFile, program, createServiceDirs,
                                 getArguments, paths, makeFileList, fileUnzipper,
                                 printInputFilesHtml, fileZipper, PrepareDownload,
                                 UpdatePaths, proglist, CheckFileType, printOut,
                                 moveFile, setDebug, add2DatabaseLog, tsv2html,
                                 GraceFullExit, FixPermissions, dlButton, getGlobal,
                                 VerifyBatchAccess)

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################
def valid_metadata(metadata):
   '''
   * Check if all required fields are filled out
   * Validate that the required fields are filled out correctly where possible
   * Return True/False
   '''

#    sequencing_platform: use technology conversion table
#    sample_type: metagenomic/isolate	
#    organism: if text? ok: error!	
#    strain: if text? ok: error!
#    serovar: if text? ok: error!
	
#    Python location library	
#    country	
#    region	
#    city	
#    zip_code	

#    longitude	
#    latitude	

#    location_note: if text? ok: error!	

#    isolation_source: human, water, food, animal, other	
#    source_note: if text? ok: error!	
#    pathogenic: yes / no / unknown
#    pathogenicity_note: if text? ok: error!
#    collection_date: Use the following format: YYYY-MM-DD
#    collected_by: if text? ok: error!	
#    usage_restrictions: delete / public	
#    release_date: if date? ok: error!		
#    email_address: if email? ok: error!	e.g: 123+123@hu	
#    notes: if text? ok: error!	

   
   return True

def get_coordinates(metadata):
   '''
   * Get coordinates of the isolate if not present
   * Return metadata updated
   '''
   
   # Coordinates not present
   if (metadata["longitude"] == '' and metadata["latitude"] == ''):
      # Region exists in the metadata
      # TODO: Improve the location string

      if metadata["region"] != '' and str(metadata["zip_code"]) != '' and metadata["city"] != '':
         location_string = "%s %s, %s, %s"%(metadata["zip_code"],
                                            metadata["city"],
                                            metadata["region"],
                                            metadata["country"])
      else:
         if str(metadata["zip_code"]) != '':
            location_string = "%s %s, %s"%(metadata["zip_code"],
                                           metadata["city"],
                                           metadata["country"])
         else:
            location_string = "%s, %s"%(metadata["city"], metadata["country"])
      
      #location_string = ', '.join([x for x in [' '.join([args.zip_code, args.city]).strip(), args.region, args.country] if x != ''])
      
      g = geocoder.google(location_string)
         
      # Coordinates were not found, use Country instead
      if g.latitude == None or g.longitude == None:
         g = geocoder.google(metadata["country"])
         metadata["location_uncertainty_flag"] = '2'
      else:
         metadata["location_uncertainty_flag"] = '1'

      # Replace country from geocoder in metadata
      # According to Google it follows the ISO 3166-1
      # http://en.wikipedia.org/wiki/ISO_3166-1
      metadata["go_country"] = g.country if g.country != None else ''
      metadata["go_longitude"] = g.lng if g.lng != None else ''
      metadata["go_latitude"] =  g.lat if g.lat != None else ''
      metadata["go_city"] =  g.locality if g.locality != None else ''
      metadata["go_zip_code"] =  g.postal if g.postal != None else ''
      metadata["go_region"] =  g.county if g.county != None else ''
      
      return metadata
      
   else:
      metadata["location_uncertainty_flag"] = '0'
      # Check location from coordinates with metadata
      g = geocoder.reverse((metadata["latitude"], metadata["longitude"]))
      metadata["go_country"] = g.country if g.country != None else ''
      metadata["go_city"] =  g.locality if g.locality != None else ''
      metadata["go_zip_code"] =  g.postal if g.postal != None else ''
      metadata["go_region"] =  g.county if g.county != None else ''
      metadata["go_longitude"] = metadata["longitude"]
      metadata["go_latitude"] =  metadata["latitude"]      
      return metadata

def uncertainty_date_flag(metadata):
   collection_date = str(metadata["collection_date"])
   date = collection_date.split("-")
   date_uncertainty_flag = '0'
   if len(date) == 2:
      date_uncertainty_flag = '1'
      collection_date = date[0] + "-" + date[1] + "-" + "01"
   elif len(date) == 1:
      date_uncertainty_flag = '2'
      collection_date = date[0] + "-" + "01" + "-" + "01"
   return {
      "date": collection_date,
      "flag": date_uncertainty_flag
   }


def printNsave(*lst):
   '''  '''
   outputfile = paths['logs']+"/pipeline.out"
   if os.path.exists(paths['logs']) and not os.path.exists(outputfile):
      # Creating output file
      with open(outputfile, 'w') as f: pass
   printLog(outputfile, True, False, *lst)

################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
# Fix this to handle non unicode characters
setDebug(False)
service, version = "BatchUpload", "2.0"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:

# args = getArguments([
##   CMD-option      variable_name  Default  CMD-help-text
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])

# Or by pasting the argument lines from the contigs file  
args = getArguments('', allowcmd=True)

# SET AND UPDATE PATHS (No need to change this part)
UpdatePaths(service, version, '', '', args.webmode)
paths.scripts = "/home/data1/services/CGEpipeline/CGEpipeline-2.0/scripts/"

# Counting how many isolates in the upload folder
dirs = os.walk(args.uploadPath).next()[1]

# Authorize Batch Access
# each submitted isolate adds an extra hour to the access duration
access_token = VerifyBatchAccess(args.usr, args.sesid, hours=len(dirs), days=1)

# Keep track of the origial uploadPath for all isolates of the user
uploadPath = args.uploadPath

# Paths and logs
paths.logs = uploadPath
paths.outputs = uploadPath

# Create group dict containing group name and associated isolate ids
isolate_groups = {}

# CREATE THE HTML OUTPUT FILE (No need to change this part)
if os.path.exists(paths['outputs']) and not os.path.exists(paths['outputs']+service+".out"):
   if args.webmode:
      with open(paths['outputs']+service+".out", 'w') as f:
         f.write('<h2>%s-%s Server - Results</h2>'%(service, version))
   with open(paths['logs']+"wrapper.log", 'w') as f: pass

if args.webmode:
   template_line = '<li>%s</li>'
   template_head = '<h2>%s</h2><ol>'
else:
   template_line = ' * %s'
   template_head = ' # %s # '
print(template_head%('Sample Summary Pages'))

# EXECUTE ISOLATE PIPLINE
#--> The input/arguments are parsed and validated
for dirname in dirs:
   #|--> The upload folder is found and the JSON object file is loaded
   uploaddir = uploadPath + dirname+ '/'
   if os.path.exists(uploaddir+'meta.json'):
      with open(uploaddir+'meta.json', 'r') as json_data:
         metadata = json.load(json_data)
      if args.webmode: os.remove(uploaddir+'meta.json')    
      #|--> The meta data for each isolate is validated
      if not valid_metadata(metadata):
         # Notify The User of the Invalid Isolate
         print 'FAIL METADATA'
         pass
      else:
         #  |--> The existence of the isolate files are validated
         if not all((os.path.exists(uploaddir+file_) for file_ in str(metadata['file_names']).strip().split(' '))):
            # Notify The User of the missing Isolate files 
            # ...
            # (done on the client side)
            #print all((os.path.exists(uploaddir+file_) for file_ in metadata['file_names'].split(" ")))
            #print [os.path.exists(uploaddir+file_) for file_ in metadata['file_names'].split(" ")]
            #print [uploaddir+file_ for file_ in metadata['file_names'].split(" ")]
            print 'FAIL ALL'
            print metadata
            pass
         else:
            #--> Each isolate is prepared for the pipeline
            iso_dir = str(metadata['upload_dir']) #Get dir from JSON object
            name = 'iso-'+dirname
            
            # Get coordinates if not present
            metadata = get_coordinates(metadata)
            uncertainty = uncertainty_date_flag(metadata)
            metadata["collection_date"] = uncertainty["date"]
            metadata["date_uncertainty_flag"] = uncertainty["flag"]
            
            # Wait has been changed to True !
            # Originally True
            # toQueue originally False      
            # toQueue = True fails:
            # qsub: script file '/srv/www/htdocs/tools/server/uploader/isolates/5_14_8_2014_1047_983_438729//cmd.csh' cannot be loaded - No such file or directory qsub: script file '/srv/www/htdocs/tools/server/uploader/isolates/5_14_8_2014_1047_983_438729//cmd.csh' cannot be loaded - No such file or directory qsub: script file '/srv/www/htdocs/tools/server/uploader/isolates/5_14_8_2014_1047_983_438729//cmd.csh' cannot be loaded - No such file or directory
            #print metadata
            _ = proglist.AddProgram(program(
                  name=name, path=paths['scripts']+'BAP-2.0.py', timer=0,
                  ptype=None, toQueue=False, wait=False, workDir=uploadPath,    
                  ssh=False,
                  args=['--uploadpath', uploaddir,
                        '--ip', args.ip,
                        '--usr', args.usr,
                        '--token', access_token,
                        '--country', metadata["country"],
                        '--region', metadata["region"],
                        '--city', metadata["city"],
                        '--zip', metadata["zip_code"],
                        '--longitude', metadata["longitude"],
                        '--latitude', metadata["latitude"],
                        '--location_note', metadata["location_note"],
                        '--collection_date', metadata["collection_date"],
                        '--isolation_source', metadata["isolation_source"],
                        '--source_note', metadata["source_note"],
                        '--pathogenic', metadata["pathogenic"],
                        '--pathogenicity_note', metadata["pathogenicity_note"],
                        '--notes', metadata["notes"],
                        '--sample_name', metadata["sample_name"],
                        '--sample_type', metadata["sample_type"],	
                        '--organism', metadata["organism"],	
                        '--strain', metadata["strain"],
                        '--subtype', metadata["subtype"],	
                        '--collected_by', metadata["collected_by"],	
                        '--usage_restrictions', metadata["usage_restrictions"],
                        '--release_date', metadata["release_date"],
                        '--sequencing_platform', metadata["sequencing_platform"],
                        '--sequencing_type', metadata["sequencing_type"],
                        '--pre_assembled', metadata["pre_assembled"],
                        '--go_country', metadata["go_country"],
                        '--go_longitude', metadata["go_longitude"],
                        '--go_latitude', metadata["go_latitude"],
                        '--go_city', metadata["go_city"],
                        '--go_zip_code', metadata["go_zip_code"],
                        '--go_region', metadata["go_region"],
                        '--date_uncertainty_flag', metadata["date_uncertainty_flag"],
                        '--location_uncertainty_flag', metadata["location_uncertainty_flag"]
                        ]
                  ))
                  
            if args.webmode: proglist[name].AppendArgs(['-w'])
            #--> The pipeline calls are made
            proglist[name].Execute()
            proglist[name].WaitOn(pattern='Done', interval=150, maxloopsleft=60, ePattern='Error')
            
            # Create isolate_group hash for later group analysis
            status = proglist[name].GetStatus()
            if status == 'Done':
               try:
                  isolate_id = int(proglist[name].FindStdoutVar(['isolate_id'])[0])
                  group_names = str(metadata["group_name"]).split()
                  if len(group_names) > 0:
                     for group_name in group_names:
                        if group_name in isolate_groups: isolate_groups[group_name].append(isolate_id)
                        else: isolate_groups[group_name] = [isolate_id]
               except Exception, e:
                  printDebug("Error: Isolate_id could not be associated with group name!", e)
               else:
                  # PRINT isolate analysis result link
                  printOut(template_line%(
                           "<a  href='https://cge.cbs.dtu.dk/tools_new/client/"
                           "platform/show_result.php?IID=%s'>%s</a><br>\n"%(
                              isolate_id, metadata["sample_name"]
                           )
                        ))
               #Remove temporary isolate folder
               # TODO: DO IT LATER
            else:
               #status = 'Failure'
               #printDebug("Error: Pipeline failed for %s"%(name))
               printOut("</ol><pre>Error: Pipeline failed for %s\n\n"%(metadata["sample_name"]))
               if 'Update of mySQL failed (user validation)!' in proglist[name].FindOutPattern("Update of mySQL failed"):
                  printOut("This error was caused by an invalidation of the user session...\n",
                           "To avoid this invalidation in the future, neither logout nor relogin to your account until your submitted batch-job has finished...</pre>")
                  GraceFullExit("Error: Execution of the program failed!\n")
               else:
                  printOut("</pre>")
   else:
      printDebug("Error: The JSON file was not found!\n%s"%(uploaddir+'meta.json'))

# EXECUTE ANALYSIS PIPELINE
for group_name, isolates in isolate_groups.items():
   printDebug("Group name: %s, isolate_ids: %s"%(group_name, isolates))
   # TODO: execute the analysis pipeline

#--> Print outputpage:
#      * Summary of errors with guide to fix them
#      * Redirection link to isolate overview page
#      * Link to CGE pipeline for submission of another query
#--> Clean up old files from upload directory


# PRINT THE STANDARD OUTPUT OF THE PROGRAM TO THE HTML OUTPUT FILE
# PRINT HTML OUTPUT

# Info Statement
page_error = getGlobal('page_error')
if page_error is None:
   if args.webmode:
      print '</ol><br><br><h3>Your samples were uploaded succesfully</h3><p>Take a look at your data on our map/list interface or <a href="/services/cge/batch.php">click here to submit a new batch job.</a></p>'
      print '<iframe style="overflow:hidden;" id="myIframe" src="/tools/client/map/" height="700" width="1024" frameBorder="0"> </iframe>'
   else:
      print 'Your samples were submitted succesfully.'
else:
   if args.webmode:
      print '</ol><h3>An error occured during processing</h3><pre>%s</pre>'%(page_error)
   else:
      print 'An error occured during processing\n%s\n'%(page_error)
################################################################################
# LOG THE TIMERS (No need to change this part)
proglist.PrintTimers() 

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# INDICATE THAT THE WRAPPER HAS FINISHED (No need to change this part)
printDebug("Done")
